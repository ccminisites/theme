<?php
$seconds_to_cache = 300;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");
get_header();
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1><?= pll__('oops-not-found') ?></h1>
            <p class="mt-3">
                <?= pll__('the-url-you-followed-is-not-handled') ?>
            </p>
            <p class="mt-3">
                <?= pll__('we-apologize-and-will-take-you-home-in') ?>
            </p>
            <p class="mt-3">
                <a href="/" class="btn btn-primary"><?= pll__('go-now-to-the-homepage') ?></a>
            </p>
            <script>
                setTimeout(function(){
                  document.location.href = '/';
                }, 10000);
            </script>
        </div>
    </div>
</div>
<?php
get_footer();
