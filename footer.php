<?php
/** @var array $attributes */

$args = [
    'post_type'      => 'badge',
    'post_status'    => 'publish',
    'posts_per_page' => -1,
    'order'          => 'ASC',
    'meta_key'       => 'weight',
    'orderby'        => 'meta_value'
];

// Maybe it's multilingual
if (function_exists('pll_current_language')) {
    $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
}


// Custom query.
$myquery = new WP_Query($args);
$badges  = $myquery->get_posts();

?>


</main>
<footer <?php if ( is_active_sidebar( 'footer-' . $args['lang'] ) ) { ?>class="footer-custom"<?php } ?>>
    <?php if ( is_active_sidebar( 'footer-' . $args['lang'] ) ) : ?>
        <?php get_sidebar('footer'); ?>
    <?php else : ?>
  <div class="container-xl">
    <div class="row footer-menu">
      <div class="col-6 <?php if ($badges) { ?>col-lg-3<?php } else { ?>col-lg-3<?php } ?> order-1 order-md-1">
          <div class="col-12 logo-copyright-wrapper">
            <figure class="col-12 col-lg-12 footer-menu-logo">
              <a href="<?= bloginfo('url') ?>">
                <img class="d-inline-block align-top"
                     src="<?= get_option('p_logo_footer') ?: 'https://via.placeholder.com/320x240?text=logo' ?>"
                     alt='<?= bloginfo('title') ?>'>
              </a>
            </figure>
              <div class="col-12 col-lg-12 copyright-wrapper">
                  <div class="col copyright">
                      &copy; <?= pll__('copyright') ?> <?= date('Y') ?>
                  </div>
              </div>
          </div>
            <div class="col-12 col-lg-12 d-flex socials-wrapper">
              <div class="col" id="social-icons">
                  <?php if (get_option('p_instagram_link')) { ?>
                    <div class="social-icon">
                      <a href="<?= get_option('p_instagram_link') ?>"
                         target="_blank"
                         aria-label="Instagram">

                        <i class="fab fa-instagram"></i>
                      </a>
                    </div>
                  <?php } ?>
                  <?php if (get_option('p_facebook_link')) { ?>
                    <div class="social-icon">
                      <a href="<?= get_option('p_facebook_link') ?>"
                         target="_blank"
                         aria-label="Facebook">
                        <i class="fab fa-facebook-f"></i>
                      </a>
                    </div>
                  <?php } ?>
                  <?php if (get_option('p_linkedin_link')) { ?>
                    <div class="social-icon">
                      <a href="<?= get_option('p_linkedin_link') ?>"
                         target="_blank"
                         aria-label="Linkedin">
                        <i class="fab fa-linkedin-in"></i>
                      </a>
                    </div>
                  <?php } ?>
                  <?php if (get_option('p_x-twitter_link')) { ?>
                      <div class="social-icon pb-3">
                          <a href="<?= get_option('p_x-twitter_link') ?>"
                             target="_blank"
                             aria-label="X twitter">
                              <i class="fa-brands fa-square-x-twitter"></i>
                          </a>
                      </div>
                  <?php } ?>
                  <?php if (get_option('p_youtube_link')) { ?>
                      <div class="social-icon pb-3">
                          <a href="<?= get_option('p_youtube_link') ?>"
                             target="_blank"
                             aria-label="Youtube">
                              <i class="fa-brands fa-youtube"></i>
                          </a>
                      </div>
                  <?php } ?>
              </div>
            </div>
        </div>
          <div class="col-6 <?php if ($badges) { ?>col-lg-2<?php } else { ?>col-lg-3<?php } ?> ps-0 d-flex order-1 order-md-1">
              <div class="col-12 d-flex contact-wrapper">
                  <div class="col-12" id="footer-contact">
                      <?php if (pll__('contact')) { ?>
                          <h6><?php pll_e('contact') ?></h6>
                      <?php } ?>
                      <div class="col-12 contact-information">
                          <p>
                              <?php pll_e('street-and-number'); ?>
                          </p>
                          <p>
                              <?php pll_e('postcode-and-location'); ?>
                          </p>
                          <p>
                              <?php pll_e('country'); ?>
                          </p>
                          <p>
                              <?php if (pll__('human-phone-number')) { ?>
                                  <a href="tel:<?php pll_e('machine-phone-number'); ?>">
                                      <?php pll_e('human-phone-number'); ?>
                                  </a>
                              <?php } ?>
                          </p>
                          <p>
                              <?php if (pll__('human-email-address')) { ?>
                                  <a href="mailto:<?php pll_e('machine-email-address'); ?>">
                                      <?php pll_e('human-email-address'); ?>
                                  </a>
                              <?php } ?>
                          </p>
                          <?php if (pll__('VAT-number')) { ?>
                              <p>
                                  <?php pll_e('VAT-number'); ?>
                              </p>
                          <?php } ?>
                      </div>
                  </div>
              </div>
          </div>
      <div class="col-12 <?php if ($badges) { ?>col-lg-4<?php } else { ?>col-lg-6<?php } ?> d-flex order-2 order-md-2">
        <div id="footer-nav-pages" class="col">
          <h6><?php pll_e('links-to-pages') ?></h6>
            <?php
            wp_nav_menu([
                'theme_location'  => 'footer-menu',
                'menu_class'      => 'footer-menu-links',
                'container_id'    => 'footerMenuPages',
                'container_class' => 'col-12 footer-nav-links',
                'bootstrap'       => true,
            ]);
            ?>
        </div>
        <div id="footer-nav-policies" class="col">
          <h6><?php pll_e('links to policies'); ?></h6>
            <?php
            wp_nav_menu([
                'theme_location'  => 'footer-menu-policies',
                'menu_class'      => 'footer-menu-links',
                'container_id'    => 'footerMenuPolicies',
                'container_class' => 'col-12 footer-nav-links',
                'bootstrap'       => true,
            ]);
            ?>
        </div>
      </div>
        <?php if ($badges) { ?>
          <div class="col-12 col-lg-3 order-3">
              <?php if (count($badges)) { ?>
                <div class="col-12 certs-org-title">
                  <h6><?= pll__('certificates-and-organisations') ?></h6>
                </div>
              <?php } ?>
            <div class="col-12 d-flex certs-org-logo-wrapper">
                <?php foreach ($badges as $badge) { ?>
                  <figure class="col-5 footer-certs-org-logo">
                      <?php if (get_field('url', $badge)) { ?>
                        <a href="<?= get_field('url', $badge) ?>" target="_blank"></a>
                      <?php } ?>
                    <img class="d-inline-block align-top"
                         src="<?= get_field('image', $badge) ?: 'https://via.placeholder.com/320x240?text='.get_the_title($badge)  ?>"
                         alt='<?= get_the_title($badge) ?>'>
                  </figure>
                <?php } ?>
            </div>
          </div>
        <?php } ?>
    </div>
      <?php endif; ?>
</footer>
<?php if (get_option('p_branding_bar_show')) { ?>
<div class="branding-information col-lg-12 col-md-12 text-center container-fluid py-4">
    <p class="d-flex justify-content-center align-items-center">
        Stirred by
        <a href="https://contentcoffee.com/" class="mx-3" target="_blank">
            <img src="<?= get_template_directory_uri() . '/assets/img/logos/logo_cc.svg'; ?>" alt="Blends Logo">
        </a>
        crafted with
        <a href="https://blends.cloud/" class="mx-3" target="_blank">
            <img src="<?= get_template_directory_uri() . '/assets/img/logos/logo_blends.png'; ?>" alt="Blends Logo">
        </a>
    </p>
</div>
<?php } ?>
<?php wp_footer(); ?>
<?php echo get_option('p_footer_raw'); ?>

</body>
</html>
