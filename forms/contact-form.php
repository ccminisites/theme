<div class="contact-form">
    <form action="" id="contact-form">
        <input type="hidden" name="form_id" value="contact">
        <input type="hidden" name="locale" value="<?= get_locale() ?>">
        <div class="row">
            <div class="col-12 col-lg-6 contact-information">
                <div class="form-group">
                    <label class="d-none" for="fullname"><?= pll__('full_name') ?> *</label>
                    <input id="fullname" type="text" class="form-control" placeholder="<?= pll__('Full Name') ?>*" required name="fullname">
                </div>
                <div class="form-group">
                    <label class="d-none" for="company"><?= pll__('company') ?> *</label>
                    <input id="company" type="text" class="form-control" placeholder="<?= pll__('Company') ?>*" required name="company">
                </div>
                <div class="form-group">
                    <label class="d-none" for="phone"><?= pll__('phone') ?></label>
                    <input id="phone" type="tel" class="form-control" placeholder="<?= pll__('Phone') ?>" name="phone">
                </div>
                <div class="form-group">
                    <label class="d-none" for="email"><?= pll__('email') ?> *</label>
                    <input id="email" type="email" class="form-control" placeholder="<?= pll__('Email') ?>*" required name="email">
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <label class="d-none" for="message"><?= pll__('message') ?> *</label>
                    <textarea id="message" name="message" placeholder="<?= pll__('Message') ?>*" class="form-control" rows="10"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group text-start">
                    <button type="submit" id="contactSubmit" value="Send" class="btn btn-primary col-auto"><?= pll__('contact-submit') ?></button>
                </div>
            </div>
        </div>
    </form>
    <div id="contact-spinner" class="d-none text-center">
        <?= pll__('loading') ?><br />
        <i class="fas fa-spin fa-circle-notch"></i>
    </div>
    <div id="contact-thankyou" class="d-none">
        <b><?= pll__('thank-you-for-getting-in-touch') ?></b>
    </div>
</div>
<script>
    // Submit a form using JS not the traditional way ;)
    // Instead of click you will use submit
    jQuery('#contact-form').on('submit', function(e){

        // Don't actually submit the form.
        e.preventDefault();

        // Gather the data in the form.
        // For real you probably will user jQuery('#testform').serialize()
        var data = jQuery('#contact-form').serialize();

        jQuery('#contact-form').addClass('d-none');
        jQuery('#contact-spinner').removeClass('d-none');
        // This would be a great place to hide your form and show a spinner

        // Post the data of the form to ccforms plugin.
        jQuery.post('/wp-json/project/v1/form', data, function (response) {
            //console.log(response);
            // This would be a great place to stop showing your spinner and instead show a thank you.
            // Or error message...
            jQuery('#contact-spinner').addClass('d-none');
            jQuery('#contact-thankyou').removeClass('d-none');
        });

    });
</script>
