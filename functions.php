<?php

// Add the title tag to the wp_head()
use Rapidmail\ApiClient\Client;
use Rapidmail\ApiClient\Exception\ApiException;
use Rapidmail\ApiClient\Exception\IncompatiblePlatformException;

add_theme_support('title-tag');
add_theme_support('post-thumbnails');

// Include stuff to help us out.
require_once get_template_directory() . '/includes/scripts-and-styles.php';
require_once get_template_directory() . '/includes/breadcrumbs.php';
require_once get_template_directory() . '/includes/helpers.php';
require_once get_template_directory() . '/includes/BootstrapNavMenuWalkerDropdown.php';
require_once get_template_directory() . '/includes/BootstrapNavMenuWalkerDropstart.php';
require_once get_template_directory() . '/includes/menus.php';
require_once get_template_directory() . '/includes/shortcodes.php';
require_once get_template_directory() . '/includes/lazyblocks.php';
require_once get_template_directory() . '/includes/handlebars-helpers.php';
require_once get_template_directory() . '/includes/editor.php';
require_once get_template_directory() . '/includes/sidebars.php';
require_once get_template_directory() . '/includes/blog.php';


/////////// CUSTOM STUFF GOES BELOW ///////////

// Please, when you see that some functions or code is grouped,
// put that into an include.


add_filter('get_site_icon_url', 'project_site_icon_url', 100, 3);
function project_site_icon_url($url, $size, $blog_id)
{
    if (!$url) {
        return get_template_directory_uri() . '/assets/img/icons/eu.png';
    }

    return $url;
}

if ( get_template_directory() != get_stylesheet_directory()) {
    if (file_exists(get_stylesheet_directory() . '/functions.php')) {
        require_once get_stylesheet_directory() . '/functions.php';
    }
}

/**
 * Turn the pll cookie off when doing a parkit.
 * This way we don't have to do the cookie banner because no cookie are being sent at all.
 *
 * @param $seconds
 *
 * @return int|mixed
 */
function project_pll_cookie_expiration($seconds)
{
    if (get_option('p_parkit_on')) {
        $seconds = -1;
    }
    return $seconds;
}
add_filter('pll_cookie_expiration', 'project_pll_cookie_expiration', 100, 1);
