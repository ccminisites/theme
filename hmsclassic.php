<?php
//to override this template, create a file named hmsclassic.php within your active theme folder. you should probably copy and paste this file's code as a starting point.
?>
<?php if ($mobile_friendly) : ?>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
<?php endif ?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style>
        :root {
            --primary-font-color: <?= get_option('p_primary_font_color') ? get_option('p_primary_font_color') : 'black' ?>;
            --secondary-font-color: <?= get_option('p_secondary_font_color') ? get_option('p_secondary_font_color') : 'white' ?>;
            --primary-color: <?= get_option('p_primary_color') ? get_option('p_primary_color') : 'darkblue' ?>;
            --secondary-color: <?= get_option('p_secondary_color') ? get_option('p_secondary_color') : 'blue' ?>;
            --supporting-color: <?= get_option('p_supporting_color') ? get_option('p_supporting_color') : 'grey' ?>;
            --primary-link-color: <?= get_option('p_primary_link_color') ? get_option('p_primary_link_color') : 'lightblue' ?>;
            --primary-link-hover-color: <?= get_option('p_primary_link_hover_color') ? get_option('p_primary_link_hover_color') : 'blue' ?>;
            --secondary-link-color: <?= get_option('p_secondary_link_color') ? get_option('p_secondary_link_color') : 'red' ?>;
            --secondary-link-hover-color: <?= get_option('p_secondary_link_hover_color') ? get_option('p_secondary_link_hover_color') : 'orange' ?>;
            --border-radius: <?= get_option('p_border_radius') !== '' ? get_option('p_border_radius') : '0.5' ?>rem;
            --primary-font: 'Inter', Helvetica, Arial, sans-serif;
            --secondary-font: <?= get_option('p_secondary_font') ? get_option('p_secondary_font') : "'Nunito', Helvetica, Arial, sans-serif" ?>;
            --secondary-font-weight: <?= get_option('p_secondary_font_weight') ? get_option('p_secondary_font_weight') : '700' ?>;
        }
    </style>
<link href="<?= get_template_directory_uri()?>/css/custom.css" rel="stylesheet" >
<body>
<?php echo $messagehtml ?>
<div class="container">
    <div class="row justify-content-center" style="min-height: 100vh">
        <div class="col-12 col-lg-4 col-md-6 d-flex wrap text-center">
            <div id='form_wrap' class="form-group flex-centered w-100">
                <form method=post class="w-100">
                    <input type=password name='hwsp_motech' placeholder='Password' class='form-control enter_password mb-4'>
                    <?php echo $hinthtml ?>
                    <button type=submit class="btn btn-primary mt-4">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
