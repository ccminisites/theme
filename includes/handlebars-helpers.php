<?php
/**
 * @file Handlebars helpers should be placed in here.
 */

/**
 * Register Handlebars Helpers.
 *
 * @param \Handlebars\Handlebars $handlebars
 */
function project_custom_lazyblock_handlebars_helper(Handlebars\Handlebars $handlebars)
{
    // Include another handlebars template.
    $handlebars->registerHelper('include', function ($file, $args) use ($handlebars) {
        $file    = str_replace("..", "", $file);
        $file    = dirname(__FILE__) . '/lazy-blocks/' . $file . '.php';
        $content = file_exists($file) ? file_get_contents($file) : '';

        return $handlebars->render($content, $args['context']);
    });

    // Dump a variable.
    $handlebars->registerHelper('dump', function ($var, $args) use ($handlebars) {
        return '<pre>' . print_r($var, true) . '</pre>';
    });

    // Return the uniqiue blockId of the block we are in.
    $handlebars->registerHelper('blockid', function ($args) use ($handlebars) {
        /** @var \Handlebars\Context $samantha */
        $samantha = $args['context'];

        return $samantha->get('blockId');
    });

    // Kinda sorta do the PHP coalesce trick.
    $handlebars->registerHelper('coalesce', function ($var1, $var2, $var3, $args) use ($handlebars) {
        $out = $var1 ? $var1 : $var2;
        $out = $out ? $out : $var3;

        return $out;
    });

    // Add one to a number.
    $handlebars->registerHelper('plusone', function ($var1, $args) use ($handlebars) {
        return (int)$var1 + 1;
    });

    // Return back the columns left from 12.
    $handlebars->registerHelper('leftover', function ($var1, $args) use ($handlebars) {
        return 12 - (int)$var1;
    });

    // Return back half of the columns left over from 12.
    $handlebars->registerHelper('halfleftover', function ($var1, $args) use ($handlebars) {
        return (int)(12 - (int)$var1) / 2;
    });

    // Return a slug of a string.
    // Caution: Requires ACF.
    $handlebars->registerHelper('slug', function ($var1, $args) use ($handlebars) {
        return acf_slugify($var1);
    });

    // Return a translated string.
    $handlebars->registerHelper('pll', function ($var1, $args) use ($handlebars) {
        return pll__($var1);
    });



    // Give back the number of columns a thing should have based on the number of things in an array.
    // For ex, you have 3 things in an array, well son you need to have col-lg-4...
    $handlebars->registerHelper('colIt', function ($var1, $args) use ($handlebars) {
        $count = 1;
        if (is_array($var1)) {
            $count = count($var1);
        }

        if ($count == 1 || $count == 0) {
            return 12;
        }

        if ($count == 2) {
            return 6;
        }

        if ($count == 3) {
            return 4;
        }

        if ($count == 4) {
            return 3;
        }

        if ($count == 5 || $count == 6) {
            return 2;
        }

        return 1;
    });

    // Give the link to the privacy page.
    // Helpful in forms.
    $handlebars->registerHelper('privacyurl', function ($args) use ($handlebars) {
        return get_privacy_policy_url();
    });

    // Give the current ISO 2 letter code.
    $handlebars->registerHelper('locale', function ($args) use ($handlebars) {
        return get_locale();
    });

    // Does this equal that?
    $handlebars->registerHelper('ifEquals', function ($var1, $var2, $args) use ($handlebars) {
        if ($var1 == $var2) {
            return $args['fn']();
        }

        return false;
    });

    // Does this not equal that?
    $handlebars->registerHelper('ifNotEquals', function ($var1, $var2, $args) use ($handlebars) {
        if ($var1 != $var2) {
            return $args['fn']();
        }

        return false;
    });

    // URL encoder
    $handlebars->registerHelper('urlEncode', function ($var1) use ($handlebars) {
            return urlencode($var1);

    });

    // Img Tags
    $handlebars->registerHelper('alternativetag', function ($var, $args) use ($handlebars) {
        return isset($var) ? $var = get_post_meta($var, '_wp_attachment_image_alt', true) : get_the_title();
    });
}


if (function_exists('lazyblocks')) {
    // This line calls the function, so keep it.
    add_action('lzb/handlebars/object', 'project_custom_lazyblock_handlebars_helper');
}
