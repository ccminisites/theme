<?php
/**
 * @file Add any loose function that just helps you here.
 */

/**
 * Take a string, and truncate to 15 words.
 * @param $str
 * @param int $limit
 *
 * @return string
 */
function project_truncate($str, $limit = 30)
{
    $str = strip_tags($str);
    $diacritics = '0123456789!@#$%^&*()_+-=./,<>?;:\'"aàȁáâǎãāăȃȧäåẚảạḁąᶏậặầằắấǻẫẵǡǟẩẳⱥæǽǣᴂꬱꜳꜵꜷꜹꜻꜽɐɑꭤᶐꬰɒͣᵃªᵄᵆᵅᶛᴬᴭᴀᴁₐbḃƅƀᵬɓƃḅḇᶀꞗȸßẞꞵꞛꞝᵇᵝᴮᴯʙᴃᵦcćĉčċƈçḉɕꞔꞓȼ¢ʗᴐᴒɔꜿᶗꝢꝣ©ͨᶜᶝᵓᴄdďḋᵭðđɗᶑḓḍḏḑᶁɖȡꝱǳʣǆʤʥȸǲǅꝺẟƍƌͩᵈᶞᵟᴰᴅᴆeèȅéēêěȇĕẽėëẻḙḛẹȩęᶒⱸệḝềḕếḗễểɇəǝɘɚᶕꬲꬳꬴᴔꭁꭂ•ꜫɛᶓȝꜣꝫɜᴈᶔɝɞƩͤᵉᵊᵋᵌᶟᴱᴲᴇⱻₑₔfẜẝƒꬵḟẛᶂᵮꞙꝭꝼʩꟻﬀﬁﬂﬃﬄᶠꜰgǵḡĝǧğġģǥꬶᵷɡᶃɠꞡᵍᶢᴳɢʛhħĥȟḣḧɦɧḫḥẖḩⱨꜧꞕƕɥʮʯͪʰʱꭜᶣᵸꟸᴴʜₕiìȉíīĩîǐȋĭïỉɨḭịįᶖḯıɩɪꭠꭡᴉᵻᵼĳỻİꟾꟷͥⁱᶤᶦᵎᶧᶥᴵᵢjȷĵǰɉɟʝĳʲᶡᶨᴶᴊⱼkḱǩꝁꝃꝅƙḳḵⱪķᶄꞣʞĸᵏᴷᴋₖlĺľŀłꝉƚⱡɫꬷꬸɬꬹḽḷḻļɭȴᶅꝲḹꞎꝇꞁỻǈǉʪʫɮˡᶩᶪꭝꭞᶫᴸʟᴌₗmḿṁᵯṃɱᶆꝳꬺꭑᴟɯɰꟺꟿꟽͫᵐᶬᶭᴹᴍₘnǹńñňŉṅᵰṇṉṋņŋɳɲƞꬻꬼȵᶇꝴꞃꞑꞥᴝᴞǋǌⁿᵑᶯᶮᶰᴺᴻɴᴎₙoᴏᴑòȍóǿőōõôȏǒŏȯöỏơꝍọǫⱺꝋɵøᴓǭộợồṑờốṍṓớỗỡṏȭȱȫổởœɶƣɸƍꝏʘꬽꬾꬿꭀꭁꭂꭃꭄꭢꭣ∅ͦᵒᶱºꟹᶲᴼᴽₒpṕṗꝕꝓᵽᵱᶈꝑþꝥꝧƥƪƿȹꟼᵖᴾᴘᴩᵨₚqʠɋꝙꝗȹꞯʘθᶿrŕȑřȓṙɍᵲꝵꞧṛŗṟᶉꞅɼɽṝɾᵳᴦɿſⱹɹɺɻ®ꝶꭇꭈꭉꭊꭋꭌͬʳʶʴʵᴿʀʁᴙᴚꭆᵣsśŝšṡᵴꞩṣşșȿʂᶊṩṥṧƨʃʄʆᶋᶘꭍʅƪﬅﬆˢᶳᶴꜱₛtťṫẗƭⱦᵵŧꝷṱṯṭţƫʈțȶʇꞇꜩʦʧʨᵺͭᵗᶵᵀᴛₜuùȕúűūũûǔȗŭüůủưꭒʉꞹṷṵụṳųᶙɥựǜừṹǘứǚữṻǖửʊᵫᵿꭎꭏꭐꭑͧᵘᶶᶷᵙᶸꭟᵁᴜᵾᵤvṽⱱⱴꝟṿᶌʋʌͮᵛⱽᶹᶺᴠᵥwẁẃŵẇẅẘⱳẉꝡɯɰꟽꟿʍʬꞶꞷʷᵚᶭᵂᴡxẋẍᶍ×ꭓꭔꭕꭖꭗꭘꭙˣ˟ᵡₓᵪyỳýȳỹŷẏÿẙỷƴɏꭚỵỿɣɤꝩʎƛ¥ʸˠᵞʏᵧzźẑžżƶᵶẓẕʐᶎʑȥⱬɀʒǯʓƺᶚƹꝣᵹᶻᶼᶽᶾᴢᴣ';
    $words = str_word_count($str, 1, $diacritics);
    $ellipse = count($words) > $limit ? '...' : '';

    return implode(' ', array_slice($words, 0, $limit)) . $ellipse;
}

/**
 * Take a string, and truncate to 15 words.
 * @param $str
 * @param int $limit
 *
 * @return string
 */
function project_truncate_words($str, $limit = 70)
{
    $str = strip_tags($str);
    $diacritics = '0123456789!@#$%^&*()_+-=./,<>?;:\'"aàȁáâǎãāăȃȧäåẚảạḁąᶏậặầằắấǻẫẵǡǟẩẳⱥæǽǣᴂꬱꜳꜵꜷꜹꜻꜽɐɑꭤᶐꬰɒͣᵃªᵄᵆᵅᶛᴬᴭᴀᴁₐbḃƅƀᵬɓƃḅḇᶀꞗȸßẞꞵꞛꞝᵇᵝᴮᴯʙᴃᵦcćĉčċƈçḉɕꞔꞓȼ¢ʗᴐᴒɔꜿᶗꝢꝣ©ͨᶜᶝᵓᴄdďḋᵭðđɗᶑḓḍḏḑᶁɖȡꝱǳʣǆʤʥȸǲǅꝺẟƍƌͩᵈᶞᵟᴰᴅᴆeèȅéēêěȇĕẽėëẻḙḛẹȩęᶒⱸệḝềḕếḗễểɇəǝɘɚᶕꬲꬳꬴᴔꭁꭂ•ꜫɛᶓȝꜣꝫɜᴈᶔɝɞƩͤᵉᵊᵋᵌᶟᴱᴲᴇⱻₑₔfẜẝƒꬵḟẛᶂᵮꞙꝭꝼʩꟻﬀﬁﬂﬃﬄᶠꜰgǵḡĝǧğġģǥꬶᵷɡᶃɠꞡᵍᶢᴳɢʛhħĥȟḣḧɦɧḫḥẖḩⱨꜧꞕƕɥʮʯͪʰʱꭜᶣᵸꟸᴴʜₕiìȉíīĩîǐȋĭïỉɨḭịįᶖḯıɩɪꭠꭡᴉᵻᵼĳỻİꟾꟷͥⁱᶤᶦᵎᶧᶥᴵᵢjȷĵǰɉɟʝĳʲᶡᶨᴶᴊⱼkḱǩꝁꝃꝅƙḳḵⱪķᶄꞣʞĸᵏᴷᴋₖlĺľŀłꝉƚⱡɫꬷꬸɬꬹḽḷḻļɭȴᶅꝲḹꞎꝇꞁỻǈǉʪʫɮˡᶩᶪꭝꭞᶫᴸʟᴌₗmḿṁᵯṃɱᶆꝳꬺꭑᴟɯɰꟺꟿꟽͫᵐᶬᶭᴹᴍₘnǹńñňŉṅᵰṇṉṋņŋɳɲƞꬻꬼȵᶇꝴꞃꞑꞥᴝᴞǋǌⁿᵑᶯᶮᶰᴺᴻɴᴎₙoᴏᴑòȍóǿőōõôȏǒŏȯöỏơꝍọǫⱺꝋɵøᴓǭộợồṑờốṍṓớỗỡṏȭȱȫổởœɶƣɸƍꝏʘꬽꬾꬿꭀꭁꭂꭃꭄꭢꭣ∅ͦᵒᶱºꟹᶲᴼᴽₒpṕṗꝕꝓᵽᵱᶈꝑþꝥꝧƥƪƿȹꟼᵖᴾᴘᴩᵨₚqʠɋꝙꝗȹꞯʘθᶿrŕȑřȓṙɍᵲꝵꞧṛŗṟᶉꞅɼɽṝɾᵳᴦɿſⱹɹɺɻ®ꝶꭇꭈꭉꭊꭋꭌͬʳʶʴʵᴿʀʁᴙᴚꭆᵣsśŝšṡᵴꞩṣşșȿʂᶊṩṥṧƨʃʄʆᶋᶘꭍʅƪﬅﬆˢᶳᶴꜱₛtťṫẗƭⱦᵵŧꝷṱṯṭţƫʈțȶʇꞇꜩʦʧʨᵺͭᵗᶵᵀᴛₜuùȕúűūũûǔȗŭüůủưꭒʉꞹṷṵụṳųᶙɥựǜừṹǘứǚữṻǖửʊᵫᵿꭎꭏꭐꭑͧᵘᶶᶷᵙᶸꭟᵁᴜᵾᵤvṽⱱⱴꝟṿᶌʋʌͮᵛⱽᶹᶺᴠᵥwẁẃŵẇẅẘⱳẉꝡɯɰꟽꟿʍʬꞶꞷʷᵚᶭᵂᴡxẋẍᶍ×ꭓꭔꭕꭖꭗꭘꭙˣ˟ᵡₓᵪyỳýȳỹŷẏÿẙỷƴɏꭚỵỿɣɤꝩʎƛ¥ʸˠᵞʏᵧzźẑžżƶᵶẓẕʐᶎʑȥⱬɀʒǯʓƺᶚƹꝣᵹᶻᶼᶽᶾᴢᴣ';
    $words = str_word_count($str, 1, $diacritics);
    $return = [];
    $cur = 0;
    while(!empty($words) && $cur < $limit) {
        $nextWord = array_shift($words);
        if ($cur + strlen($nextWord) <= $limit) {
            $cur += strlen($nextWord);
            $return[] = $nextWord;
        }
        else {
            array_unshift($words, $nextWord);
            break;
        }

    }
    $ellipse = !empty($words) ? '...' : '';

    return implode(' ', $return) . $ellipse;
}

/**
 * @param string $raw_content
 * @return string
 */
function create_fallback_string($raw_content): string
{
    $content = apply_filters('the_content', $raw_content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = explode(PHP_EOL, trim(strip_tags($content)));
    return reset($content);
}


/**
 * Just get all the months into string translations but also a nice function to convert
 * a numbered month to the full text translated version.
 *
 * @param int $month
 *
 * @return mixed|string
 */
function project_month(int $month)
{
    $months = [];
    $months[1] = pll__('january');
    $months[2] = pll__('february');
    $months[3] = pll__('march');
    $months[4] = pll__('april');
    $months[5] = pll__('may');
    $months[6] = pll__('june');
    $months[7] = pll__('july');
    $months[8] = pll__('august');
    $months[9] = pll__('september');
    $months[10] = pll__('october');
    $months[11] = pll__('november');
    $months[12] = pll__('december');
    return isset($months[$month]) ? $months[$month] : '';
}

/**
 * Just get all the months into string translations
 * and make a nice littel function to return the short form of a month name.
 *
 * @param int $month
 *
 * @return mixed|string
 */
function project_short_month(int $month)
{
    $months = [];
    $months[1] = pll__('jan');
    $months[2] = pll__('feb');
    $months[3] = pll__('mar');
    $months[4] = pll__('apr');
    $months[5] = pll__('may');
    $months[6] = pll__('jun');
    $months[7] = pll__('jul');
    $months[8] = pll__('aug');
    $months[9] = pll__('sep');
    $months[10] = pll__('oct');
    $months[11] = pll__('nov');
    $months[12] = pll__('dec');
    return isset($months[$month]) ? $months[$month] : '';
}


function project_check_spam($content)
{
    // innocent until proven guilty
    $isSpam  = false;
    $content = (array)$content;

    if (function_exists('akismet_init')) {
        $wpcom_api_key = get_option('wordpress_api_key') ? get_option('wordpress_api_key') : 'd6d876515497';

        if ( ! empty($wpcom_api_key)) {
            $akismet_api_host = $wpcom_api_key . '.rest.akismet.com';
            $akismet_api_port = 80;

            // set remaining required values for akismet api
            $content['user_ip']    = preg_replace('/[^0-9., ]/', '', $_SERVER['REMOTE_ADDR']);
            $content['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
            $content['referrer']   = $_SERVER['HTTP_REFERER'];
            $content['blog']       = get_option('home');

            if (empty($content['referrer'])) {
                $content['referrer'] = get_permalink();
            }

            $queryString = '';

            foreach ($content as $key => $data) {
                if ( ! empty($data)) {
                    $queryString .= $key . '=' . urlencode(stripslashes($data)) . '&';
                }
            }

            $response = akismet_http_post($queryString, $akismet_api_host, '/1.1/comment-check', $akismet_api_port);

            if (isset($response[1]) && $response[1] === 'true') {
                $isSpam = true;
            }
        }
    }

    return $isSpam;
}
