<?php
/**
 * @file In this file we register the lazyblocks that are to be used.
 */


/**
 * Bring in all the lazy locks that we have.
 */
function project_load_my_lazy_blocks()
{
    if (function_exists('lazyblocks')) {
        $blocks = [];

        // Why don't we just do a glob and then include?
        // Well globs cost time and we can solve this by just not being lazy.
        // Irony...

        // Create blocks in the lazy blocks directory,
        // Then add a line here.
        $blocks[] = 'alert';
        $blocks[] = 'heading-1';
        $blocks[] = 'heading-2';
        $blocks[] = 'heading-3';
        $blocks[] = 'section';
        $blocks[] = 'content';
        $blocks[] = 'spacer';
        $blocks[] = 'cta';
        $blocks[] = 'cards';
        $blocks[] = 'divider';
        $blocks[] = 'image';
        $blocks[] = 'image-image';
        $blocks[] = 'forms';
        $blocks[] = 'fly-in-section';
        $blocks[] = 'form';
        $blocks[] = 'svg';
        $blocks[] = 'content-image';
        $blocks[] = 'content-content';
        $blocks[] = 'twitter-wall';
        $blocks[] = 'subscribe-form';
        $blocks[] = 'jumbotron';
        $blocks[] = 'banner';
        $blocks[] = 'embed';
        $blocks[] = 'content-video';
        $blocks[] = 'accordion';
        $blocks[] = 'carousel';
        $blocks[] = 'google-map';
        $blocks[] = 'subscribe-form';
        $blocks[] = 'teasers';
        $blocks[] = 'news';
        $blocks[] = 'gallery';
        $blocks[] = 'category';
        $blocks[] = 'breadcrumbs';
        $blocks[] = 'pricing-table';
        $blocks[] = 'testimonial';
        $blocks[] = 'icon-box';
        $blocks[] = 'animated-text';
        $blocks[] = 'countdown';
        $blocks[] = 'number-box';

        // These blocks depend on post types, we will filter in the allowed blocks hook.
        $blocks[] = 'events';
        $blocks[] = 'past-events';
        $blocks[] = 'jobs';
        $blocks[] = 'projects';
        $blocks[] = 'members';
        
        $additionalFile = ABSPATH . 'wp-content/additional-lazy-blocks.php';
        if (file_exists($additionalFile)) {
            include_once $additionalFile;
        }

      // Let's ROCK!
        foreach ($blocks as $slug) {
          // Each block that we make will not have a wrapper.
          // We don't want to get into an endless DIV game in the html.
          // Plus we want to have control of the class name.
            add_filter('lazyblock/' . $slug . '/allow_wrapper', function ($a, $b) {
                return false;
            }, 10, 2);
          // Require the lazyblock, block code.
            if ( get_template_directory() != get_stylesheet_directory()) {
                if (file_exists(get_stylesheet_directory() . '/lazy-blocks/' . $slug . '/' . $slug . '-block.php')) {
                    require_once get_stylesheet_directory() . '/lazy-blocks/' . $slug . '/' . $slug . '-block.php';
                } else {
                    require_once __DIR__ . '/../lazy-blocks/' . $slug . '/' . $slug . '-block.php';
                }
            } else {
                require_once __DIR__ . '/../lazy-blocks/' . $slug . '/' . $slug . '-block.php';
            }
        }
    }
}

// This calls the function, so leave it.
add_action('lzb/init', 'project_load_my_lazy_blocks', 100);



/**
 * Only allow the lazyblocks. We do not want uknown blocks littering our project.
 *
 * @param $allowed_blocks
 *
 * @return array
 */
function project_allowed_block_types_all($allowed_blocks)
{
    // Add non lazy blocks here that should be allowed, ideally none, however you never know.
    $allowed_blocks = [];
    //$allowed_blocks[] = 'core/paragraph';

    // Add any blocks to this that should be take out, even lazy blocks.
    $disallowed_blocks = [];


    if (!post_type_exists('event')) {
        $disallowed_blocks[] = 'lazyblock/events';
        $disallowed_blocks[] = 'lazyblock/past-events';
    }
    if (!post_type_exists('job')) {
        $disallowed_blocks[] = 'lazyblock/jobs';
    }
    if (!post_type_exists('project')) {
        $disallowed_blocks[] = 'lazyblock/projects';
    }
    if (!post_type_exists('member')) {
        $disallowed_blocks[] = 'lazyblock/members';
    }

    $block_types = WP_Block_Type_Registry::get_instance()->get_all_registered();
    foreach ($block_types as $name => $block_type) {
        if (preg_match("/^lazyblock/", $name) && !in_array($name, $disallowed_blocks)) {
            $allowed_blocks[] = $name;
        }
    }

    return $allowed_blocks;
}

// This calls the function, so leave it.
add_filter('allowed_block_types_all', 'project_allowed_block_types_all', 100);
