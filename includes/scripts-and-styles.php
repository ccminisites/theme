<?php
/**
 * @file Regsiter the scripts and styles for the project.
 */

/**
 * Register the scripts and styles for the project front end.
 */
function project_add_scripts_and_styles()
{
    wp_enqueue_style('style-parent', get_stylesheet_directory_uri() . '/style.css', ['bootstrap']);
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('inputmask', get_template_directory_uri() . '/css/inputmask.css');
    wp_enqueue_style('dashicons');

    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery.js', [], false, true);
    wp_enqueue_script('jqueryinputmask', get_template_directory_uri() . '/js/jquery.inputmask.min.js', [], false, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', [], false, true);
    wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/4d5e89b640.js', [], false, true);
    wp_enqueue_script('slick.js', get_template_directory_uri() . '/libs/slick/slick.min.js', [], false, false);
    wp_enqueue_script('project', get_template_directory_uri() . '/js/project.js', [], false, true);

}

add_action('wp_enqueue_scripts', 'project_add_scripts_and_styles');

/**
 * Add scripts and styles just in the admin.
 */
function project_admin_enqueue_scripts()
{
}

add_action('admin_enqueue_scripts', 'project_admin_enqueue_scripts');
