<?php
/**
 * @file Shortcodes go in this file.
 */

/**
 * Add a shortcode to do font awesome icons
 *
 * @param $params
 *
 * @return string
 */
function fa_shortcode($params)
{
    return '<i class="' . $params['class'] . '"></i>';
}

add_shortcode('fa', 'fa_shortcode');

/**
 * Add a shortcode to do a regular badge
 * @see https://getbootstrap.com/docs/5.2/components/badge/
 *
 * @param $params
 *
 * @return string
 */
function badge_shortcode($params, $content)
{
    $supportedColors = ['primary', 'secondary', 'supporting', 'white'];
    $textColor = (isset($params['text_color']) && in_array($params['text_color'], $supportedColors)) ? $params['text_color'] : 'primary';
    $backgroundColor = (isset($params['background_color']) && in_array($params['background_color'], $supportedColors)) ? $params['background_color'] : 'primary';
    return '<span class="badge bg-'. $backgroundColor .' text-color-' .$textColor. '">'.esc_attr($content).'</span';
}

add_shortcode('badge', 'badge_shortcode');


/**
 * Add a shortcode to show country flags
 *
 * @param $params
 *
 * @return string
 */
function flag_shortcode($params)
{
    $iso = isset($params['iso']) ? $params['iso'] : 'be';
    $size = isset($params['size']) ? (int)$params['size'] : '24';

    return '<img src="https://lipis.github.io/flag-icon-css/flags/4x3/' . $iso . '.svg" width="' . $size . 'px" />';
}

add_shortcode('flag', 'flag_shortcode');

/**
 * Add a shortcode to show CTAs
 *
 * @param $params
 *
 * @return string
 */
function cta_shortcode($params)
{
    $url = isset($params['url']) ? $params['url'] : '/';
    $label = isset($params['label']) ? $params['label'] : 'cta label needed';
    $newtab = isset($params['newtab']) && $params['newtab'] ? '_blank' : '_self';
    $variant = isset($params['variant']) && $params['variant'] ? 'btn-' . $params['variant'] : 'btn-primary';

    return '<a href="' . $url . '" class="btn ' . $variant . '" target="' . $newtab . '">' . $label . '</a>';
}

add_shortcode('cta', 'cta_shortcode');


/**
 * Show a whos who.
 *
 * @param $params
 *
 * @return string
 */
function whoswho_shortcode($params)
{
    $name = isset($params['name']) ? $params['name'] : 'bob';
    $icon = isset($params['icon']) ? $params['icon'] : 'dummy-man1' or 'dummy-man2';
    $position = isset($params['position']) ? $params['position'] : 'lead bob';

    $out = <<<OUT
<div class="row whoswho pb-4">
  <div class="col-lg-3 whoswhoicon-wrapper">
    <img class="whoswhoicon" src="/wp-content/themes/project/assets/img/icons/$icon.svg" alt="$name" />
  </div>
  <div class="col-lg-9 whoswhoinfo-wrapper">
    <h5 class="whoswho-title">$name</h5>
    <span class="whoswhos-position">
      $position
    </span>
  </div>
</div>
OUT;

    return $out;
}

add_shortcode('whoswho', 'whoswho_shortcode');

/**
 * Put an impressive number on the screen.
 *
 * @param $params
 *
 * @return string
 */
function impressive_shortcode($params)
{
    $number = isset($params['number']) ? $params['number'] : '100';
    $label = isset($params['label']) ? $params['label'] : 'impressive';
    // Top or Bottom for now.
    $supportedLabelPostiions = ['top', 'bottom'];
    $labelPosition = (isset($params['label_position']) && in_array($params['label_position'], $supportedLabelPostiions)) ?
      $params['label_position'] :
      'top';
    // Primary, secondary or supporting
    $supportedColors = ['primary', 'secondary', 'supporting'];
    $numberColor = (isset($params['number_color']) && in_array($params['number_color'], $supportedColors)) ? $params['number_color'] : 'primary';
    $labelColor = (isset($params['label_color']) && in_array($params['label_color'], $supportedColors)) ? $params['label_color'] : 'primary';

    return <<<OUT
        <div class="impressive $labelPosition">
          <div class="impressive-header text-center $labelColor">$label</div>
          <div class="impressive-numbers text-center $numberColor" data-value="$number">$number</div>
        </div>
        OUT;
}

add_shortcode('impressive', 'impressive_shortcode');

/**
 * This is an image with little label below it and when you hover a  GO symbol...
 *
 * @param $params
 *
 * @return string
 */
function goimg_shortcode($params)
{
    $img = isset($params['img']) ? $params['img'] : 'https://placehold.it/200/200';
    $url = isset($params['url']) ? $params['url'] : 'https://www.google.com';
    $label = isset($params['label']) ? $params['label'] : 'go';

    $out = <<<OUT
<div class="goimg">
  <img src="$img" /><br />
  <a href="$url">$label</a>
</div>
OUT;
    return $out;
}

add_shortcode('goimg', 'goimg_shortcode');


/**
 *
 * @param $params
 *
 * @return string
 */
function poi_shortcode($params, $content = null)
{
    $icon = isset($params['icon']) ? $params['icon'] : 'paper';
    $title = isset($params['title']) ? $params['title'] : 'An Interesting Point';
    $out = <<<OUT
<div class="row poi">
  <div class="col-3 poiicon-wrapper">
  <img class="poiicon" src="/wp-content/themes/project/assets/img/icons/$icon" alt="$name" />
  </div>
  <div class="col-9" poicontent-wrapper>
    <h5 class="poititle">$title</h5>
    <div class="poicontent">
      $content;
    </div>
  </div>
</div>
OUT;
    return $out;
}

add_shortcode('poi', 'poi_shortcode');
