<?php

/**
 * Register widget areas.
 *
 * @since Twenty Twenty 1.0
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function project_sidebar_registration() {
    if (function_exists('pll_the_languages')) {
        $languages = pll_the_languages(['raw' => 1]);
        $languageSlugs = [];

        foreach ($languages as $slug => $language) {
            $languageSlugs[] = $slug;
        }

        // Arguments used in all register_sidebar() calls.
        $shared_args = array(
          'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
          'after_title'   => '</h2>',
          'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
          'after_widget'  => '</div></div>',
        );

        foreach ($languageSlugs as $language) {
            // Header.
            register_sidebar(
              array_merge(
                $shared_args,
                array(
                  'name'        => __( 'Header ' .$language, 'project' ),
                  'id'          => 'header-' . $language,
                  'description' => __( 'Widgets in this area will be displayed in the header', 'project' ),
                )
              )
            );

            // Footer.
            register_sidebar(
              array_merge(
                $shared_args,
                array(
                  'name'        => __( 'Footer ' . $language, 'project' ),
                  'id'          => 'footer-' . $language,
                  'description' => __( 'Widgets in this area will be displayed in the footer.', 'project' ),
                )
              )
            );
        }
    }
}

add_action( 'widgets_init', 'project_sidebar_registration' );
