<?php

$seconds_to_cache = 300;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");
get_header();

if (have_posts()) {
    // Load posts loop.
    while (have_posts()) {
        the_post();
        ?>
        <div class="container-fluid px-0 main-content">
            <?php the_content(); ?>
        </div>
        <?php
    }
} else {
    get_404_template();
}

get_footer();
