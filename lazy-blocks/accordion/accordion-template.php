<div class="container-md">
    <div class="row lazyblock-accordion justify-content-center accordions card-group" id="{{#blockid}}">
        {{#each accordion}}
        <div class="col-12 col-md-10 px-md-0 pb-3 accordion">
            <div class="accordion-wrapper card h-100" data-chapterid="">
                <a href="#accordion-{{#blockid}}-{{@index}}" class="accordion-titlelink collapsed"
                   aria-controls="accordion-{{#blockid}}-{{@index}}" aria-expanded="false"
                   data-bs-toggle="collapse">
                    <div class="accordion-title">
                        <h5>{{title}}</h5>
                        <div class="toggle-button">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </a>
                <div class="collapse accordion-content" id="accordion-{{#blockid}}-{{@index}}">
                    {{{content}}}
                </div>
            </div>
        </div>
        {{/each}}
    </div>
</div>
