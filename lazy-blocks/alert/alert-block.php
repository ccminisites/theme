<?php

$blockslug = 'alert';

// Start the block
$block             = [];
$block['title']    = 'Alert';
$block['icon']     = 'dashicons dashicons-warning';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];

// Text
$control                         = [];
$control['label']                = '';
$control['name']                 = 'content';
$control['type']                 = 'classic_editor';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-content-content';

// Add the control to the controls
$controls[$control_id] = $control;

// Select
$control              = [];
$control['label']     = 'Style';
$control['name']      = 'style';
$control['type']      = 'select';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'primary';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Primary',
        'value' => 'alert-primary',
    ],
    [
        'label' => 'Secondary',
        'value' => 'alert-secondary',
    ],
    [
        'label' => 'Success',
        'value' => 'alert-success',
    ],
    [
        'label' => 'Danger',
        'value' => 'alert-danger',
    ],
    [
        'label' => 'Info',
        'value' => 'alert-info',
    ],
    [
        'label' => 'Warning',
        'value' => 'alert-warning',
    ],
];

// Make an id.
$control_id = 'control-content-alignment';

// Add the control to the controls
$controls[$control_id] = $control;

// Select
$control              = [];
$control['label']     = 'Outlined';
$control['name']      = 'outlined';
$control['type']      = 'select';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'no';
$control['child_of']  = '';
$control['choices']   = [
  [
    'label' => 'No',
    'value' => 'no-outline',
  ],
  [
    'label' => 'Yes',
    'value' => 'outlined',
  ],
];
// Make an id.
$control_id = 'control-outlined';

// Add the control to the controls
$controls[$control_id] = $control;

// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
