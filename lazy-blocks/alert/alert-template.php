<div class="container-md">
    <div class="row lazyblock-alert">
        <div class="col-12">
            <div class="alert {{style}} {{outlined}}">{{{content}}}</div>
        </div>
    </div>
</div>
