<?php
/** @var array $attributes */
$containerClass = $attributes['className'] ? $attributes['className'] . '-container' : '';
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        const typewriterItems = document.querySelectorAll(".typewriter-item");
        let currentItemIndex = 0;

        function animateTypewriter() {
            typewriterItems[currentItemIndex].classList.add("typing");
            currentItemIndex = (currentItemIndex + 1) % typewriterItems.length;

            setTimeout(function () {
                typewriterItems.forEach((item) => {
                    item.classList.remove("typing");
                });

                animateTypewriter();
            }, 4000);
        }

        animateTypewriter();
    });
</script>


<div class="container-md lazyblock-animated-text-container <?= $attributes['blockUniqueClass'] ?>-container <?= $containerClass ?>">
    <div class="row lazyblock-content <?= $attributes['className'] ?> <?= $attributes['blockUniqueClass'] ?>">
        <div class="col-12 d-flex text-<?= $attributes['textalignment'] ?>">
            <?= $attributes['main-text'] . '&nbsp;&nbsp;' ?>
            <?php foreach ($attributes['text-items'] as $index => $value) {
                $text_to_animate = $value['text-content'] ?: '';
                ?>

                <div class="typewriter-texts">
                    <span class="typewriter-item typewriter-<?= $index ?>">
                        <?= $text_to_animate ?>
                    </span>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

