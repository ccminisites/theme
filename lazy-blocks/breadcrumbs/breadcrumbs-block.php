<?php

$blockslug = 'breadcrumbs';
$blocktitle = 'Breadcrumbs';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-ellipsis';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = true;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [
    'spacings' => false,
    'display' => false,
    'scrollReveal' => false,
    'frame' => false,
    'customCSS' => false,
];

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];


// START CONTAINER CONTROL
$control = [];
$control['label'] = 'Container';
$control['type'] = 'select';
$control['name'] = 'container';
$control['default'] = 'container-md';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'false';
$control['multiple'] = 'false';
$control['translate'] = 'false';

$choices = [];
$choices = [
    [
        'label' => 'MD',
        'value' => 'container-md',
    ],
    [
        'label' => 'Fluid',
        'value' => 'container-fluid',
    ],
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END CONTAINER CONTROL


// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['show_preview'] = 'never';
$code['single_output'] = false;
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['editor_css'] = '';
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}

