<?php
/** @var array $attributes */
// cleaning and shorthand css classes into simple variables
$containerClass = $attributes['className'] ? $attributes['className'] . '-container' : '';
$containerClasses = $attributes['container'] . ' ' . $attributes['blockUniqueClass'] . '-container' . ' ' . $containerClass;
$blockID = $attributes['blockId'];

function generate_singular_breadcrumb()
{

    if (is_singular()) {
        $separator = ' <i class="fas fa-chevron-right"></i>';

        $home = pll__('Home');
        // current page title
        $current_page_title = get_the_title();

        $breadcrumb = '<li class="breadcrumb"><a href="' . home_url() . '">' . $home . '</a>' . $separator . '</li>';

        $category = get_the_category();
        if (!empty($category)) {
            // get the first category if there are multiple;
            $category_id = $category[0]->cat_ID;
            // use category function and setting the second parameter to false, to exclude the current category ID
            // we will use parents as an call back function when we want to filter the array.
            $parents = get_category_parents($category_id, false, $separator);
            if (!empty($parents)) {
                $parents_array = explode($separator, $parents);
                $parents_array = array_filter($parents_array);
                foreach ($parents_array as $parent) {
                    $breadcrumb .= '<li class="breadcrumb"><a href="' . get_category_link(get_cat_ID($parent)) . '">' . $parent . '</a> ' . $separator . ' </li>';
                }
            }
        }

        // output the current post title
        $breadcrumb .= '<li class="breadcrumb">' . $current_page_title . '</li>';

    }

    return $breadcrumb;
}


?>

<style>

</style>

<div class="<?= $containerClasses; ?>">
    <div class="row lazyblock-breadcrumbs" id="breadcrumbs-<?= $blockID; ?>">
        <div class="col-12">
            <nav aria-label="breadcrumb" class="breadcrumb-navigation pt-4">
                <ol class="breadcrumbs">
                    <?= generate_singular_breadcrumb(); ?>
                </ol>
            </nav>
        </div>
    </div>
</div>

