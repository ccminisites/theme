<?php

$blockslug = 'cards';

// Start the block
$block             = [];
$block['title']    = 'Cards';
$block['icon']     = 'dashicons dashicons-images-alt';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];

// Content
$control                          = [];
$control['label']                 = 'Cards';
$control['name']                  = 'card';
$control['type']                  = 'repeater';
$control['rows_min']              = '1';
$control['rows_max']              = '';
$control['rows_label']            = 'Card {{#}}: {{card-title}}';
$control['rows_add_button_label'] = '+ Add Card';
$control['rows_collapsible']      = 'true';
$control['rows_collapsed']        = 'true';
$control['save_in_meta']          = false;
$control['child_of']              = '';
$control['hide_if_not_selected']  = 'false';

// Make an id.
$control_id = 'cards_card';

// Add the control to the controls
$controls[$control_id] = $control;

// Image
$control                         = [];
$control['label']                = 'Image';
$control['name']                 = 'card-image';
$control['type']                 = 'image';
$control['save_in_meta']         = false;
$control['child_of']             = 'cards_card';
$control['hide_if_not_selected'] = 'true';

// Make an id.
$control_id = 'cards-card-image';

// Add the control to the controls
$controls[$control_id] = $control;

// START SELECT CONTROL
$control = [];
$control['label'] = 'Object fit';
$control['type'] = 'select';
$control['name'] = 'object-fit';
$control['default'] = 'contain';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [
  [
    'label' => 'Contain',
    'value' => 'contain',
  ],
  [
    'label' => 'Cover',
    'value' => 'cover',
  ],
];
$control['choices'] = $choices;
$control_id = 'cards-image-contain';
$controls[$control_id] = $control;

// END SELECT CONTROL

// Title
$control                         = [];
$control['label']                = 'Title';
$control['name']                 = 'card-title';
$control['type']                 = 'text';
$control['save_in_meta']         = false;
$control['child_of']             = 'cards_card';
$control['hide_if_not_selected'] = 'false';
$control['default']              = '';

// Make an id.
$control_id            = 'cards_card_title';
$controls[$control_id] = $control;


// Content
$control = [];
$control['label'] = 'Content';
$control['name'] = 'card-content';
$control['type'] = 'classic_editor';
$control['child_of'] = 'cards_card';

// Make an id.
$control_id = 'cards_card_content';
// Add the control to the controls
$controls[$control_id] = $control;


// Label
$control                         = [];
$control['label']                = 'CTA Label';
$control['name']                 = 'cta-label';
$control['type']                 = 'text';
$control['child_of']             = 'cards_card';


// Make an id.
$control_id = 'cards_card_cta_label';

// Add the control to the controls
$controls[$control_id] = $control;


// URL
$control                 = [];
$control['label']        = 'Url';
$control['name']         = 'cta-url';
$control['type']         = 'url';
$control['save_in_meta'] = false;
$control['child_of']     = 'cards_card';

// Make an id.
$control_id = 'cards_card_url';
// Add the control to the controls
$controls[$control_id] = $control;

// Style
$control                         = [];
$control['type']                 = 'select';
$control['name']                 = 'buttonstyle';
$control['default']              = 'btn-primary';
$control['label']                = 'Button style';
$control['save_in_meta']         = false;
$control['required']             = false;
$control['allow_null']           = 'true';
$control['child_of']     = 'cards_card';
$control['hide_if_not_selected'] = 'false';
$control['placement']            = 'inspector';

$control['choices'] = [
  [
    'label' => 'Primary',
    'value' => 'btn-primary',
  ],
  [
    'label' => 'Secondary',
    'value' => 'btn-secondary',
  ],
  [
    'label' => 'Supporting',
    'value' => 'btn-supporting',
  ],
];

// Make an id.
$control_id            = 'cards_card_buttonstyle';

// Add the control to the controls
$controls[$control_id] = $control;


// Layout
$control = [];
$control['label'] = 'Layout';
$control['name'] = 'layout';
$control['type'] = 'select';
$control['placement'] = 'inspector';
$control['required'] = true;
$control['default'] = '3';
$control['child_of'] = '';

$control['choices'] = [
    [
        'label' => '2 by 2',
        'value' => '2',
    ],
    [
        'label' => '3 by 3',
        'value' => '3',
    ],
    [
        'label' => '4 by 4',
        'value' => '4',
    ],
    [
        'label' => '5 by 5',
        'value' => '5',
    ],
];

// Make an id.
$control_id = 'cards_card_layout';
// Add the control to the controls
$controls[$control_id] = $control;

// START NUMBER CONTROL
$control = [];
$control['label'] = 'Gutter control cards';
$control['type'] = 'number';
$control['name'] = 'gutter';
$control['help'] = 'Value between 0-5';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['min'] = '0';
$control['max'] = '5';
$control['step'] = '1';
$control['default'] = '4';

$control_id = 'cards_card_gutter';
$controls[$control_id] = $control;
// END NUMBER CONTROL

// START NUMBER CONTROL
$control = [];
$control['label'] = 'Image height in px';
$control['type'] = 'number';
$control['name'] = 'height';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['min'] = '';
$control['max'] = '';
$control['step'] = '5';
$control['default'] = '160';

$control_id = 'cards_image_height';
$controls[$control_id] = $control;
// END NUMBER CONTROL

// START SELECT CONTROL
$control = [];
$control['label'] = 'Card background Color';
$control['type'] = 'select';
$control['name'] = 'background-color';
$control['default'] = 'white';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
  [
    'label' => 'Primary',
    'value' => 'primary',
  ],
  [
    'label' => 'Secondary',
    'value' => 'secondary',
  ],
  [
    'label' => 'Supporting',
    'value' => 'supporting',
  ],
  [
    'label' => 'White',
    'value' => 'white',
  ],
  [
    'label' => 'Transparent',
    'value' => 'transparent',
  ],
];

$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SELECT CONTROL

// START SELECT CONTROL
$control = [];
$control['label'] = 'Font Color';
$control['type'] = 'select';
$control['name'] = 'font-color';
$control['default'] = 'primary';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
  [
    'label' => 'Primary',
    'value' => 'primary',
  ],
  [
    'label' => 'Secondary',
    'value' => 'secondary',
  ],
  [
    'label' => 'Black',
    'value' => 'black',
  ],
  [
    'label' => 'White',
    'value' => 'white',
  ],
];

$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START TOGGLE
$control = [];
$control['label']                = 'Border radius';
$control['name']                 = 'border-radius';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = true;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Border Radius';

// Make an id.
$control_id = 'cards-border-radius';

// Add the control to the controls
$controls[$control_id] = $control;

// END SELECT CONTROL

// START TOGGLE
$control = [];
$control['label']                = 'Add line under image?';
$control['name']                 = 'image-line';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Add line';

// Make an id.
$control_id = 'cards_card_line';
// Add the control to the controls
$controls[$control_id] = $control;

// START TEXT ALIGNMENT CONTROL
$control = [];
$control['label'] = 'Text alignment';
$control['type'] = 'select';
$control['name'] = 'textalignment';
$control['default'] = 'text-start';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'text-start'
    ],
    [
        'label' => 'Center',
        'value' => 'text-center'
    ],
    [
        'label' => 'Right',
        'value' => 'text-end'
    ]
];

$control['choices'] = $choices;
// Make an id.
$control_id = 'control-text-alignment';
$controls[$control_id] = $control;
// END TEXT ALIGNMENT CONTROL

// Alignment
$control              = [];
$control['label']     = 'CTA alignment';
$control['name']      = 'cta-alignment';
$control['type']      = 'select';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'justify-content-start';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Left',
        'value' => 'justify-content-start',
    ],
    [
        'label' => 'Center',
        'value' => 'justify-content-center',
    ],
    [
        'label' => 'Right',
        'value' => 'justify-content-end',
    ],
];

// Make an id.
$control_id = 'control-cta-alignment';

// Add the control to the controls
$controls[$control_id] = $control;

include __DIR__ . '/../extraspace.php';
include __DIR__ . '/../extraspace-margin.php';

// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(dirname(__FILE__) . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
