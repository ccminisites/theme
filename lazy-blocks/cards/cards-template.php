<style>
    #cards-{{blockId}} {
    padding-top: {{custompaddingtop}}rem;
    padding-bottom: {{custompaddingbottom}}rem;
    margin-top: {{custommargintop}}rem;
    margin-bottom: {{custommarginbottom}}rem;
    }
    @media (max-width: 767.98px) {
        #cards-{{blockId}} {
        padding-top: 1.5rem;
        padding-bottom: 1.5rem;
    }
    }
    @media (max-width: 575.98px) {
        #cards-{{blockId}} {
        margin-top: 0;
        margin-bottom: 0;
    }
    }
</style>

<div class="container-md">
    <div class="lazyblock-cards background-{{background-color}} font-{{font-color}} {{#if border-radius}}border-radius{{/if}}" id="cards-{{blockId}}">
        <div class="row gy-5 gx-0 g-md-{{gutter}} row-cols-1 row-cols-lg-2 row-cols-xl-{{layout}}">
            {{#each card}}
            <div class="col">
                <div class="card h-100"">
                {{#if cta-url}}
                <a href="{{cta-url}}" class="card-link">
                {{/if}}
                {{#if card-image}}
                <div class="card-img-wrapper small" style="--height: {{height}}px;">
                    <img class="{{ object-fit }}" src="{{card-image.url}}" alt="{{#alternativetag card-image.id}}">
                    {{#if image-line}}
                    <span class="image-line"></span>
                    {{/if}}
                </div>
                {{/if}}
                {{#if cta-url}}
                </a>
                {{/if}}
                <div class="card-content-wrapper h-100 {{textalignment}}">
                    {{#if card-title}}
                    <h4 class="card-title">{{card-title}}</h4>
                    {{/if}}
                    {{#if card-content}}
                    <div class="card-content">
                        {{{card-content}}}
                    </div>
                    {{/if}}
                    {{#if cta-label}}
                    <div class="card-cta {{cta-alignment}}">
                        <a class="btn {{buttonstyle}} " href="{{cta-url}}" target="{{target}}">{{cta-label}}</a>
                    </div>
                    {{/if}}
                </div>
            </div>
        </div>
        {{/each}}
    </div>
</div>
</div>
