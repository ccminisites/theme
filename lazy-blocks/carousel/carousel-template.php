<div class="container-md">
    <div class="row mx-0 lazyblock-carousel justify-content-center carousel-wrapper">
        <div class="col-12 col-lg-10 px-0 carousel-inner-wrapper">
            {{#each carousel}}
            <div class="row">
                {{#if backgroundimage}}
                <div class="full-image col-12 px-0">
                    <img src="{{backgroundimage.url}}" alt="">
                </div>
                {{else}}
                <div class="full-color {{background-color}}"></div>
                {{/if}}
                <div class="col-11 col-lg-5 {{#if content}}blockquote-line{{/if}} carousel-section">
                    <section>
                        {{#if title}}
                        <h5>{{title}}</h5>
                        {{/if}}
                        {{#if content}}
                        <div>{{{content}}}</div>
                        {{/if}}
                    </section>
                </div>
            </div>
            {{/each}}
        </div>
    </div>
</div>
