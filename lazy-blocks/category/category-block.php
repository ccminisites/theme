<?php

$blockslug = 'category';
$blocktitle = 'Category Listing';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-excerpt-view';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';


// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];

// START TEXT CONTROL
$control = [];
$control['label'] = 'Post Type Name';
$control['type'] = 'text';
$control['name'] = 'post-type';
$control['default'] = 'post';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
//$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START TEXT CONTROL
$control = [];
$control['label'] = 'Category Name';
$control['type'] = 'text';
$control['name'] = 'category_name';
$control['default'] = 'general';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// select control show image
$control = [];
$control['label'] = 'Show Post image?';
$control['name'] = 'post_image';
$control['type'] = 'select';
$control['placement'] = 'inspector';
$control['help'] = 'Show post Featured Image above text?';
$control['required'] = false;
$control['default'] = 'no';
$control['child_of'] = '';

$control['choices'] = [
    [
        'label' => 'Yes',
        'value' => 'yes',
    ],
    [
        'label' => 'No',
        'value' => 'no',
    ],
];

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// Layout
$control = [];
$control['label'] = 'Layout';
$control['name'] = 'layout';
$control['type'] = 'select';
$control['placement'] = 'inspector';
$control['required'] = true;
$control['default'] = '3';
$control['child_of'] = '';

$control['choices'] = [
    [
        'label' => '2 by 2',
        'value' => '2',
    ],
    [
        'label' => '3 by 3',
        'value' => '3',
    ],
    [
        'label' => '4 by 4',
        'value' => '4',
    ],
    [
        'label' => '5 by 5',
        'value' => '5',
    ],
];

// Make an id.
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START TOGGLE
$control = [];
$control['label']                = 'Border radius on card?';
$control['name']                 = 'border-radius';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = true;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Border Radius';

// Make an id.
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START SELECT CONTROL
$control = [];
$control['label'] = 'Card Item background Color';
$control['type'] = 'color';
$control['name'] = 'background-color';
$control['default'] = 'white';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';


$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Pagination';
$control['name']     = 'pagination';
$control['type']     = 'number';
$control['default'] = '12';
$control['help'] = '-1 all';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['min'] = '-1';
$control['max'] = '';
$control['step'] = '1';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// START Pagination Style Control
$control = [];
$control['label'] = 'Pagination Style';
$control['type'] = 'select';
$control['name'] = 'paginationstyle';
$control['default'] = 'generic';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Generic',
        'value' => 'generic'
    ],
    [
        'label' => 'Large',
        'value' => 'large'
    ],
    [
        'label' => 'Load More',
        'value' => 'loadmore'
    ],
    [
        'label' => 'None',
        'value' => 'none'
    ]
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Offset';
$control['name']     = 'offset';
$control['type']     = 'number';
$control['default'] = '0';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['min'] = '0';
$control['max'] = '';
$control['step'] = '1';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;



// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
