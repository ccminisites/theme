<?php
/** @var array $attributes */
$containerClass = $attributes['className'] ? $attributes['className'] . '-container' : '';
$blockID = $attributes['blockId'];

// Setup
$queryvar = $attributes['blockId'] . '_page'; // work on a custom query url param?
$orderqueryvar = $attributes['blockId'] . '_order'; // work on a custom query url param?
$cutoffqueryvar = $attributes['blockId'] . '_cutoff'; // work on a custom query url param?
$page = (int)($_GET[$queryvar] ?? 0); // what page are we on? 0 - X
$pagination = (int)($attributes['pagination'] ?: 12); // What is our pagination?
$offset = ((int)$attributes['offset']) + $page * $pagination; // Offset from the block plus the offset of the page X pagination
$order = $_GET[$orderqueryvar] ?? 'DESC';
$cutoff = $_GET[$cutoffqueryvar] ?? 'all';


// The query params with out our param in. This way we don't interfer with other stuff.
$get = $_GET;
unset($get[$queryvar]);


$category_name = $attributes['category_name'];

$args = [
        'post_type' => $attributes['post-type'],
        'category_name' => $category_name,
        'post_status' => 'publish',
        'posts_per_page' => $pagination,
        'order' => $order,
        'orderby' => 'date',
        'offset' => $offset,
];


$category_query = new WP_Query($args);

?>

<style>
    #category-items-<?= $attributes['blockId']?> .row .col .card {
        background-color: <?= $attributes['background-color'] ?>;
    }
</style>


<div class="container-md lazyblock-section lazyblock-category" id="section-category-posts">
    <div class="row ">
        <div class="col-lg-12">
            <div class="container-md">
                <div class="lazyblock-cards <?= $attributes['border-radius'] ? 'border-radius' : '' ?>"
                     id="category-items-<?= $attributes['blockId'] ?>">
                    <div class="row gy-5 gx-0 g-md-4 row-cols-1 row-cols-lg-2 row-cols-xl-<?= $attributes['layout']; ?>" id="content_<?= $attributes['blockId'] ?>">
                        <?php if ($category_query->have_posts()) { ?>
                            <?php while ($category_query->have_posts()) : $category_query->the_post(); ?>
                                <div class="col">
                                    <div class="card h-100">
                                        <div class="card-content-wrapper h-100 text-start">
                                            <div class="post-category-img ">
                                                <?= $attributes['post_image'] === 'yes' ? get_the_post_thumbnail() : '' ?>
                                            </div>
                                            <h4 class="card-title my-3"><?= the_title(); ?></h4>
                                            <div class="card-content">
                                                <p><?= project_truncate(get_the_excerpt(), '30'); ?></p>
                                            </div>
                                            <div class="read-more d-flex justify-content-end">
                                                <a href="<?= the_permalink(); ?>">
                                                    <?= pll_e('read-more'); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>
                        <?php } else { ?>
                            <p><?php _e('Sorry, no posts exists for this category'); ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php if ($attributes['paginationstyle'] !== 'none') { ?>
        <?php
        $max = $category_query->max_num_pages - (floor($attributes['offset'] / $pagination));
        // Only show pager if there is more than one page.
    if ($max > 1) {
        ?>
        <div
                class="row pagination <?= $attributes['paginationstyle'] === 'generic' ? ' pagination_generic' : '' ?><?= $attributes['paginationstyle'] === 'large' ? ' pagination_large' : '' ?><?= $attributes['paginationstyle'] === 'loadmore' ? ' d-none' : '' ?> mt-5"
                id="pagination_<?= $attributes['blockId'] ?>">
            <a class="pagination_link pagination_link_first"
               href="<?= $get ? '?' . http_build_query($get) : './' // query url to go to the first page with disturbing others.      ?>"><?php pll_e('First') ?></a>
            <?php
            $i = 0;
            $query = $get; // url query without our queryvar in.
            while ($i++ < $max) {
                $query[$queryvar] = $i - 1;
                $queryparams = http_build_query($query); // query url params with our page.
                ?>
                <a
                        class="pagination_link<?= $page === $i - 1 ? ' active' : '' ?> pagination_link_pagediff-<?= $page - ($i - 1) ?>"
                        href="?<?= $queryparams ?>"><?= $i ?></a>
            <?php } ?>
            <a class="pagination_link pagination_link_last"
               href="?<?= http_build_query($get + [$queryvar => ($max - 1)]) // query url with the last page      ?>"><?php pll_e('Last') ?></a>
        </div>


    <?php if ($attributes['paginationstyle'] === 'loadmore' && ($max - 1)) { ?>

        <div class="row pagination pagination_loadmore mt-5">
            <a href="#" data-page="0" data-max="<?= $max - 1 ?>" data-target="content_<?= $attributes['blockId'] ?>"
               data-base="<?= http_build_query($get) ?>" data-queryvar="<?= $queryvar ?>"
               class="pagination_loadmore btn btn-primary rounded-0"
               id="pagination_loadmore_<?= $attributes['blockId'] ?>"
            ><?php pll_e('load_more'); ?></a>
            <span class="d-none" id="hidden_content_<?= $attributes['blockId'] ?>"></span>
        </div>
        <script>
            jQuery(document).ready(function () {
                jQuery('#pagination_loadmore_<?= $attributes['blockId'] ?>').on('click', function (e) {
                    e.preventDefault();
                    let dis = jQuery(this); // save this for later
                    let page = jQuery(this).data('page'); // get the page we are on

                    if (typeof page !== "undefined" && !dis.hasClass('processing')) { // check the page and make sure we are not already processing.
                        let base = jQuery(this).data('base'); // base url
                        let max = jQuery(this).data('max'); // the max page we can go to
                        let queryvar = jQuery(this).data('queryvar'); // the url param we are working on
                        let target = jQuery(this).data('target'); // the target div
                        dis.addClass('processing'); // turn on the processing
                        let url = '?' + base + '&' + queryvar + '=' + (page + 1); // make the url we need to load up.
                        jQuery('#hidden_' + target).load(url + ' #' + target, function () { // load the div in to the hidden
                            jQuery('#' + target).append(jQuery('#hidden_' + target + ' > div').html()); // append the html of the div within the hidden
                            jQuery('#hidden_' + target).html(''); // blank out the hidden
                            page++; // increase the page
                            dis.data('page', page); // change the data on the link
                            dis.removeClass('processing'); // remove processing
                            if (page === max) {
                                dis.addClass('d-none'); // hide the link if we are at the end
                            }
                        });
                    }
                });
            });
        </script>
    <?php } ?>
    <?php
    }
    ?>
    <?php } else { ?>
        <div class="row nothingtoshow">
            <div class="col-12">
                <p><?php pll_e('nothing_to_show'); ?></p>
            </div>
        </div>
    <?php } ?>
    <?php wp_reset_query(); ?>


</div>