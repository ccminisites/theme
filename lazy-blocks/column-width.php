<?php

// Bootstrap class true and false
$control = [];
$control['label']                = 'Column Grid';
$control['name']                 = 'columngrid';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = false;
$control['child_of']             = '';
$control['alongside_text']       = 'Use custom width';

// Make an id.
$control_id = 'columngrid';

// Add the control to the controls
$controls[$control_id] = $control;

// columns-class
$control                         = [];
$control['label']                = 'Column Width Number';
$control['name']                 = 'columnwidth';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '1';
$control['max']                  = '12';
$control['step']                 = '1';
$control['default']              = '8';
$control['placement']            = 'inspector';
$control['help']                 = 'Value between 1-12';

// Make an id.
$control_id = 'columnwidth';

// Add the control to the controls
$controls[$control_id] = $control;
