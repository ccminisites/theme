<?php

$blockslug = 'content-content';

// Start the block
$block             = [];
$block['title']    = 'Content Content';
$block['icon']     = 'dashicons dashicons-buddicons-friends';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];

// Content 1
$control                         = [];
$control['label']                = 'Content One';
$control['name']                 = 'contentone';
$control['type']                 = 'classic_editor';
$control['child_of']             = '';
$control['width']                = '50';

// Make an id.
$control_id = 'control-two-content-content-one';

// Add the control to the controls
$controls[$control_id] = $control;

// Content 2
$control                         = [];
$control['label']                = 'Content Two';
$control['name']                 = 'contenttwo';
$control['type']                 = 'classic_editor';
$control['child_of']             = '';
$control['width']                = '50';

// Make an id.
$control_id = 'control-two-content-content-two';

// Add the control to the controls
$controls[$control_id] = $control;

// Radio
$control              = [];
$control['label']     = 'Division';
$control['name']      = 'division';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '6';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => '5-5',
        'value' => '5',
    ],
    [
        'label' => '6-6',
        'value' => '6',
    ],
    [
        'label' => '7-5',
        'value' => '7',
    ],
    [
        'label' => '8-4',
        'value' => '8',
    ],
    [
        'label' => '9-3',
        'value' => '9',
    ],
];

// Make an id.
$control_id = 'control-content-content-division';

// Add the control to the controls
$controls[$control_id] = $control;

// START TOGGLE
$control = [];
$control['label']                = 'Inverse content';
$control['name']                 = 'inverse';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Inverse';

// Make an id.
$control_id = 'control-content-content-inverse';

// Add the control to the controls
$controls[$control_id] = $control;

// Alignment
$control              = [];
$control['label']     = 'Alignment';
$control['name']      = 'alignment';
$control['type']      = 'select';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'align-items-center';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Top',
        'value' => 'align-items-start',
    ],
    [
        'label' => 'Center',
        'value' => 'align-items-center',
    ],
    [
        'label' => 'Bottom',
        'value' => 'align-items-end',
    ],
];

// Make an id.
$control_id = 'control-content-content-alignment';

// Add the control to the controls
$controls[$control_id] = $control;

// Toggle
$control                         = [];
$control['label']                = 'Content One blockquote line?';
$control['name']                 = 'contentoneblockquote';
$control['type']                 = 'toggle';
$control['default']              = false;
$control['checked']              = false;
$control['child_of']             = '';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-two-content-content-one-blue';

// Add the control to the controls
$controls[$control_id] = $control;

// Toggle
$control                         = [];
$control['label']                = 'Content Two blockquote line?';
$control['name']                 = 'contenttwoblockquote';
$control['type']                 = 'toggle';
$control['default']              = false;
$control['checked']              = false;
$control['child_of']             = '';
$control['placement']            = 'inspector';


// Make an id.
$control_id = 'control-two-content-content-two-blue';

// Add the control to the controls
$controls[$control_id] = $control;


// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
