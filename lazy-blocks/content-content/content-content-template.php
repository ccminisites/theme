<div class="container-md">
    <div class="row lazyblock-content-content {{alignment}}">
        {{#ifEquals division '5'}}
        {{#if contentoneblockquote}}
        <div class="{{#if inverse}}col-12 col-lg-5 blockquote-line order-2{{else}}col-12 col-lg-5 blockquote-line order-1{{/if}}">
            {{else}}
        <div class="{{#if inverse}}col-12 col-lg-5 order-2{{else}}col-12 col-lg-5 order-1{{/if}}">
            {{/if}}
            {{{contentone}}}
        </div>
        {{#if contenttwoblockquote}}
        <div class="{{#if inverse}}col-12 col-lg-5 blockquote-line order-1{{else}}col-12 col-lg-5 blockquote-line order-2{{/if}}">
            {{else}}
        <div class="{{#if inverse}}col-12 col-lg-5 order-1{{else}}col-12 col-lg-5 order-2{{/if}}">
            {{/if}}
            {{{contenttwo}}}
        </div>
        {{/ifEquals}}
        {{#ifNotEquals division '5'}}
        {{#if contentoneblockquote}}
        <div class="{{#if inverse}}col-12 col-lg-{{#leftover division}} blockquote-line order-2{{else}}col-12 col-lg-{{division}} blockquote-line order-1{{/if}}">
            {{else}}
        <div class="{{#if inverse}}col-12 col-lg-{{#leftover division}} order-2{{else}}col-12 col-lg-{{division}} order-1{{/if}}">
        {{/if}}
            {{{contentone}}}
        </div>
        {{#if contenttwoblockquote}}
        <div class="{{#if inverse}}col-12 col-lg-{{division}} blockquote-line order-1{{else}}col-12 col-lg-{{#leftover division}} blockquote-line order-2{{/if}}">
            {{else}}
        <div class="{{#if inverse}}col-12 col-lg-{{division}} order-1{{else}}col-12 col-lg-{{#leftover division}} order-2{{/if}}">
        {{/if}}
            {{{contenttwo}}}
        </div>
            {{/ifNotEquals}}
    </div>
</div>
