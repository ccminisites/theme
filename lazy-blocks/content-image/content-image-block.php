<?php

$blockslug = 'content-image';

// Start the block
$block             = [];
$block['title']    = 'Content Image';
$block['icon']     = 'dashicons dashicons-media-document';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];
///////////// START CONTROLS /////////////


// Content
$control                         = [];
$control['label']                = 'Content';
$control['name']                 = 'content';
$control['type']                 = 'inner_blocks';
$control['width']                = '50';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-image-content-content';

// Add the control to the controls
$controls[$control_id] = $control;


// Image
$control                         = [];
$control['label']                = 'Image';
$control['name']                 = 'image';
$control['type']                 = 'image';
$control['width']                = '50';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-image-content-image';

// Add the control to the controls
$controls[$control_id] = $control;

// START SQUISH CONTROL
$control              = [];
$control['label']     = 'Container';
$control['type']      = 'radio';
$control['name']      = 'container';
$control['default']   = 'container-md';
$control['checked'] = true;
$control['help'] = '';
$control['child_of']  = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$choices = [];
$choices = [
  [
    'label' => 'MD',
    'value' => 'container-md',
  ],
  [
    'label' => 'Fluid',
    'value' => 'container-fluid',
  ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SQUISH CONTROL

// START IMAGE CONTROL
$control = [];
$control['label'] = 'Content Background Image';
$control['type'] = 'image';
$control['name'] = 'content-backgroundimage';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END IMAGE CONTROL


// START SELECT CONTROL
$control = [];
$control['label'] = 'Content Background Color';
$control['type'] = 'select';
$control['name'] = 'content-background-color';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
  [
    'label' => 'Primary',
    'value' => 'primary',
  ],
  [
    'label' => 'Secondary',
    'value' => 'secondary',
  ],
  [
    'label' => 'Supporting',
    'value' => 'supporting',
  ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SELECT CONTROL

// START IMAGE CONTROL
$control = [];
$control['label'] = 'Image Background Image';
$control['type'] = 'image';
$control['name'] = 'image-backgroundimage';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END IMAGE CONTROL


// START SELECT CONTROL
$control = [];
$control['label'] = 'Image Background Color';
$control['type'] = 'select';
$control['name'] = 'image-background-color';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
  [
    'label' => 'Primary',
    'value' => 'primary',
  ],
  [
    'label' => 'Secondary',
    'value' => 'secondary',
  ],
  [
    'label' => 'Supporting',
    'value' => 'supporting',
  ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SELECT CONTROL


// START TOGGLE
$control = [];
$control['label']                = 'Inverse content-image to image-content?';
$control['name']                 = 'inverse';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Inverse';

// Make an id.
$control_id = 'control-image-content-inverse';

// Add the control to the controls
$controls[$control_id] = $control;
// END TOGGLE

// START TOGGLE
$control = [];
$control['label']                = 'Border radius on image?';
$control['name']                 = 'border-radius';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Border Radius';

// Make an id.
$control_id = 'control-image-content-border-radius';

// Add the control to the controls
$controls[$control_id] = $control;
// END TOGGLE


// Radio
$control              = [];
$control['label']     = 'Division';
$control['name']      = 'division';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '6';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => '6-6',
        'value' => '6',
    ],
    [
        'label' => '7-5',
        'value' => '7',
    ],
    [
        'label' => '8-4',
        'value' => '8',
    ],
    [
        'label' => '9-3',
        'value' => '9',
    ],
];

// Make an id.
$control_id = 'control-content-image-division';

// Add the control to the controls
$controls[$control_id] = $control;

// Radio
$control              = [];
$control['label']     = 'Content Centered?';
$control['name']      = 'flex-centered';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'flex-centered';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'No',
        'value' => '',
    ],
    [
        'label' => 'Yes',
        'value' => 'flex-centered',
    ],
];


// Make an id.
$control_id = 'control-content-flex-centered';

// Add the control to the controls
$controls[$control_id] = $control;

include __DIR__ . '/../extraspace-zero-default.php';

$control                         = [];
$control['label']                = 'Use custom Image size ?';
$control['name']                 = 'imagesize-toggle';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Yes';
$control['save_in_meta']          = false;

// Make an id.
$control_id = 'imagesize-toggle';

// Add the control to the controls
$controls[$control_id] = $control;

$control                         = [];
$control['label']                = 'Image size (percentage)';
$control['name']                 = 'imagesize';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '0';
$control['max']                  = '100';
$control['step']                 = '1';
$control['default']              = '100';
$control['placement']            = 'inspector';

// Make an id.
$control_id = 'imagesize';

// Add the control to the controls
$controls[$control_id] = $control;

$control                         = [];
$control['label']                = 'Change image order on mobile (when needed)';
$control['name']                 = 'imageorder';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = false;
$control['child_of']             = '';
$control['alongside_text']       = 'Change order';
$control['save_in_meta']          = false;

// Make an id.
$control_id = 'imageorder';

// Add the control to the controls
$controls[$control_id] = $control;

///////////// END CONTROLS /////////////
// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
