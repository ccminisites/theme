<style>
    #content-image-{{blockId}} {
    padding-top: {{custompaddingtop}}rem;
    padding-bottom: {{custompaddingbottom}}rem;
    }
    {{#if imagesize-toggle}}
    #content-image-{{blockId}} .on-the-fly-{{blockId}} {
    width: {{imagesize}}%;
    }
    {{/if}}
    {{#if imageorder}}
    @media (max-width: 991.98px) {
        #content-image-{{blockId}} > .image-wrapper.order-2 {
                                       order: 0!important;
                                   }
    #content-image-{{blockId}} > .image-wrapper.order-1 {
                                   order: 3 !important;
                               }
    }
    {{/if}}
</style>

<div class="{{container}}">
    <div class="row lazyblock-content-image {{#if border-radius}}border-radius{{/if}}" id="content-image-{{blockId}}">
        <div class="content-wrapper col-lg-{{division}} col-12 px-lg-5 mb-lg-0 mb-4 {{#if inverse}} order-2 {{else}} order-1 {{/if}}">
            <div class="d-flex {{flex-centered}} h-100">
                {{#if content-backgroundimage}}
                <div class="fluid-image" style="background: center / cover no-repeat url('{{content-backgroundimage.url}}'); opacity: {{opacity}}%;"></div>
                {{/if}}
                {{#if content-background-color}}
                <div class="fluid-color {{content-background-color}}"></div>
                {{/if}}
                <div class="d-flex flex-wrap">
                    <InnerBlocks />
                </div>
            </div>
        </div>
        <div class="image-wrapper col-lg-{{#leftover division}} {{#if inverse}} order-1 {{else}} order-2 {{/if}}">
            <div class="d-flex {{flex-centered}} h-100">
                {{#if image-backgroundimage}}
                <div class="fluid-image" style="background: center / cover no-repeat url('{{image-backgroundimage.url}}'); opacity: {{opacity}}%;"></div>
                {{/if}}
                {{#if image-background-color}}
                <div class="fluid-color {{image-background-color}}"></div>
                {{/if}}
                {{#if image}}
                <img class="on-the-fly-{{blockId}}" src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}"/>
                {{/if}}
            </div>
        </div>
    </div>
</div>
