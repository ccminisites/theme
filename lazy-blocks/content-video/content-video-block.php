<?php

$blockslug = 'content-video';
$blocktitle = 'Content Video';

// Start the block
$block             = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon']     = 'dashicons dashicons-media-document';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];


// START CONTENT
$control             = [];
$control['label']    = 'Content';
$control['name']     = 'content';
$control['type']     = 'inner_blocks';
$control['width']    = '50';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END CONTENT CONTROL

// START TEXTAREA CONTROL
$control = [];
$control['label'] = 'Code';
$control['type'] = 'textarea';
$control['name'] = 'code';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '50';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END TEXTAREA CONTROL

// START TOGGLE
$control = [];
$control['label']                = 'Inverse content-video to video-content?';
$control['name']                 = 'inverse';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Inverse';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END TOGGLE


// END THE CONTROLS
$block['controls'] = $controls;

// START CODE
$code = [];
$code['output_method'] = 'html';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
