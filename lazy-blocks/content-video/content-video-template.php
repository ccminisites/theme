<div class="container-md">
    <div class="row mx-0 lazyblock-content-video">
        <div class="col-12 col-lg-5 flex-centered content-wrapper {{#if inverse}} order-2 pe-lg-0 {{else}}pb-4 pb-lg-0 order-1 ps-lg-0 {{/if}}">
                <div class="wrap">
                    <InnerBlocks />
                </div>
        </div>
        <div class="col-12 col-lg-7 embed-wrapper {{#if inverse}}pb-4 pb-lg-0 order-1 ps-lg-0 {{else}} order-2 pe-lg-0 {{/if}}">
            <div class="ratio ratio-16x9">
                {{{code}}}
            </div>
        </div>
    </div>
</div>
