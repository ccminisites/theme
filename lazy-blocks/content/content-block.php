<?php

$blockslug = 'content';

// Start the block
$block             = [];
$block['title']    = 'Content';
$block['icon']     = 'dashicons dashicons-media-document';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];

// Text
$control                         = [];
$control['label']                = '';
$control['name']                 = 'content';
$control['type']                 = 'classic_editor';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-content-content';

// Add the control to the controls
$controls[$control_id] = $control;

// Radio
$control              = [];
$control['label']     = 'Division';
$control['name']      = 'division';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '12';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Default',
        'value' => '8',
    ],
    [
        'label' => 'Full width',
        'value' => '12',
    ],
];


// Make an id.
$control_id = 'control-content-image-division';

// Add the control to the controls
$controls[$control_id] = $control;


// Select
$control              = [];
$control['label']     = 'Content Alignment';
$control['name']      = 'alignment';
$control['type']      = 'select';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'justify-content-start';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Left',
        'value' => 'justify-content-start',
    ],
    [
        'label' => 'Center',
        'value' => 'justify-content-center',
    ],
    [
        'label' => 'Right',
        'value' => 'justify-content-end',
    ]
];

// Make an id.
$control_id = 'control-content-alignment';

// Add the control to the controls
$controls[$control_id] = $control;


// Select
$control              = [];
$control['label']     = 'Content text Alignment';
$control['name']      = 'textalignment';
$control['type']      = 'select';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'start';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Left',
        'value' => 'start',
    ],
    [
        'label' => 'Center',
        'value' => 'center',
    ],
    [
        'label' => 'Right',
        'value' => 'end',
    ]
];

// Make an id.
$control_id = 'control-content-text-alignment';

// Add the control to the controls
$controls[$control_id] = $control;


// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
