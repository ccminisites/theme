<?php

$blockslug = 'countdown';
$blocktitle = 'Countdown';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-hourglass';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = true;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [
    'spacings' => false,
    'display' => false,
    'scrollReveal' => false,
    'frame' => false,
    'customCSS' => false,
];

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];


// Holder for the controls
$controls = [];


// Date End text
$control = [];
$control['label'] = 'Date End';
$control['name'] = 'date-end';
$control['type'] = 'text';
$control['help'] = 'put the date end like this: Sep 28, 2023 16:20:20';
$control['save_in_meta'] = false;
$control['child_of'] = '';
$control['hide_if_not_selected'] = 'false';
$control['default'] = '';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';


// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END of date end

// font size
$control = [];
$control['label'] = 'Font size';
$control['name'] = 'font-size';
$control['type'] = 'number';
$control['help'] = 'font size using REM';
$control['save_in_meta'] = false;
$control['child_of'] = '';
$control['hide_if_not_selected'] = 'false';
$control['default'] = '';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['placement'] = 'inspector';


// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END of font size

// line-height
$control = [];
$control['label'] = 'Line height between number and label';
$control['name'] = 'line-height';
$control['type'] = 'number';
$control['help'] = 'font size using PX';
$control['save_in_meta'] = false;
$control['child_of'] = '';
$control['hide_if_not_selected'] = 'false';
$control['default'] = '';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['placement'] = 'inspector';


// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END of font size


// START TEXT ALIGNMENT CONTROL
$control = [];
$control['label'] = 'Text alignment';
$control['type'] = 'select';
$control['name'] = 'textalignment';
$control['default'] = 'start';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'start'
    ],
    [
        'label' => 'Center',
        'value' => 'center'
    ],
    [
        'label' => 'Right',
        'value' => 'end'
    ]
];

$control['choices'] = $choices;

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT ALIGNMENT CONTROL


include __DIR__ . '/../extraspace.php';
include __DIR__ . '/../extraspace-margin.php';

// Add the controls to the block
$block['controls'] = $controls;

// START CODE
$code = [];
$code['show_preview'] = 'never';
$code['single_output'] = false;
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['editor_css'] = '';
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
