<?php
/** @var array $attributes */
$containerClass = $attributes['className'] ? $attributes['className'] . '-container' : '';
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        function count_down() {

            let target_date = new Date("<?= $attributes['date-end'] ?>").getTime();

            const now = new Date().getTime();
            const time_remaining = target_date - now;

            // Calculate days, hours, minutes, and seconds
            // return the real integer for day, hours, minutes and seconds
            const days = Math.floor(time_remaining / (1000 * 60 * 60 * 24));
            const hours = Math.floor((time_remaining % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            const minutes = Math.floor((time_remaining % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((time_remaining % (1000 * 60)) / 1000);

            // display the countdown in HTML
            document.getElementById('days').textContent = days;
            document.getElementById('hours').textContent = hours;
            document.getElementById('minutes').textContent = minutes;
            document.getElementById('seconds').textContent = seconds;

            if (time_remaining <= 0) {
                document.getElementById('lazyblock-countdown').innerHTML = "Countdown expired!";
            }
        }

        // Update every second
        setInterval(() => {
            count_down()
        }, 1000)
    });
</script>

<style>
    .number, .label {
        font-size: <?= $attributes['font-size'] ?>rem !important;
        line-height: <?= $attributes['line-height'] ?>px !important;
    }
</style>

<div class="container-md lazyblock-countdown-container <?= $attributes['blockUniqueClass'] ?>-container <?= $containerClass ?>">
    <div class="row lazyblock-countdown-content <?= $attributes['className'] ?> <?= $attributes['blockUniqueClass'] ?>">
        <div class="col-12 d-flex justify-content-<?= $attributes['textalignment'] ?>">
            <div id="lazyblock-countdown">
                <div class="unit">
                    <span class="number" id="days"></span>
                    <span class="label">Days</span>
                </div>
                <div class="unit">
                    <span class="number" id="hours"></span>
                    <span class="label">Hours</span>
                </div>
                <div class="unit">
                    <span class="number" id="minutes"></span>
                    <span class="label">Minutes</span>
                </div>
                <div class="unit">
                    <span class="number" id="seconds"></span>
                    <span class="label">Seconds</span>
                </div>
            </div>
        </div>
    </div>
</div>

