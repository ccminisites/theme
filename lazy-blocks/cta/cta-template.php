<div class="row lazyblock-cta w-100">
    <div class="col-lg-12 text-{{alignment}}">
        <a class="btn {{buttonstyle}} {{#if extra-buttonstyle}}{{extra-buttonstyle}}{{/if}} {{#if large}}large{{/if}}" href="{{url}}" target="{{target}}">{{{label}}}</a>
    </div>
</div>
