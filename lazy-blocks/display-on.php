<?php

// START SELECT CONTROL
$control = [];
$control['label'] = 'Only show on';
$control['type'] = 'select';
$control['name'] = 'displayOn';
$control['default'] = '';
$control['checked'] = true;
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = '';
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$control['translate'] = 'false';
$control['choices'] = [
    [
        'label' => 'Large screens',
        'value' => 'd-none d-xl-block',
    ],
    [
        'label' => 'Tablet screens',
        'value' => 'd-none d-lg-block d-xl-none',
    ],
    [
        'label' => 'Mobile screens',
        'value' => 'd-lg-none',
    ],
    [
        'label' => 'Large & Tablet screens',
        'value' => 'd-none d-lg-block',
    ],
    [
        'label' => 'Mobile & Tablet screens',
        'value' => 'd-xl-none',
    ],
    [
        'label' => 'Hide',
        'value' => 'd-none',
    ],
];
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SELECT CONTROL
