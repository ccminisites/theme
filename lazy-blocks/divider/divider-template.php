<style>
    #divider-{{blockId}} {
    padding-top: {{custompaddingtop}}rem;
    padding-bottom: {{custompaddingbottom}}rem;
    }
</style>

<div class="{{container}}">
  <div class="row lazyblock-divider" id="divider-{{blockId}}">
    <div class="col-12">
      <hr />
    </div>
  </div>
</div>
