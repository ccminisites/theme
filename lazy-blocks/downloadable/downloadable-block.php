<?php

$blockslug = 'downloadable';

// Start the block
$block             = [];
$block['title']    = 'Downloadable';
$block['icon']     = 'dashicons dashicons-download';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];

// Label
$control             = [];
$control['label']    = 'Label';
$control['name']     = 'label';
$control['type']     = 'text';
$control['child_of'] = '';


// Make an id.
$control_id = 'control-'.$blockslug.'-'.$control['name'];

// Add the control to the controls
$controls[$control_id] = $control;

// START FILE CONTROL
$control = [];
$control['label'] = 'File';
$control['type'] = 'file';
$control['name'] = 'file';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['preview_size'] = 'thumbnail';
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allowed_mime_types'] = ['pdf','zip'];

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END FILE CONTROL

// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = true;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
