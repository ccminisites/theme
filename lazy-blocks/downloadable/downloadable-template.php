<?php if ($attributes['file']) { ?>
<?php

/** @var array $attributes */
    
    
  $wpFile = get_attached_file($attributes['file']['id']);
  $wpFileSize = size_format(filesize($wpFile), 1);
  $wpBase = basename($wpFile);

?>
<div class="row lazyblock-downloadable">
    <div class="col-lg-12">
      <a class="btn btn-primary" href="<?= $attributes['file']['url'] ?>" target="_blank" download><?= $attributes['label'] ?></a>
      <br />
      <span style="font-size: xx-small;"><em><?= $wpBase ?> - <?= $wpFileSize ?></em></span>
    </div>
</div>
<?php } ?>
