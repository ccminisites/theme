<?php

$control                         = [];
$control['label']                = 'Margin Top (rem)';
$control['name']                 = 'custommargintop';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-20';
$control['max']                  = '20';
$control['step']                 = '1';
$control['default']              = '0';
$control['placement']            = 'inspector';

// Make an id.
$control_id = 'custommargintop';

// Add the control to the controls
$controls[$control_id] = $control;

$control                         = [];
$control['label']                = 'Margin Bottom (rem)';
$control['name']                 = 'custommarginbottom';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '-20';
$control['max']                  = '20';
$control['step']                 = '1';
$control['default']              = '0';
$control['placement']            = 'inspector';

// Make an id.
$control_id = 'custommarginbottom';

// Add the control to the controls
$controls[$control_id] = $control;
