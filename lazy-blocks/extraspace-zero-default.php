<?php

$control                         = [];
$control['label']                = 'Padding Top (rem)';
$control['name']                 = 'custompaddingtop';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '0';
$control['max']                  = '20';
$control['step']                 = '1';
$control['default']              = '0';
$control['placement']            = 'inspector';

// Make an id.
$control_id = 'custompaddingtop';

// Add the control to the controls
$controls[$control_id] = $control;

$control                         = [];
$control['label']                = 'Padding Bottom (rem)';
$control['name']                 = 'custompaddingbottom';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '0';
$control['max']                  = '20';
$control['step']                 = '1';
$control['default']              = '0';
$control['placement']            = 'inspector';

// Make an id.
$control_id = 'custompaddingbottom';

// Add the control to the controls
$controls[$control_id] = $control;
