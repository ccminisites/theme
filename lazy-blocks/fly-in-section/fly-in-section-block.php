<?php

$blockslug = 'fly-in-section';
$blocktitle = 'Fly in Section';

// Start the block
$block             = [];
$block['slug']     = 'lazyblock/' . $blockslug;
$block['title']    = $blocktitle;
$block['icon']     = 'dashicons dashicons-exit';
$block['category'] = 'common';

$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];


// START Identifier CONTROL
$control = [];
$control['label'] = 'Unique Identifier';
$control['type'] = 'text';
$control['name'] = 'identifier';
$control['default'] = 'someRandomWord';
$control['help'] = 'This results in the ID #flyin-someRandomWord';
$control['child_of'] = '';
$control['placement'] = 'inspector';
//$control['placement'] = 'inspector';
$control['width'] = '75';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// START IMAGE CONTROL
$control = [];
$control['label'] = 'Background Image';
$control['type'] = 'image';
$control['name'] = 'backgroundimage';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END IMAGE CONTROL


// START SELECT CONTROL
$control = [];
$control['label'] = 'Background Color';
$control['type'] = 'select';
$control['name'] = 'background-color';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Primary',
        'value' => 'primary',
    ],
    [
        'label' => 'Secondary',
        'value' => 'secondary',
    ],
    [
        'label' => 'Supporting',
        'value' => 'supporting',
    ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SELECT CONTROL


// START SQUISH CONTROL
$control              = [];
$control['label']     = 'Fly in direction';
$control['type']      = 'radio';
$control['name']      = 'direction';
$control['default']   = 'left';
$control['checked'] = true;
$control['help'] = '';
$control['child_of']  = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'left',
    ],
    [
        'label' => 'Right',
        'value' => 'right',
    ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SQUISH CONTROL

// START Width CONTROL
$control = [];
$control['label'] = 'Width (vw)';
$control['type'] = 'text';
$control['name'] = 'width';
$control['default'] = '25';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
//$control['placement'] = 'inspector';
$control['width'] = '40';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// START Height CONTROL
$control = [];
$control['label'] = 'Height (vh)';
$control['type'] = 'text';
$control['name'] = 'height';
$control['default'] = '100';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
//$control['placement'] = 'inspector';
$control['width'] = '40';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START Center TOGGLE
$control                         = [];
$control['type']                 = 'toggle';
$control['name']                 = 'center';
$control['default'] = false;
$control['checked'] = false;
$control['label']                = 'Vertically centered';
$control['save_in_meta']         = false;
$control['required']             = false;
$control['child_of']             = '';
$control['hide_if_not_selected'] = false;
//$control['placement'] = 'content';
$control['placement']            = 'inspector';
$control['help'] = '';
$control['alongside_text'] = 'Vertically centered';

// Make an id.
$control_id            = 'control-cta-outline';

// Add the control to the controls
$controls[$control_id] = $control;

// START Opacity CONTROL
$control = [];
$control['label'] = 'Opacity (%)';
$control['type'] = 'text';
$control['name'] = 'opacity';
$control['default'] = '100';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
//$control['placement'] = 'inspector';
$control['width'] = '40';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// Content
$control             = [];
$control['label']    = '';
$control['name']     = 'content';
$control['type']     = 'inner_blocks';
$control['child_of'] = '';

$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'html';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];


if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
