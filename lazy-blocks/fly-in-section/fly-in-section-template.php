<div id="{{identifier}}" class="container-fluid lazyblock-fly-in-section direction-{{direction}} {{#if center}}center{{/if}} {{#if background-color}}fluid-color {{background-color}}{{/if}}{{#if backgroundimage}}fluid-image{{/if}}" style="height:{{height}}vh; width: {{width}}vw; opacity: {{opacity}}%;{{#if backgroundimage}}background: center / cover no-repeat url('{{backgroundimage.url}}');{{/if}}">
    <div class="flyin-section-close">
        <a href="#flyin-{{identifier}}">
            <i class="fas fa-times"></i>
        </a>
    </div>
<div class="lazyblock-fly-in-section-inner">

    <InnerBlocks />

</div>
</div>
