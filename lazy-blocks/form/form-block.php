<?php

$blockslug = 'form';
$blocktitle = 'Form';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-feedback';
$block['description'] = "Display and handle a form";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];

// Form selections
$control                         = [];
$control['label']                = 'Which Form to Show?';
$control['type']                 = 'select';
$control['name']                 = 'theform';
$control['default']              = '';
$control['help']                 = '';
$control['save_in_meta']         = false;
$control['required']             = true;
$control['child_of']             = '';
$control['hide_if_not_selected'] = 'false';
$control['placement']            = 'content';
//$control['placement']          = 'inspector';
$control['width']                = '100';
$control['allow_null']           = 'false';
$control['multiple']             = 'false';

$forms = get_posts([
  'post_type' => 'form',
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'order'    => 'ASC',
  'orderby' => 'title',
]);

$choices = [];
$choices[] = [
  'label' => '-- Choose a form to display --',
  'value' => ''
];

foreach ($forms as $form) {
    $choices[] = [
      'label' => $form->post_title,
      'value' => $form->ID
    ];
}


$control['choices'] = $choices;
$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;

// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(dirname(__FILE__) . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
