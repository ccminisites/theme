<?php

/** @var array $attributes */

$formdata = get_field('the_form', $attributes['theform']);
$formthankyou = get_field('the_thankyou', $attributes['theform']);
if ($formdata) {
?>
<div class="container-md">
  <div class="row mx-0 lazyblock-form px-0 px-lg-3">
    <div class="col-12" id="fb-info">
        <?php
        $formFields = json_decode(preg_replace('/"$/', '', preg_replace('/^\"/', '', stripslashes($formdata))), true);
//        die("<pre>" . print_r($formFields, true) . "</pre>");
        ?>
    </div>
    <div class="col-12">
      <form id="form-<?php echo $attributes['blockId']; ?>">
        <input type="hidden" name="locale" value="<?php echo get_locale(); ?>" />
        <input type="hidden" name="form_id" value="<?php echo $attributes['theform']; ?>" />
        <div>

        </div>
      </form>
      <div id="form-<?php echo $attributes['blockId']; ?>-spinner" class="d-none">
        some spinning gif ...
      </div>
      <div id="form-<?php echo $attributes['blockId']; ?>-thankyou" class="d-none">
        <?php echo $formthankyou; ?>
      </div>
    </div>
    <script>
              var selector = '#form-<?php echo $attributes['blockId']; ?>';

              jQuery(function($) {
                $(selector +' > div').formRender({
                dataType: 'json',
                formData: <?php echo $formdata; ?>
                });
              });

              jQuery(function($) {
                $(selector).on('submit', function(e){
                  e.preventDefault();

                  // Get the form data
                  var postData = $(this).serialize();
                  console.log(postData);

                  // This would be a great place to hide your form and show a spinner
                  jQuery(selector).addClass('d-none');
                  jQuery(selector + '-spinner').removeClass('d-none');

                  // Post the data
                  jQuery.post('/wp-json/project/v2/form', postData, function (response) {
                    console.log(response);
                    // This would be a great place to stop showing your spinner and instead show a thank you.
                    // Or error message...
                    if (response == 'OK') {
                      jQuery(selector + '-spinner').addClass('d-none');
                      jQuery(selector + '-thankyou').removeClass('d-none');
                    } else {
                      alert('Error occurred while submitting the form!');
                      // Reshow the form.
                      jQuery(selector + '-spinner').addClass('d-none');
                      jQuery(selector).removeClass('d-none');
                    }
                  });
                });
              });

    </script>
  </div>
</div>
<?php } ?>
