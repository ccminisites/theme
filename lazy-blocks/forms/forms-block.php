<?php

$blockslug = 'forms';
$blocktitle = 'Form';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-controls-play';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];

// Label
$control                         = [];
$control['label']                = 'Label';
$control['name']                 = 'label';
$control['type']                 = 'text';
$control['child_of']             = '';
$control['width']                = '100';
$control['help']                 = 'Only fill in if needed';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// Form selections
$control                         = [];
$control['label']                = 'Form';
$control['type']                 = 'select';
$control['name']                 = 'form';
$control['default']              = 'contact';
$control['help']                 = '';
$control['save_in_meta']         = false;
$control['required']             = false;
$control['child_of']             = '';
$control['hide_if_not_selected'] = 'false';
$control['placement']            = 'content';
//$control['placement']          = 'inspector';
$control['width']                = '100';
$control['allow_null']           = 'false';
$control['multiple']             = 'false';
$choices = [];
$choices = [
    [
        'label' => 'Contact',
        'value' => 'contact',
    ],
];

$control['choices'] = $choices;
$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;

// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(dirname(__FILE__) . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
