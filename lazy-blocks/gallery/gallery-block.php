<?php
$blockslug = 'gallery';
$blocktitle = 'Image Gallery';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-format-gallery';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = true;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [
	'spacings' => false,
	'display' => false,
	'scrollReveal' => false,
	'frame' => false,
	'customCSS' => false,
];

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];


// START TEXT CONTROL
$control = [];
$control['label'] = 'Title';
$control['type'] = 'text';
$control['name'] = 'title';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL


// START TEXT CONTROL
$control = [];
$control['label'] = 'Post Type Name';
$control['type'] = 'text';
$control['name'] = 'type';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
//$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL

// START TEXT ALIGNMENT CONTROL
$control = [];
$control['label'] = 'Title text alignment';
$control['type'] = 'select';
$control['name'] = 'textalignment';
$control['default'] = 'text-start';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
	[
		'label' => 'Left',
		'value' => 'text-start'
	],
	[
		'label' => 'Center',
		'value' => 'text-center'
	],
	[
		'label' => 'Right',
		'value' => 'text-end'
	]
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT ALIGNMENT CONTROL


// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Pagination';
$control['name']     = 'pagination';
$control['type']     = 'number';
$control['default'] = '12';
$control['help'] = '-1 all';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['min'] = '-1';
$control['max'] = '';
$control['step'] = '1';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// START Pagination Style Control
$control = [];
$control['label'] = 'Pagination Style';
$control['type'] = 'select';
$control['name'] = 'paginationstyle';
$control['default'] = 'generic';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Generic',
        'value' => 'generic'
    ],
    [
        'label' => 'Large',
        'value' => 'large'
    ],
    [
        'label' => 'Load More',
        'value' => 'loadmore'
    ],
    [
        'label' => 'None',
        'value' => 'none'
    ]
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Offset';
$control['name']     = 'offset';
$control['type']     = 'number';
$control['default'] = '0';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['min'] = '0';
$control['max'] = '';
$control['step'] = '1';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(dirname(__FILE__) . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
	lazyblocks()->add_block($block);
}
