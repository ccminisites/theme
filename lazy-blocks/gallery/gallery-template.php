<?php
/** @var array $attributes */
/*
    make sure to create register 'image' field and assign it to your post type.
*/
// Setup
$queryvar = $attributes['blockId'] . '_page'; // work on a custom query url param?
$orderqueryvar = $attributes['blockId'] . '_order'; // work on a custom query url param?
$cutoffqueryvar = $attributes['blockId'] . '_cutoff'; // work on a custom query url param?
$page = (int)($_GET[$queryvar] ?? 0); // what page are we on? 0 - X
$pagination = (int)($attributes['pagination'] ?: 12); // What is our pagination?
$offset = ((int)$attributes['offset']) + $page * $pagination; // Offset from the block plus the offset of the page X pagination
$order = $_GET[$orderqueryvar] ?? 'DESC';
$cutoff = $_GET[$cutoffqueryvar] ?? 'all';


// The query params with out our param in. This way we don't interfer with other stuff.
$get = $_GET;
unset($get[$queryvar]);

$post_type = $attributes['type'] ?: 'post';
//die("<pre>" . print_r($order, true) . "</pre>");($order);
// Arguments for a post query
$args = [
    'post_type' => $post_type,
    'post_status' => 'publish',
    'posts_per_page' => $pagination,
    'order' => $order,
    'orderby' => 'date',
    'offset' => $offset,
];

// Maybe it's multilingual
if (function_exists('pll_current_language')) {
    $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
}

// the gallery query
$myquery = new WP_Query($args);
$gallery_images = $myquery->get_posts();
$pageLink = get_page_link();

?>

<script>
    // gallery plugin
    document.addEventListener('DOMContentLoaded', function () {

        let my_gallery_list = document.getElementById('content_<?= $attributes['blockId'] ?>'),
            options = {
                childList: true
            },
            observer = new MutationObserver(mutuationCallback);

        observer.observe(my_gallery_list, options);

        function mutuationCallback(mutations) {
            for (let mutation of mutations) {
                if (mutation.type === 'childList') {
                    doPopUpSlider();
                }
            }
        }

        function doPopUpSlider() {
            // get all images inside the gallery container
            const gallery_images = document.querySelectorAll('#gallery-items-container img');
            const body = document.querySelector('body');
            const modal_popup = document.querySelector('.gallery-image-modal-popup');
            const images_amount = document.querySelector('.images-amount');
            const close_button = document.querySelector('.fa-times');

            let current_index = 0;
            const prevButton = document.querySelector('.prev-button');
            const nextButton = document.querySelector('.next-button');

            // show the current image
            function showImage(index) {
                gallery_images.forEach((image, currentImage) => {
                    if (currentImage === index) {
                        image.style.display = 'block';
                    }
                });
            }

            function goToPrevious() {
                current_index = (current_index === 0) ? gallery_images.length - 1 : current_index - 1;
                showImage(current_index);
                updateModalPopup();
            }

            function goToNext() {
                current_index = (current_index === gallery_images.length - 1) ? 0 : current_index + 1;
                showImage(current_index);
                updateModalPopup();
            }

            function updateModalPopup() {
                // take the src image already shown on the po up
                // and replaced with the src of the current index
                modal_image('.slider-image-item').src = gallery_images[current_index].src;
                images_amount.innerHTML = current_index + 1 + '/' + gallery_images.length;
            }

            prevButton.addEventListener('click', goToPrevious);
            nextButton.addEventListener('click', goToNext);

            // show index in the popup
            for (let i = 0; i < gallery_images.length; i++) {
                let img = gallery_images[i];
                img.addEventListener('click', e => {
                    body.style.overflow = 'hidden';
                    e.stopPropagation();
                    modal_popup.style.display = 'block';
                    current_index = i;
                    showImage(current_index);
                    updateModalPopup();
                });
            }

            close_button.addEventListener('click', () => {
                body.style.overflow = 'auto';
                modal_popup.style.display = 'none';
            });

            function modal_image(image) {
                return document.querySelector('.gallery-image-modal-popup ' + image);
            }
        }

        doPopUpSlider();
    });

</script>

<div class="lazyblock-gallery-container">
    <div class="lazyblock-gallery">
        <?php if ($attributes["title"] ?? '') { ?>
            <div class="col-12 col-lg-12 gallery-wrapper">
                <div class="col pb-4 <?= $attributes["textalignment"]; ?>">
                    <h2 class="title"><?= $attributes["title"] ?></h2>
                </div>
            </div>
        <?php } ?>

        <?php
        $myquery = new WP_Query($args);
        if ($myquery->have_posts()) {
        $max = $myquery->max_num_pages - (floor($attributes['offset'] / $pagination)); // The max pages with the block offset considered.
        ?>
        <div id="gallery-items-container">
            <div class="row col-12" id="content_<?= $attributes['blockId'] ?>">
                <?php while ($myquery->have_posts()) {
                    $myquery->the_post(); ?>
                    <div class="col-lg-3 col-md-3 col-6 pb-4 gallery-item-wrapper">

                        <div class="card h-100 px-0">
                            <div class="gallery-card-img-wrapper">
                                <?php if (get_field('image')) { ?>
                                    <img src="<?= get_field('image') ?: 'https://via.placeholder.com/150?text=' . get_the_title() ?>"
                                         class="gallery-card-img-top"
                                         alt="<?= get_the_title(); ?>">
                                <?php } ?>
                            </div>
                            <!--<div class="gallery-title-wrapper">
                                <h3 class="gallery-card-title">
                                        <?= get_the_title(); ?>
                                </h3>
                            </div>-->
                        </div>
                    </div>
                <?php } ?>
                <?php } else { ?>
                    <div class="row nothingtoshow">
                        <div class="col-12">
                            <p><?php pll_e('nothing_to_show'); ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <!-- Modal Markup -->
        <div class="gallery-image-modal-popup">
            <div class="wrapper">
                <span><i class="fas fa-times"></i></span>
                <img class="slider-image-item" src="" alt="Image Modal">
                <p class="images-amount"></p>

                <a class="prev-button"><i class="fas fa-chevron-left"></i></a>
                <a class="next-button"><i class="fas fa-chevron-right"></i></a>
            </div>
        </div>

        <?php if ($attributes['paginationstyle'] !== 'none') { ?>
            <?php
            $max = $myquery->max_num_pages - (floor($attributes['offset'] / $pagination));
            // Only show pager if there is more than one page.
        if ($max > 1) {
            ?>
            <div
                    class="row pagination <?= $attributes['paginationstyle'] === 'generic' ? ' pagination_generic' : '' ?><?= $attributes['paginationstyle'] === 'large' ? ' pagination_large' : '' ?><?= $attributes['paginationstyle'] === 'loadmore' ? ' d-none' : '' ?>"
                    id="pagination_<?= $attributes['blockId'] ?>">
                <a class="pagination_link pagination_link_first"
                   href="<?= $get ? '?' . http_build_query($get) : './' // query url to go to the first page with disturbing others.      ?>"><?php pll_e('First') ?></a>
                <?php
                $i = 0;
                $query = $get; // url query without our queryvar in.
                while ($i++ < $max) {
                    $query[$queryvar] = $i - 1;
                    $queryparams = http_build_query($query); // query url params with our page.
                    ?>
                    <a
                            class="pagination_link<?= $page === $i - 1 ? ' active' : '' ?> pagination_link_pagediff-<?= $page - ($i - 1) ?>"
                            href="?<?= $queryparams ?>"><?= $i ?></a>
                <?php } ?>
                <a class="pagination_link pagination_link_last"
                   href="?<?= http_build_query($get + [$queryvar => ($max - 1)]) // query url with the last page      ?>"><?php pll_e('Last') ?></a>
            </div>


        <?php if ($attributes['paginationstyle'] === 'loadmore' && ($max - 1)) { ?>

            <div class="row pagination pagination_loadmore">
                <a href="#" data-page="0" data-max="<?= $max - 1 ?>" data-target="content_<?= $attributes['blockId'] ?>"
                   data-base="<?= http_build_query($get) ?>" data-queryvar="<?= $queryvar ?>"
                   class="pagination_loadmore btn btn-primary"
                   id="pagination_loadmore_<?= $attributes['blockId'] ?>"
                ><?php pll_e('load_more'); ?></a>
                <span class="d-none" id="hidden_content_<?= $attributes['blockId'] ?>"></span>
            </div>
            <script>
                jQuery(document).ready(function () {
                    jQuery('#pagination_loadmore_<?= $attributes['blockId'] ?>').on('click', function (e) {
                        e.preventDefault();
                        let dis = jQuery(this); // save this for later
                        let page = jQuery(this).data('page'); // get the page we are on

                        if (typeof page !== "undefined" && !dis.hasClass('processing')) { // check the page and make sure we are not already processing.
                            let base = jQuery(this).data('base'); // base url
                            let max = jQuery(this).data('max'); // the max page we can go to
                            let queryvar = jQuery(this).data('queryvar'); // the url param we are working on
                            let target = jQuery(this).data('target'); // the target div
                            dis.addClass('processing'); // turn on the processing
                            let url = '?' + base + '&' + queryvar + '=' + (page + 1); // make the url we need to load up.
                            jQuery('#hidden_' + target).load(url + ' #' + target, function () { // load the div in to the hidden
                                jQuery('#' + target).append(jQuery('#hidden_' + target + ' > div').html()); // append the html of the div within the hidden
                                jQuery('#hidden_' + target).html(''); // blank out the hidden
                                page++; // increase the page
                                dis.data('page', page); // change the data on the link
                                dis.removeClass('processing'); // remove processing
                                if (page === max) {
                                    dis.addClass('d-none'); // hide the link if we are at the end
                                }
                            });
                        }
                    });
                });
            </script>
        <?php } ?>
        <?php
        }
        ?>
        <?php } else { ?>
            <div class="row nothingtoshow">
                <div class="col-12">
                    <p><?php pll_e('nothing_to_show'); ?></p>
                </div>
            </div>
        <?php } ?>
        <?php wp_reset_query(); ?>

    </div>
</div>

