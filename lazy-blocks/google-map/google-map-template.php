<style>
    .border-radius {
        border-radius: 0 !important;
    }
</style>
<div class="container-md">
    <div class="row lazyblock-google-map justify-content-center">
        <div class="col-12 col-lg-{{layout}} order-2 order-md-1 pb-4 pb-lg-0 mapouter">
            <div class="gmap_canvas {{#if border-radius}}border-radius{{/if}}">
                <iframe width="100%" height="100%" id="gmap_canvas"
                        src="https://maps.google.com/maps?q={{#urlEncode query}}&t=&z=11&ie=UTF8&iwloc=&output=embed"
                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                <a class="d-none" href="https://www.embedgooglemap.net">embed google map</a>
            </div>
        </div>
        <div class="col-12 col-lg-{{#leftover layout}} order-1 order-md-2 pb-4 pb-lg-0 {{#if hide-block}}d-none{{/if}}">
            <div class="card h-100 {{#if border-radius}}border-radius{{/if}}">
                <div class="card-body flex-column h-100">
                    <div class="title">
                        <h4>{{title}}</h4>
                    </div>
                    <div class="location-wrapper blockquote-line">
                        <p class="location">{{street}}</p>
                        <p class="location">{{city}}</p>
                        {{#if country}}
                        <p class="location">{{country}}</p>
                        {{/if}}
                        <br>
                        <p class="phone">
                            <a href="tel:{{phone}}" class="phone">{{phone}}</a>
                        </p>
                    </div>
                    {{#if label}}
                    <div class="cta-button text-{{alignment}}">
                        <a class="btn {{buttonstyle}} {{#if outline}}outline{{/if}}" href="{{url}}" target="{{target}}">{{label}}</a>
                    </div>
                    {{/if}}
                </div>
            </div>
        </div>
    </div>
</div>
