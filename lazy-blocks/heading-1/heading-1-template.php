<div class="container-md">
    <div class="row lazyblock-heading-1 {{alignment}}">
        <div class="col-12 col-lg-{{division}} text-{{textalignment}}">
            <h1>{{{text}}}</h1>
        </div>
    </div>
</div>
