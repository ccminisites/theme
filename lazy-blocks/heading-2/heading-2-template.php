<div class="container-md">
    <div class="row lazyblock-heading-2 {{alignment}}">
        <div class="col-12 col-lg-{{division}} text-{{textalignment}}">
            <h2>{{{text}}}</h2>
        </div>
    </div>
</div>
