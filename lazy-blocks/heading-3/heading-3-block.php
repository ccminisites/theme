<?php

$blockslug = 'heading-3';
$blocktitle = 'Heading 3';

// START THE BLOCK
$block             = [];
$block['slug']     = 'lazyblock/' . $blockslug;
$block['title']    = $blocktitle;
$block['icon']     = 'dashicons dashicons-buddicons-tracking';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];

// START TEXT CONTROL
$control = [];
$control['label'] = 'Heading text';
$control['type'] = 'text';
$control['name'] = 'text';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';

$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL

// Division
$control              = [];
$control['label']     = 'Division';
$control['type']      = 'radio';
$control['name']      = 'division';
$control['default']   = '10';
$control['help'] = '';
$control['child_of']  = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required']  = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Default',
        'value' => '10',
    ],
    [
        'label' => 'Full width',
        'value' => '12',
    ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END RADIO CONTROL

// Alignment
$control              = [];
$control['label']     = 'Heading text alignment';
$control['type']      = 'select';
$control['name']      = 'textalignment';
$control['default']   = 'start';
$control['help'] = '';
$control['child_of']  = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required']  = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'start',
    ],
    [
        'label' => 'Center',
        'value' => 'center',
    ],
    [
        'label' => 'Right',
        'value' => 'end',
    ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SELECT CONTROL

// Alignment
$control              = [];
$control['label']     = 'Heading alignment';
$control['type']      = 'select';
$control['name']      = 'alignment';
$control['default']   = 'justify-content-center';
$control['help'] = '';
$control['child_of']  = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required']  = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'justify-content-start',
    ],
    [
        'label' => 'Center',
        'value' => 'justify-content-center',
    ],
    [
        'label' => 'Right',
        'value' => 'justify-content-end',
    ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SELECT CONTROL

// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'html';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];


if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
