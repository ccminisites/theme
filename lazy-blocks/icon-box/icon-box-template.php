<?php
/** @var array $attributes */
// cleaning classes and block variables.
$blockID = $attributes['blockId'];
?>

<style>
    #icon-box-card-<?= $blockID; ?> {
        padding-top: <?= $attributes['custompaddingtop']; ?>rem;
        padding-bottom: <?= $attributes['custompaddingbottom']; ?>rem;
    }

    #icon-box-card-<?= $blockID; ?> {
        margin-top: <?= $attributes['custommargintop']; ?>rem;
        margin-bottom: <?= $attributes['custommarginbottom']; ?>rem;
    }
</style>

<div class="container-md <?= $attributes['blockUniqueClass'] ?> <?= $attributes['className'] ?>">
    <div class="lazyblock-icon-box-cards"
         id="icon-box-card-<?= $blockID; ?>">
        <div class="row gy-5 gx-0 g-md-<?= $attributes['gutter']; ?> row-cols-1 row-cols-lg-3 row-cols-xl-<?= $attributes['layout']; ?>">
            <?php foreach ($attributes['box-items'] as $value) {
                $box_content = $value['box-content'];
                ?>
                <div class="col mb-5">
                    <div class="card h-100 <?= $attributes['border-radius'] ? 'border-radius' : 'rounded-0'; ?>">
                        <div class="card-content-wrapper h-100 <?= $attributes['textalignment']; ?>">
                            <?php if ($box_content) { ?>
                                <div class="card-content">
                                    <?= $box_content; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>