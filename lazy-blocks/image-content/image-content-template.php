<div class="container-md">
    <div class="row lazyblock-image-content {{flex-centered}}">
        <div class="col-lg-{{division}}">
            <img src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}"/>
        </div>
        <div class="col-lg-{{#leftover division}} col-12 mb-lg-0 mb-4">
            <InnerBlocks />
        </div>
    </div>
</div>
