<?php

$blockslug = 'image-image';

// Start the block
$block             = [];
$block['title']    = 'Image Image';
$block['icon']     = 'dashicons dashicons-format-image';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];
///////////// START CONTROLS /////////////

// Image
$control                         = [];
$control['label']                = 'Image';
$control['name']                 = 'image';
$control['type']                 = 'image';
$control['width']                = '50';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-image-image-image';

// Add the control to the controls
$controls[$control_id] = $control;
// Image
$control                         = [];
$control['label']                = 'Image 2';
$control['name']                 = 'image2';
$control['type']                 = 'image';
$control['width']                = '50';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-image-image-image2';

// Add the control to the controls
$controls[$control_id] = $control;

// START TOGGLE
$control = [];
$control['label']                = 'Border radius on images?';
$control['name']                 = 'border-radius';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Border Radius';

// Make an id.
$control_id = 'control-image-content-border-radius';

// Add the control to the controls
$controls[$control_id] = $control;

// Radio
$control              = [];
$control['label']     = 'Division';
$control['name']      = 'division';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '6';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => '7-5',
        'value' => '7',
    ],
    [
        'label' => '6-6',
        'value' => '6',
    ],
    [
        'label' => '5-7',
        'value' => '5',
    ],
];


// Make an id.
$control_id = 'control-image-content-division';

// Add the control to the controls
$controls[$control_id] = $control;


// Radio
$control              = [];
$control['label']     = 'Image Centered?';
$control['name']      = 'flex-centered';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'No',
        'value' => '',
    ],
    [
        'label' => 'Yes',
        'value' => 'flex-centered',
    ],
];


// Make an id.
$control_id = 'control-content-flex-centered';

// Add the control to the controls
$controls[$control_id] = $control;

// START TOGGLE
$control = [];
$control['label']                = 'Make image cover height?';
$control['name']                 = 'cover';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Cover height';

// Make an id.
$control_id = 'control-image-cover';
// Add the control to the controls
$controls[$control_id] = $control;

///////////// END CONTROLS /////////////
// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
