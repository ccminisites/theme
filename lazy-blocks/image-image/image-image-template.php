<div class="container-md">
    <div class="row lazyblock-image-image {{flex-centered}} {{#if border-radius}}border-radius{{/if}}">
        <div class="col-lg-{{division}} col-12 mb-lg-0 mb-4 image-first">
            <img class="{{#if cover}}cover{{/if}}" src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}"/>
        </div>
        <div class="col-lg-{{#leftover division}} col-12 image-second">
            <img class="{{#if cover}}cover{{/if}}" src="{{image2.url}}" alt="{{image2.alt}}" title="{{image2.alt}}"/>
        </div>
    </div>
</div>
