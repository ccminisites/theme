<?php

$blockslug = 'image';

// Start the block
$block             = [];
$block['title']    = 'Image';
$block['icon']     = 'dashicons dashicons-format-image';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];
///////////// START CONTROLS /////////////

// Image
$control                         = [];
$control['label']                = 'Image';
$control['name']                 = 'image';
$control['type']                 = 'image';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-image-image';

// Add the control to the controls
$controls[$control_id] = $control;

// START TOGGLE
$control = [];
$control['label']                = 'Border radius';
$control['name']                 = 'border-radius';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Border Radius';

// Make an id.
$control_id = 'control-image-border-radius';

// Add the control to the controls
$controls[$control_id] = $control;

// Radio
$control              = [];
$control['label']     = 'Division';
$control['name']      = 'division';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '8';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Default',
        'value' => '8',
    ],
    [
        'label' => 'Full width',
        'value' => '12',
    ],
];


// Make an id.
$control_id = 'control-image-division';

// Add the control to the controls
$controls[$control_id] = $control;

// Alignment
$control              = [];
$control['label']     = 'Alignment';
$control['name']      = 'alignment';
$control['type']      = 'select';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'left';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Left',
        'value' => 'justify-content-start',
    ],
    [
        'label' => 'Center',
        'value' => 'justify-content-center',
    ],
    [
        'label' => 'Right',
        'value' => 'justify-content-end',
    ],
];

// Make an id.
$control_id = 'control-image-alignment';

// Add the control to the controls
$controls[$control_id] = $control;

include __DIR__ . '/../extraspace-zero-default.php';

$control                         = [];
$control['label']                = 'Use custom Image size ?';
$control['name']                 = 'imagesize-toggle';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Yes';
$control['save_in_meta']          = false;

// Make an id.
$control_id = 'imagesize-toggle';

// Add the control to the controls
$controls[$control_id] = $control;

$control                         = [];
$control['label']                = 'Image size (percentage)';
$control['name']                 = 'imagesize';
$control['type']                 = 'number';
$control['child_of']             = '';
$control['min']                  = '0';
$control['max']                  = '100';
$control['step']                 = '1';
$control['default']              = '100';
$control['placement']            = 'inspector';

// Make an id.
$control_id = 'imagesize';

// Add the control to the controls
$controls[$control_id] = $control;

$control                         = [];
$control['label']                = 'Image alignment center (when needed)';
$control['name']                 = 'imagealignment';
$control['type']                 = 'toggle';
$control['child_of']             = '';
$control['required'] 			 = false;
$control['default']              = false;
$control['checked']              = true;
$control['placement']            = 'inspector';

// Make an id.
$control_id = 'imagealignment';

// Add the control to the controls
$controls[$control_id] = $control;

// URL
$control             = [];
$control['label']    = 'Url';
$control['name']     = 'url';
$control['type']     = 'url';
$control['help'] = 'If needed, please insert internal or external url';
$control['child_of'] = '';
$control['placement'] = 'content';
$control['width'] = '50';
$control['save_in_meta'] = false;

// Make an id.
$control_id = 'control-image-url';

// Add the control to the controls
$controls[$control_id] = $control;

// Target
$control              = [];
$control['label']     = 'Open Link in New Tab?';
$control['name']      = 'target';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '_self';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Yes',
        'value' => '_blank',
    ],
    [
        'label' => 'No',
        'value' => '_self',
    ],
];

// Make an id.
$control_id = 'control-image-url-target';

// Add the control to the controls
$controls[$control_id] = $control;

///////////// END CONTROLS /////////////
// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
