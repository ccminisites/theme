<style>
    #image-{{blockId}} {
    padding-top: {{custompaddingtop}}rem;
    padding-bottom: {{custompaddingbottom}}rem;
    }
    {{#if imagesize-toggle}}
    #image-{{blockId}} img {
    width: {{imagesize}}%;
    }
    {{/if}}
    {{#if imagealignment}}
    .image {
        display: flex;
        align-content: center;
        justify-content: center;
    }
    {{/if}}
    @media (max-width: 767.98px) {
    }
</style>

<div class="container-md">
    <div class="row lazyblock-image {{alignment}}" id="image-{{blockId}}">
        <div class="col-12 col-lg-{{division}} image">
            {{#if url}}
            <a href="{{url}}" class="image-link" target="{{target}}">
            {{/if}}
                <img class="{{#if border-radius}}border-radius{{/if}}" src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}"/>
            {{#if url}}
            </a>
            {{/if}}
        </div>
    </div>
</div>
