<?php



$blockslug = 'members';
$blocktitle = 'Members';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-megaphone';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];




// START TEXT CONTROL
$control = [];
$control['label'] = 'Title';
$control['type'] = 'text';
$control['name'] = 'title';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL


// START TEXT ALIGNMENT CONTROL
$control = [];
$control['label'] = 'Title text alignment';
$control['type'] = 'select';
$control['name'] = 'textalignment';
$control['default'] = 'text-center';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'text-start'
    ],
    [
        'label' => 'Center',
        'value' => 'text-center'
    ],
    [
        'label' => 'Right',
        'value' => 'text-end'
    ]
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT ALIGNMENT CONTROL


// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Amount of members (newest first)';
$control['name']     = 'amount_of_members';
$control['type']     = 'number';
$control['default'] = '';
$control['help'] = 'Tip if you put -1 you will get all of them';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['min'] = '-1';
$control['max'] = '';
$control['step'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END NUMBER CONTROL


// START TEXT CONTROL
$control = [];
$control['label'] = 'CTA Label';
$control['type'] = 'text';
$control['name'] = 'label';
$control['default'] = '';
$control['help'] = 'Only use if you need a general cta button';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '40';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL


// START URL CONTROL
$control = [];
$control['label'] = 'CTA Url';
$control['type'] = 'url';
$control['name'] = 'url';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '40';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END URL CONTROL


// START TARGET CONTROL
$control = [];
$control['label'] = 'Open Url in New Tab?';
$control['type'] = 'radio';
$control['name'] = 'target';
$control['default'] = '_blank';
$control['checked'] = true;
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '20';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$choices = [];
$choices[] = [
    'label' => 'Yes',
    'value' => '_blank'
];
$choices[] = [
    'label' => 'No',
    'value' => '_self'
];
$control['choices'] = $choices;

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TARGET CONTROL


// START BTN STYLE CONTROL
$control = [];
$control['label'] = 'Button Style';
$control['type'] = 'select';
$control['name'] = 'buttonstyle';
$control['default'] = 'btn-primary';
$control['help'] = 'Only needed for cta button';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Primary',
        'value' => 'btn-primary'
    ],
    [
        'label' => 'Secondary',
        'value' => 'btn-secondary'
    ],
    [
        'label' => 'Supporting',
        'value' => 'btn-supporting'
    ]
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END BTN STYLE CONTROL

// START TOGGLE
$control = [];
$control['label'] = 'Button Outline style';
$control['type'] = 'toggle';
$control['name'] = 'outline';
$control['default'] = false;
$control['checked'] = false;
$control['alongside_text'] = 'Outline';
$control['help'] = 'Only needed for cta button';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TOGGLE


// START BTN ALIGNMENT CONTROL
$control = [];
$control['label'] = 'Button Alignment';
$control['type'] = 'select';
$control['name'] = 'alignment';
$control['default'] = 'start';
$control['help'] = 'Only needed for cta button';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'start'
    ],
    [
        'label' => 'Center',
        'value' => 'center'
    ],
    [
        'label' => 'Right',
        'value' => 'end'
    ]
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END BTN ALIGNMENT CONTROL


// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(dirname(__FILE__) . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
