<?php
/** @var array $attributes */


$args = [
    'post_type' => 'member',
    'post_status' => 'publish',
    'posts_per_page' => $attributes['amount_of_members'],
    'order' => 'DESC',
    'orderby' => 'publish_date',
];

// Maybe it's multilingual
if (function_exists('pll_current_language')) {
    $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
} ?>

<div class="container-md">
    <div class="row lazyblock-members justify-content-center">
        <div class="col-12 col-lg-9 members-wrapper">
            <?php if ($attributes["title"] ?? '') { ?>
                <div class="row">
                    <div class="col pb-4 <?= $attributes["textalignment"]; ?>">
                        <h2 class="title"><?= $attributes["title"] ?></h2>
                    </div>
                </div>
            <?php } ?>
        </div>
            <?php
            // Custom query.
            $myquery = new WP_Query($args);
            $members = $myquery->get_posts();
            ?>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <?php if ($members) { ?>

                <?php foreach ($members as $member) { ?>
                    <div class="col pb-4 member-item-wrapper">
                        <div class="card h-100 px-0">
                            <div class="member-card-img-wrapper">
                                <?php if (get_field('image', $member)) { ?>
                                <img src="<?= get_field('image', $member) ?: 'https://via.placeholder.com/320x240?text='.get_the_title($member)  ?>" class="member-card-img-top"
                                     alt="<?= get_the_title($member) ?>">
                                <?php } ?>
                            </div>
                            <div class="card-body flex-column h-100">
                                <div class="member-card-title">
                                    <p><?= get_field('title', $member) ?></p>
                                </div>
                                <div class="member-card-content">
                                    <?= get_field('content', $member) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            <?php } else { ?>
                <div class="col no-members">
                    <div class="col">
                        <p><?php pll_e('No Members'); ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php
// Restore original post data.
wp_reset_postdata();
?>

