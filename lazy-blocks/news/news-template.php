<?php
/** @var array $attributes */


$args = [
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => $attributes['amount_of_news_items'],
    'order' => 'DESC',
    'orderby' => 'publish_date',
];

// Maybe it's multilingual
if (function_exists('pll_current_language')) {
    $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
}
?>
<div class="container-md">
    <div class="row lazyblock-news justify-content-center">
        <div class="col-12 col-lg-9 news-wrapper">
            <?php if ($attributes["title"] ?? '') { ?>
                <div class="row">
                    <div class="col <?= $attributes["textalignment"]; ?>">
                        <h2 class="title"><?= $attributes["title"] ?></h2>
                    </div>
                </div>
            <?php } ?>
            <?php
            // Custom query.
            $myquery = new WP_Query($args);
            // Check that we have query results.
            if ($myquery->have_posts()) {
            // Start looping over the query results.
            while ($myquery->have_posts()) {
            $myquery->the_post();
                $categories = get_the_category();
                ?>
                    <div class="row pt-4 news-item-wrapper">
                        <?php if ($categories) { ?>
                        <div class="news-item-category"><?= ucfirst($categories[0]->name); ?></div>
                        <?php } ?>
                        <div class="news-item-title"><h4>
                                <a href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a>
                            </h4></div>
                        <div class="news-item-excerpt">
                            <?= project_truncate(get_the_excerpt(), '56'); ?>
                        </div>
                        <div class="news-item-cta"><a
                                href="<?= get_the_permalink() ?>"><?= pll__('Read more'); ?></a></div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="row no-news">
                    <div class="col">
                        <p><?php pll_e('No News'); ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php
// Restore original post data.
wp_reset_postdata();
?>
