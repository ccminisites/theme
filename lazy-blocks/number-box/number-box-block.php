<?php

$blockslug = 'number-box';
$blocktitle = 'Number Box';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-admin-page';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = true;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [
    'spacings' => false,
    'display' => false,
    'scrollReveal' => false,
    'frame' => false,
    'customCSS' => false,
];

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];


// Holder for the controls
$controls = [];

// START REPEATER CONTROL
$control = [];
$control['label'] = 'Number-Content';
$control['type'] = 'repeater';
$control['name'] = 'number-content-elements';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['rows_min'] = '1';
$control['rows_max'] = '10';
$control['rows_label'] = 'Number Content {{#}}';
$control['rows_add_button_label'] = '+ Add Content';
//$control['rows_collapsible']      = true;
//$control['rows_collapsed']        = true;

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END REPEATER CONTROL

// number
$control = [];
$control['label'] = 'Number';
$control['name'] = 'number';
$control['type'] = 'number';
$control['child_of'] = '';
$control['child_of'] = 'control-' . $blockslug . '-number-content-elements';
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['placement'] = 'content';
$control['help'] = '';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-number-content-elements-' . $control['name'];
$controls[$control_id] = $control;
// END number

// Content
$control = [];
$control['label'] = 'Content';
$control['name'] = 'content';
$control['type'] = 'classic_editor';
$control['child_of'] = 'control-' . $blockslug . '-number-content-elements';
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['placement'] = 'content';
$control['help'] = '';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-number-content-elements-' . $control['name'];
$controls[$control_id] = $control;
// END CONTENT


// START TOGGLE
$control = [];
$control['label'] = 'Border radius';
$control['name'] = 'border-radius';
$control['type'] = 'toggle';
$control['placement'] = 'inspector';
$control['required'] = false;
$control['default'] = true;
$control['checked'] = true;
$control['child_of'] = '';
$control['alongside_text'] = 'Border Radius';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TOGGLE


// START NUMBER CONTROL
$control = [];
$control['label'] = 'Gutter control cards';
$control['type'] = 'number';
$control['name'] = 'gutter';
$control['help'] = 'Value between 0-5';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['min'] = '0';
$control['max'] = '5';
$control['step'] = '1';
$control['default'] = '4';
$control['hide_if_not_selected'] = 'false';
$control['hide_if_not_selected'] = false;
$control['save_in_meta_name'] = false;
$control['save_in_meta'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['multiple'] = 'false';
$control['output_format'] = '';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END

// Layout
$control = [];
$control['label'] = 'Layout';
$control['name'] = 'layout';
$control['type'] = 'select';
$control['placement'] = 'inspector';
$control['required'] = true;
$control['default'] = '3';
$control['child_of'] = '';
$control['hide_if_not_selected'] = 'false';
$control['placement'] = 'inspector';
$control['hide_if_not_selected'] = false;
$control['save_in_meta_name'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['multiple'] = 'false';
$control['output_format'] = '';

$control['choices'] = [
    [
        'label' => '2 by 2',
        'value' => '2',
    ],
    [
        'label' => '3 by 3',
        'value' => '3',
    ],
    [
        'label' => '4 by 4',
        'value' => '4',
    ],
    [
        'label' => '5 by 5',
        'value' => '5',
    ],
];

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END


include __DIR__ . '/../extraspace.php';
include __DIR__ . '/../extraspace-margin.php';

// Add the controls to the block
$block['controls'] = $controls;

// START CODE
$code = [];
$code['show_preview'] = 'never';
$code['single_output'] = false;
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['editor_css'] = '';
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
