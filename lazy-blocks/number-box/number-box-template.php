<?php
/** @var array $attributes */
// cleaning classes and block variables.
$blockID = $attributes['blockId'];
?>

<style>
    #number-box-<?= $blockID; ?> {
        padding-top: <?= $attributes['custompaddingtop']; ?>rem;
        padding-bottom: <?= $attributes['custompaddingbottom']; ?>rem;
    }

    #number-box-<?= $blockID; ?> {
        margin-top: <?= $attributes['custommargintop']; ?>rem;
        margin-bottom: <?= $attributes['custommarginbottom']; ?>rem;
    }
</style>

<script>

    jQuery(document).ready(function ($) {

        function counter_me(element) {
            let $numberBox = $(element).find('.number-box-counter');
            let number = parseFloat($(element).data('number'));

            $numberBox.stop(true, true);

            $({counter: 0}).animate(
                {
                    counter: number
                },
                {
                    duration: 4000,
                    step: function (currentValue) {
                        $numberBox.text(Math.ceil(currentValue));
                    },
                    complete: function () {
                    }
                }
            );
        }

        function triggerAnimations() {
            //console.log('Function triggered');
            $('.col[data-number]').each(function () {
                counter_me(this);
            });
        }

        triggerAnimations();

        let resizeTimer;
        window.addEventListener('resize', function () {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function () {
                console.log('Resized');
                triggerAnimations();
            }, 500);
        });
    });


</script>


<div class="container-md <?= $attributes['blockUniqueClass'] ?> <?= $attributes['className'] ?>">
    <div class="lazyblock-number-box"
         id="number-box-<?= $blockID; ?>">
        <div class="row gy-5 gx-0 g-md-<?= $attributes['gutter']; ?> row-cols-1 row-cols-lg-2 row-cols-xl-<?= $attributes['layout']; ?>"
        ">
        <?php foreach ($attributes['number-content-elements'] as $value) {
            $number = $value['number'];
            $content = $value['content'];
            ?>
            <div class="col" data-number="<?= $number; ?>">
                <div class="card h-100 <?= $attributes['border-radius'] ? 'border-radius' : 'rounded-0'; ?>">
                    <div class="card-content-wrapper h-100 d-flex align-content-center justify-content-center">
                        <?php if (empty($content)) { ?>
                            <div class="number-box-counter"></div>
                        <?php } else { ?>
                            <div class="d-flex justify-content-around align-items-center">
                                <?php if ($number) { ?>
                                    <div class="number-box-counter"></div>
                                <?php } ?>
                                <?php if ($content) { ?>
                                    <div class="number-box-content">
                                        <?= $content; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>