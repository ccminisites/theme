<?php

$blockslug = 'pricing-table';
$blocktitle = 'Pricing Table';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-editor-table';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = true;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [
    'spacings' => false,
    'display' => false,
    'scrollReveal' => false,
    'frame' => false,
    'customCSS' => false,
];

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];


// Holder for the controls
$controls = [];

// Content
$control = [];
$control['label'] = 'Cards';
$control['name'] = 'card-items';
$control['type'] = 'repeater';
$control['rows_min'] = '1';
$control['rows_max'] = '';
$control['rows_label'] = 'Price {{#}}';
$control['rows_add_button_label'] = '+ Add Card';
$control['rows_collapsible'] = 'true';
$control['rows_collapsed'] = 'true';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['child_of'] = '';
$control['hide_if_not_selected'] = 'false';
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['translate'] = 'false';


// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END

// Title
$control = [];
$control['label'] = 'Title';
$control['name'] = 'card-title';
$control['type'] = 'text';
$control['save_in_meta'] = false;
$control['child_of'] = 'control-' . $blockslug . '-card-items';
$control['hide_if_not_selected'] = 'false';
$control['default'] = '';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';


// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END TITLE

// Price
$control = [];
$control['label'] = 'Price';
$control['name'] = 'price-amount';
$control['type'] = 'text';
$control['save_in_meta'] = false;
$control['child_of'] = 'control-' . $blockslug . '-card-items';
$control['hide_if_not_selected'] = 'false';
$control['default'] = '';
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END TITLE

// highlight the price
$control = [];
$control['label'] = 'highlight the price with special text';
$control['name'] = 'highlight-price';
$control['type'] = 'text';
$control['save_in_meta'] = false;
$control['child_of'] = 'control-' . $blockslug . '-card-items';
$control['hide_if_not_selected'] = 'false';
$control['default'] = '';
$control['required'] = false;
$control['save_in_meta_name'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END TITLE

// Content
$control = [];
$control['label'] = 'Content';
$control['name'] = 'card-content';
$control['type'] = 'classic_editor';
$control['child_of'] = 'control-' . $blockslug . '-card-items';
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END CONTENT


// Label
$control = [];
$control['label'] = 'CTA Label';
$control['name'] = 'cta-label';
$control['type'] = 'text';
$control['child_of'] = 'control-' . $blockslug . '-card-items';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END CTA LABEL


// URL
$control = [];
$control['label'] = 'Url';
$control['name'] = 'cta-url';
$control['type'] = 'url';
$control['save_in_meta'] = false;
$control['child_of'] = 'control-' . $blockslug . '-card-items';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END URL

// Style
$control = [];
$control['type'] = 'select';
$control['name'] = 'buttonstyle';
$control['default'] = 'btn-primary';
$control['label'] = 'Button style';
$control['save_in_meta'] = false;
$control['required'] = false;
$control['allow_null'] = 'true';
$control['child_of'] = '';
$control['hide_if_not_selected'] = 'false';
$control['placement'] = 'inspector';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

$control['choices'] = [
    [
        'label' => 'Primary',
        'value' => 'btn-primary',
    ],
    [
        'label' => 'Secondary',
        'value' => 'btn-secondary',
    ],
    [
        'label' => 'Supporting',
        'value' => 'btn-supporting',
    ],
];

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END BTN STYLE


// Layout
$control = [];
$control['label'] = 'Layout';
$control['name'] = 'layout';
$control['type'] = 'select';
$control['placement'] = 'inspector';
$control['required'] = true;
$control['default'] = '3';
$control['child_of'] = '';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

$control['choices'] = [
    [
        'label' => '2 by 2',
        'value' => '2',
    ],
    [
        'label' => '3 by 3',
        'value' => '3',
    ],
    [
        'label' => '4 by 4',
        'value' => '4',
    ],
    [
        'label' => '5 by 5',
        'value' => '5',
    ],
];

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END LAYOUT

// START NUMBER CONTROL
$control = [];
$control['label'] = 'Gutter control cards';
$control['type'] = 'number';
$control['name'] = 'gutter';
$control['help'] = 'Value between 0-5';
$control['child_of'] = '';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['min'] = '0';
$control['max'] = '5';
$control['step'] = '1';
$control['default'] = '4';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END NUMBER CONTROL


// START TOGGLE
$control = [];
$control['label'] = 'Border radius';
$control['name'] = 'border-radius';
$control['type'] = 'toggle';
$control['placement'] = 'inspector';
$control['required'] = false;
$control['default'] = true;
$control['checked'] = true;
$control['child_of'] = '';
$control['alongside_text'] = 'Border Radius';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TOGGLE


// START TEXT ALIGNMENT CONTROL
$control = [];
$control['label'] = 'Text alignment';
$control['type'] = 'select';
$control['name'] = 'textalignment';
$control['default'] = 'text-start';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'text-start'
    ],
    [
        'label' => 'Center',
        'value' => 'text-center'
    ],
    [
        'label' => 'Right',
        'value' => 'text-end'
    ]
];

$control['choices'] = $choices;

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT ALIGNMENT CONTROL

// Alignment
$control = [];
$control['label'] = 'CTA alignment';
$control['name'] = 'cta-alignment';
$control['type'] = 'select';
$control['placement'] = 'inspector';
$control['required'] = true;
$control['default'] = 'justify-content-start';
$control['child_of'] = '';
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';

$control['choices'] = [
    [
        'label' => 'Left',
        'value' => 'justify-content-start',
    ],
    [
        'label' => 'Center',
        'value' => 'justify-content-center',
    ],
    [
        'label' => 'Right',
        'value' => 'justify-content-end',
    ],
];

// Make an id and add it to control
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

include __DIR__ . '/../extraspace.php';
include __DIR__ . '/../extraspace-margin.php';

// Add the controls to the block
$block['controls'] = $controls;

// START CODE
$code = [];
$code['show_preview'] = 'never';
$code['single_output'] = false;
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['editor_css'] = '';
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
