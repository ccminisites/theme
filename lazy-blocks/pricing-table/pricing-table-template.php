<?php
/** @var array $attributes */
// cleaning classes and block variables.
$blockID = $attributes['blockId'];

?>

<div class="container-md">
    <div class="lazyblock-pricing-cards"
         id="pricing-card-<?= $blockID; ?>">
        <div class="row gy-5 gx-0 g-md-<?= $attributes['gutter']; ?> row-cols-1 row-cols-lg-3 row-cols-xl-<?= $attributes['layout']; ?>">
            <?php foreach ($attributes['card-items'] as $value) {
                $title = $value['card-title'];
                $price_amount = $value['price-amount'];
                $card_content = $value['card-content'];
                $cta_label = $value['cta-label'];
                $cta_url = $value['cta-url'];
                $highlight_text = $value['highlight-price'];
                ?>

                <div class="col mb-5">
                    <div class="card h-100 <?= $attributes['border-radius'] ? 'border-radius' : 'rounded-0'; ?>">
                        <div class="card-content-wrapper h-100 <?= $attributes['textalignment']; ?>">
                            <?php if ($highlight_text) { ?>
                                <span class="highlight-price">
                                    <?= $highlight_text; ?>
                                </span>
                            <?php } ?>

                            <?php if ($title) { ?>
                                <h4 class="price-title">
                                    <?= $title; ?>
                                </h4>
                            <?php } ?>

                            <?php if ($price_amount) { ?>
                                <h5 class="price-amount">
                                    <?= $price_amount; ?>
                                </h5>
                            <?php } ?>

                            <?php if ($card_content) { ?>
                                <div class="card-content">
                                    <?= $card_content; ?>
                                </div>
                            <?php } ?>

                            <?php if ($cta_label) { ?>
                                <div class="card-cta d-flex <?= $attributes['cta-alignment']; ?> py-3">
                                    <a class="btn rounded-0 <?= $attributes['buttonstyle'] ?>" href="<?= $cta_url; ?>">
                                        <?= $cta_label; ?>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>