<?php
/** @var array $attributes */


$args = [
    'post_type' => 'project',
    'post_status' => 'publish',
    'posts_per_page' => $attributes['amount_of_projects'],
    'order' => 'DESC',
    'orderby' => 'publish_date',
];

// Maybe it's multilingual
if (function_exists('pll_current_language')) {
    $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
} ?>

<div class="container-md">
    <div class="row lazyblock-projects justify-content-center">
        <div class="col-12 col-lg-9 projects-wrapper">
            <?php if ($attributes["title"] ?? '') { ?>
                <div class="row">
                    <div class="col <?= $attributes["textalignment"]; ?>">
                        <h2 class="title"><?= $attributes["title"] ?></h2>
                    </div>
                </div>
            <?php } ?>
            <?php
            // Custom query.
            $myquery = new WP_Query($args);
            $projects = $myquery->get_posts();
            ?>
            <?php if ($projects) { ?>

                <?php foreach ($projects as $project) { ?>
                    <div class="row pt-5 project-item-wrapper">

                    <?php if (get_field('image', $project)) { ?>
                    <div class="col-12 col-lg-4 pb-3 pb-lg-0 project-item-full-img">
                        <div class="project-item-img-wrapper">
                            <img src="<?= get_field('image', $project) ?: 'https://via.placeholder.com/320x240?text='.get_the_title($project)  ?>" class="project-item-img"
                                 alt="<?= get_the_title($project) ?>">
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 project-item-full-info">
                    <?php if (get_field('label', $project)) { ?>
                        <div class="project-item-category"><?= get_field('label', $project); ?></div>
                    <?php } ?>
                    <div class="project-item-title"><h4>
                            <a href="<?= get_the_permalink($project); ?>"><?= get_the_title($project); ?></a>
                        </h4></div>
                    <div class="project-item-excerpt">
                        <?= project_truncate(get_the_excerpt($project), '36'); ?>
                    </div>
                    <div class="project-item-cta"><a
                            href="<?= get_the_permalink($project) ?>"><?= pll__('Read more'); ?></a></div>
                    </div>

                    <?php } else { ?>

                    <?php if (get_field('label', $project)) { ?>
                        <div class="project-item-category"><?= get_field('label', $project); ?></div>
                    <?php } ?>
                        <div class="project-item-title"><h4>
                                <a href="<?= get_the_permalink($project); ?>"><?= get_the_title($project); ?></a>
                            </h4></div>
                        <div class="project-item-excerpt">
                            <?= project_truncate(get_the_excerpt($project), '36'); ?>
                        </div>
                        <div class="project-item-cta"><a
                                href="<?= get_the_permalink($project) ?>"><?= pll__('Read more'); ?></a></div>
                    </div>
                <?php } ?>
                <?php } ?>
            <?php } else { ?>
                <div class="row no-projects">
                    <div class="col">
                        <p><?php pll_e('No Projects'); ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php
// Restore original post data.
wp_reset_postdata();
?>

