<?php

$blockslug = 'section';
$blocktitle = 'Section';

// Start the block
$block             = [];
$block['slug']     = 'lazyblock/' . $blockslug;
$block['title']    = $blocktitle;
$block['icon']     = 'dashicons dashicons-tablet';
$block['category'] = 'common';

$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];


// START IMAGE CONTROL
$control = [];
$control['label'] = 'Background Image';
$control['type'] = 'image';
$control['name'] = 'backgroundimage';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END IMAGE CONTROL


// START SELECT CONTROL
$control = [];
$control['label'] = 'Background Color';
$control['type'] = 'select';
$control['name'] = 'background-color';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Primary',
        'value' => 'primary',
    ],
    [
        'label' => 'Secondary',
        'value' => 'secondary',
    ],
    [
        'label' => 'Supporting',
        'value' => 'supporting',
    ],
    [
        'label' => 'White',
        'value' => 'white',
    ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SELECT CONTROL


// START Opacity CONTROL
$control = [];
$control['label'] = 'Opacity (%)';
$control['type'] = 'text';
$control['name'] = 'opacity';
$control['default'] = '100';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'inspector';
//$control['placement'] = 'inspector';
$control['width'] = '40';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START SQUISH CONTROL
$control              = [];
$control['label']     = 'Container';
$control['type']      = 'radio';
$control['name']      = 'container';
$control['default']   = 'md';
$control['checked'] = true;
$control['help'] = '';
$control['child_of']  = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$choices = [];
$choices = [
    [
        'label' => 'MD',
        'value' => 'md',
    ],
    [
        'label' => 'Fluid',
        'value' => 'fluid',
    ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SQUISH CONTROL

include __DIR__ . '/../display-on.php';

// Content
$control             = [];
$control['label']    = '';
$control['name']     = 'content';
$control['type']     = 'inner_blocks';
$control['child_of'] = '';

$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


include __DIR__ . '/../extraspace.php';

// Alignment
$control              = [];
$control['label']     = 'Column alignment';
$control['type']      = 'select';
$control['name']      = 'alignment';
$control['default']   = 'justify-content-center';
$control['help'] = 'Only works with column grid active';
$control['child_of']  = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required']  = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$choices = [];
$choices = [
    [
        'label' => 'Left',
        'value' => 'justify-content-start',
    ],
    [
        'label' => 'Center',
        'value' => 'justify-content-center',
    ],
    [
        'label' => 'Right',
        'value' => 'justify-content-end',
    ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END SELECT CONTROL

include __DIR__ . '/../column-width.php';

// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'html';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];


if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
