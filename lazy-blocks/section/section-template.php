<style>
    #section-{{blockId}} {
    padding-top: {{custompaddingtop}}rem;
    padding-bottom: {{custompaddingbottom}}rem;
    }
</style>


{{#ifEquals container 'fluid'}}
<div class="container-fluid lazyblock-section {{displayOn}}" id="section-{{blockId}}">
    {{#if backgroundimage}}
    <div class="fluid-image" style="background: center / cover no-repeat url('{{backgroundimage.url}}'); opacity: {{opacity}}%;"></div>
    {{/if}}
    {{#if background-color}}
    <div class="fluid-color {{background-color}}" style="opacity: {{opacity}}%;"></div>
    {{/if}}
    <div class="container-md">
        <div class="row {{#if columngrid}}{{alignment}}{{/if}}">
            <div class="{{#if columngrid}}col-lg-{{columnwidth}}{{else}}col-lg-12{{/if}}"><InnerBlocks /></div>
        </div>
    </div>
</div>
{{/ifEquals}}
{{#ifEquals container 'md'}}
<div class="container-md lazyblock-section {{displayOn}}" id="section-{{blockId}}">
    {{#if backgroundimage}}
    <div class="container-image" style="background: center / cover no-repeat url('{{backgroundimage.url}}'); opacity: {{opacity}}%;"></div>
    {{/if}}
    {{#if background-color}}
    <div class="container-color {{background-color}}" style="opacity: {{opacity}}%;"></div>
    {{/if}}
    <div class="row {{#if columngrid}}{{alignment}}{{/if}}">
        <div class="{{#if columngrid}}col-lg-{{columnwidth}}{{else}}col-lg-12{{/if}}"><InnerBlocks /></div>
    </div>
</div>
{{/ifEquals}}

