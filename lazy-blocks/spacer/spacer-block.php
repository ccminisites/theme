<?php

$blockslug = 'spacer';
$blocktitle = 'Spacer';

// START THE BLOCK
$block             = [];
$block['slug']     = 'lazyblock/' . $blockslug;
$block['title']    = $blocktitle;
$block['icon']     = 'dashicons dashicons-image-flip-vertical';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];

// START NUMBER CONTROL
$control = [];
$control['label'] = 'Space in REMS';
$control['type'] = 'number';
$control['name'] = 'space';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '50';
$control['min'] = '0';
$control['max'] = '20';
$control['step'] = '1';
$control['default'] = '5';

$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END NUMBER CONTROL


// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'html';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];


if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
