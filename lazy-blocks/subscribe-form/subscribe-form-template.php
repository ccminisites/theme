<?php
/** @var array $attributes */
$image = $attributes['backgroundimage']['url'] ?? false;
$colors = $attributes['background-color'] ?? false;
?>
<div class="row mx-0 lazyblock-subscribe-form">
    <?php if ($attributes['backgroundimage']) { ?>
        <div class="full-image" style="background: center / cover no-repeat url('<?= $image ?>');"></div>
    <?php } ?>
    <?php if ($attributes['background-color']) { ?>
    <div class="full-color <?= $colors ?>"></div>
    <?php } ?>
    <div class="col-12 text-center subscribe-wrapper">
        <h3 class="col-12 col-lg-10"><?= $attributes['title'] ?></h3>
        <h5 class="col-10 col-lg-7"><?= $attributes['subtitle'] ?></h5>
        <form class="col-10 col-lg-7" id="newsletter-form" action="">
            <input type="hidden" name="form_id" value="newsletter">
            <input type="hidden" name="locale" value="<?= get_locale() ?>">
            <div class="row field-button-wrapper justify-content-center">
                <div class="col-12 col-lg-7">
                    <div class="form-group">
                        <label class="d-none" for="email"><?= pll__('newsletter-email-label'); ?></label>
                        <input id="email" type="email" class="form-control" placeholder="<?= pll__('email') ?>*" required name="email">
                    </div>
                    <div class="form-group">
                        <button type="submit" id="newsletterSubmit" class="btn btn-primary col-auto"><?= pll__('newsletter-submit') ?></button>
                    </div>
                </div>
            </div>
        </form>
        <div id="newsletter-spinner" class="d-none text-center">
            <?= pll__('loading') ?><br />
            <i class="fas fa-spin fa-circle-notch"></i>
        </div>
        <div id="newsletter-thankyou" class="d-none">
            <b><?= pll__('thank-you-for-subscribing') ?></b>
        </div>
    </div>
</div>

<script>
    // Submit a form using JS not the traditional way ;)
    // Instead of click you will use submit
    jQuery('#newsletter-form').on('submit', function(e){

        // Don't actually submit the form.
        e.preventDefault();

        // Gather the data in the form.
        // For real you probably will user jQuery('#testform').serialize()
        var data = jQuery('#newsletter-form').serialize();
        jQuery('#newsletter-form').addClass('d-none');
        jQuery('#newsletter-spinner').removeClass('d-none');
        // This would be a great place to hide your form and show a spinner
        // Post the data of the form to ccforms plugin.
        jQuery.post('/wp-json/project/v1/form', data, function (response) {
            //console.log(response);
            // This would be a great place to stop showing your spinner and instead show a thank you.
            // Or error message...
            jQuery('#newsletter-spinner').addClass('d-none');
            jQuery('#newsletter-thankyou').removeClass('d-none');
        });
    });
</script>
