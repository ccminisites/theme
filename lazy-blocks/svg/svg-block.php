<?php

$blockslug = 'svg';

// Start the block
$block             = [];
$block['title']    = 'SVG';
$block['icon']     = 'dashicons dashicons-format-image';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];
///////////// START CONTROLS /////////////

// Textarea
$control                         = [];
$control['label']                = 'SVG Code';
$control['name']                 = 'svg';
$control['type']                 = 'textarea';
$control['save_in_meta']         = false;
$control['child_of']             = '';
$control['hide_if_not_selected'] = 'false';

// Make an id.
$control_id = 'control-svg-svg';

// Add the control to the controls
$controls[$control_id] = $control;


// Radio
$control              = [];
$control['label']     = 'Full Width';
$control['name']      = 'fullwidth';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '0';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Yes',
        'value' => '1',
    ],
    [
        'label' => 'No',
        'value' => '0',
    ],
];


// Make an id.
$control_id = 'control-image-fullwidth';

// Add the control to the controls
$controls[$control_id] = $control;



///////////// END CONTROLS /////////////
// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
