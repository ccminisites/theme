<?php

$blockslug = 'teasers';
$blocktitle = 'Teasers';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-excerpt-view';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';


// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];

// START TEXT CONTROL
$control = [];
$control['label'] = 'Title';
$control['type'] = 'text';
$control['name'] = 'title';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// Layout
$control = [];
$control['label'] = 'Layout';
$control['name'] = 'layout';
$control['type'] = 'select';
$control['placement'] = 'inspector';
$control['required'] = true;
$control['default'] = 'image-content';
$control['child_of'] = '';

$control['choices'] = [
  [
    'label' => 'Image - Content',
    'value' => 'image-content',
  ],
  [
    'label' => 'Small Image (4) - Content (8)',
    'value' => 'small-image-content',
  ],
  [
    'label' => 'Vertical (image top)',
    'value' => 'vertical-image-top',
  ],
];

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// Dates
$control = [];
$control['label'] = 'Show dates?';
$control['name'] = 'show_dates';
$control['type'] = 'select';
$control['placement'] = 'inspector';
$control['help'] = 'Show publication date of posts';
$control['required'] = true;
$control['default'] = 'no';
$control['child_of'] = '';

$control['choices'] = [
  [
    'label' => 'Yes',
    'value' => 'yes',
  ],
  [
    'label' => 'No',
    'value' => 'no',
  ],
];

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START TOGGLE
$control = [];
$control['label']                = 'Border radius on image?';
$control['name']                 = 'border-radius';
$control['type']                 = 'toggle';
$control['placement']            = 'inspector';
$control['required']             = false;
$control['default']              = false;
$control['checked']              = true;
$control['child_of']             = '';
$control['alongside_text']       = 'Border Radius';

// Make an id.
$control_id = 'control-image-content-border-radius';

// Add the control to the controls
$controls[$control_id] = $control;
// END TOGGLE

// START SQUISH CONTROL
$control              = [];
$control['label']     = 'Container';
$control['type']      = 'select';
$control['name']      = 'container';
$control['default']   = 'md';
$control['checked'] = true;
$control['help'] = '';
$control['child_of']  = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$choices = [];
$choices = [
  [
    'label' => 'MD',
    'value' => 'md',
  ],
  [
    'label' => 'Fluid',
    'value' => 'fluid',
  ],
];
$control['choices'] = $choices;
$control_id = 'control-'. $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// START TEXT CONTROL
$control = [];
$control['label'] = 'Post Type';
$control['type'] = 'text';
$control['name'] = 'type';
$control['default'] = 'post';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Pagination';
$control['name']     = 'pagination';
$control['type']     = 'number';
$control['default'] = '12';
$control['help'] = '-1 all';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['min'] = '-1';
$control['max'] = '';
$control['step'] = '1';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START Pagination Style Control
$control = [];
$control['label'] = 'Pagination Style';
$control['type'] = 'select';
$control['name'] = 'paginationstyle';
$control['default'] = 'generic';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['multiple'] = 'false';
$control['output_format'] = '';
$choices = [];
$choices = [
  [
    'label' => 'Generic',
    'value' => 'generic'
  ],
  [
    'label' => 'Large',
    'value' => 'large'
  ],
  [
    'label' => 'Load More',
    'value' => 'loadmore'
  ],
  [
    'label' => 'None',
    'value' => 'none'
  ]
];

$control['choices'] = $choices;
$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT ALIGNMENT CONTROL

// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Offset';
$control['name']     = 'offset';
$control['type']     = 'number';
$control['default'] = '0';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['min'] = '0';
$control['max'] = '';
$control['step'] = '1';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Column Size';
$control['name']     = 'col';
$control['type']     = 'number';
$control['default'] = '4';
$control['help'] = '3 makes rows of 4, 4 makes rows of 3.';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['min'] = '1';
$control['max'] = '12';
$control['step'] = '1';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Margin bottom cards for spacing';
$control['name']     = 'mb';
$control['type']     = 'number';
$control['default'] = '2';
$control['help'] = 'Leave space between cards, choose number from 1 till 5';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['min'] = '1';
$control['max'] = '5';
$control['step'] = '1';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// START ALLOW CUTOFF CONTROL
$control = [];
$control['label'] = 'Allow Cutoff';
$control['type'] = 'toggle';
$control['name'] = 'allowcutoff';
$control['default'] = false;
$control['checked'] = false;
$control['alongside_text'] = 'Allow the user to to cutoff items by date field?';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END ALLOW CUTOFF CONTROL

// START TEXT CONTROL
$control = [];
$control['label'] = 'Cutoff field';
$control['type'] = 'text';
$control['name'] = 'cutofffield';
$control['default'] = 'starting_date';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// START ALLOW SORTING CONTROL
$control = [];
$control['label'] = 'Allow Sorting';
$control['type'] = 'toggle';
$control['name'] = 'allowsorting';
$control['default'] = true;
$control['checked'] = true;
$control['alongside_text'] = 'Allow the user to sort?';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END ALLOW SORTING CONTROL

// START ALLOW FILTERING CONTROL
$control = [];
$control['label'] = 'Allow Filtering';
$control['type'] = 'toggle';
$control['name'] = 'allowfiltering';
$control['default'] = true;
$control['checked'] = true;
$control['alongside_text'] = 'Allow the user to filter?';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END ALLOW FILTERING CONTROL


// START FILTERS CONTROL
$control = [];
$control['label'] = 'Filters';
$control['type'] = 'textarea';
$control['name'] = 'filters';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = "tax:category                         field:location";
$control['characters_limit'] = '';

$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END FILTERS CONTROL


$control_id = 'control-'.$blockslug.'-'.$control['name'];
$controls[$control_id] = $control;
// END FILTERS CONTROL


// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
