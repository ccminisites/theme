<?php
/** @var array $attributes */
//die("<pre>" . print_r($attributes, true) . "</pre>");

// Setup
$queryvar = $attributes['blockId'] . '_page'; // work on a custom query url param?
$orderqueryvar = $attributes['blockId'] . '_order'; // work on a custom query url param?
$cutoffqueryvar = $attributes['blockId'] . '_cutoff'; // work on a custom query url param?
$page = (int)($_GET[$queryvar] ?? 0); // what page are we on? 0 - X
$pagination = (int)($attributes['pagination'] ?: 12); // What is our pagination?
$offset = ((int)$attributes['offset']) + $page * $pagination; // Offset from the block plus the offset of the page X pagination
$order = $_GET[$orderqueryvar] ?? 'DESC';
$cutoff = $_GET[$cutoffqueryvar] ?? 'all';


// The query params with out our param in. This way we don't interfer with other stuff.
$get = $_GET;
unset($get[$queryvar]);


// Create a template water fall.
$template = false;
// First check if we have a template file in the base theme that matches the post type & display type.
if (file_exists(get_template_directory() . '/teasers/' . $attributes['type'] . '-' . $attributes['layout'] . '.php')) {
    $template = get_template_directory() . '/teasers/' . $attributes['type'] . '-' . $attributes['layout'] . '.php'; // parent theme?
}

// If not, check if we have a template file for the "post" post type with the selected display type as fallback
if (file_exists(get_template_directory() . '/teasers/post-' . $attributes['layout'] . '.php')) {
    $template = get_template_directory() . '/teasers/post-' . $attributes['layout'] . '.php'; // parent theme?
}

// Now check the child theme for specific templating
if (file_exists(get_stylesheet_directory() . '/teasers/' . $attributes['type'] . '-' . $attributes['layout'] . '.php')) {
    $template = get_stylesheet_directory() . '/teasers/' . $attributes['type'] . '-' . $attributes['layout'] . '.php'; // child theme?
}

$post_type = $attributes['type'] ?: 'post';
//die("<pre>" . print_r($order, true) . "</pre>");($order);
// Arguments for a post query
$args = [
  'post_type' => $post_type,
  'post_status' => 'publish',
  'posts_per_page' => $pagination,
  'order' => $order,
  'orderby' => 'date',
  'offset' => $offset,
];
// @todo Add a possible limit

// Maybe it's multilingual
if (function_exists('pll_current_language')) {
    $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
}

// @todo add_filter('teasers_block_arguments', $args);






if (!function_exists('makeFieldFilter')) {
    function makeFieldFilter($field, $label, $queryvar)
    {
        global $wpdb;
        $values = $wpdb->get_results("SELECT DISTINCT(meta_value) as meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = '" . $field . "'",
          ARRAY_A);

        $values = array_map(function ($value) {
            return $value['meta_value'];
        }, $values);

        sort($values);



        $html = '<select name="' . $queryvar . '">';
        $html .= '<option value="">' . $label . '</option>';
        foreach ($values as $value) {
            $selected = ($_GET[$queryvar] ?? null) == $value ? ' selected' : '';
            $value = $rewrites[$value] ?? $value;
            $html .= '<option value="' . $value . '"' . $selected . '>' . $value . '</option>';
        }
        $html .= '</select>';

        return $html;
    }
}

if (!function_exists('makeMRefFieldFilter')) {
    function makeMRefFieldFilter($field, $label, $queryvar)
    {
        global $wpdb;
        $values = $wpdb->get_results("SELECT DISTINCT(meta_value) as meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = '" . $field . "'",
          ARRAY_A);
        $post_ids = array_filter(array_map(function ($value) {
            return $value['meta_value'];
        }, $values));

        $html = '<select name="' . $queryvar . '">';
        $html .= '<option value="">' . $label . '</option>';
        $options = [];
        foreach ($post_ids as $post_id) {
            if (is_serialized($post_id))
            {
                $temp_post_ids = @unserialize($post_id);
                foreach ($temp_post_ids as $temp_post_id) {
                    $post = get_post($temp_post_id);
                    $options[$post->ID] = $post->post_title;
                }
            } else {
                $post = get_post($post_id);
                $options[$post->ID] = $post->post_title;
            }
        }
        asort($options);
        foreach ($options as $value => $label) {
            $selected = ($_GET[$queryvar] ?? null) == $value ? ' selected' : '';
            $html .= '<option value="' . $value . '"' . $selected . '>' . $label . '</option>';
        }

        $html .= '</select>';

        return $html;
    }
}

if (!function_exists('makeRefFieldFilter')) {
    function makeRefFieldFilter($field, $label, $queryvar)
    {
        global $wpdb;
        $values = $wpdb->get_results("SELECT DISTINCT(meta_value) as meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = '" . $field . "'",
          ARRAY_A);
        $post_ids = array_filter(array_map(function ($value) {
            return $value['meta_value'];
        }, $values));



        $html = '<select name="' . $queryvar . '">';
        $html .= '<option value="">' . $label . '</option>';
        $options = [];
        foreach ($post_ids as $post_id) {
            $post = get_post($post_id);
            $options[$post->ID] = $post->post_title;
        }
        asort($options);
        foreach ($options as $value => $label) {
            $selected = ($_GET[$queryvar] ?? null) == $value ? ' selected' : '';
            $html .= '<option value="' . $value . '"' . $selected . '>' . $label . '</option>';
        }

        $html .= '</select>';

        return $html;
    }
}

if (!function_exists('makeTaxFilter')) {
    function makeTaxFilter($tax, $label, $queryvar)
    {
        $terms = get_terms([
          'taxonomy' => $tax
        ]);

        $html = '<select name="' . $queryvar . '">';
        $html .= '<option value="">' . $label . '</option>';
        $options = [];
        foreach ($terms as $term) {

            $options[$term->term_id] = $term->name;
        }
        asort($options);
        foreach ($options as $value => $label) {

            $selected = (($_GET[$queryvar] ?? null) == $value) ? ' selected' : '';
            $html .= '<option value="' . $value . '"' . $selected . '>' . $label . '</option>';
        }

        $html .= '</select>';

        return $html;
    }
}

// Setting up the filters from the block attributes.
$filters = [];
if ($attributes["allowfiltering"] && $attributes["filters"]) {
    $filters = explode("\n", $attributes["filters"]);
    $filters = array_map(function($line) {
        return explode(":", trim($line));
    }, $filters);
}

// Here we need to add the filter values if there is on to the query filtering.

$tax_query = [
  'relation' => 'AND'
];
$meta_query = [
  'relation' => 'AND'
];

if ($attributes['allowcutoff'])
{
    if ($cutoff != 'all') {
        $op = '<=';
        if ($cutoff == 'upcoming') {
            $op = '>=';
            // If viewing upcoming things show the one closest to today at the top. Else just DESC.
            $args['order'] = 'ASC';
        }
        $meta_query[] = [
          'key' => $attributes['cutofffield'],
          'value' => date('Y-m-d H:i:s'),
          'compare' => $op
        ];
    }
    // Change the ordering to starting date.
    $args['meta_key'] = $attributes['cutofffield'];
    $args['orderby'] = 'meta_value_num';
    $args['meta_type'] = 'DATE';
}


// Rendering the filters
foreach ($filters as $index => $params)
{
    $filterqueryvar = $attributes['blockId'].'_filter_'.$index;

    if ($params[0] === 'field') {
      $filters[$index]['html'] = makeFieldFilter($params[1], $params[2], $filterqueryvar);
      if ($_GET[$filterqueryvar] ?? null) {
        $meta_query[] = [
          'key'	  	  => $params[1],
          'value'	  	=> $_GET[$filterqueryvar],
          'compare' 	=> '=',
        ];
      }
    }
    if ($params[0] === 'reffield') {
        $filters[$index]['html'] = makeRefFieldFilter($params[1], $params[2], $filterqueryvar);
        if ($_GET[$filterqueryvar] ?? null) {
            $meta_query[] = [
              'key'	  	  => $params[1],
              'value'	  	=> $_GET[$filterqueryvar],
              'compare' 	=> '=',
            ];
        }
    }
    if ($params[0] === 'mreffield') {
        $filters[$index]['html'] = makeMRefFieldFilter($params[1], $params[2], $filterqueryvar);
        if ($_GET[$filterqueryvar] ?? null) {
            $meta_query[] = [
              'key'	  	  => $params[1],
              'value'	  	=> '"'.$_GET[$filterqueryvar].'"',
              'compare' 	=> 'LIKE',
            ];
        }
    }
    if ($params[0] === 'tax') {
        $filters[$index]['html'] = makeTaxFilter($params[1], $params[2], $filterqueryvar);
        if ($_GET[$filterqueryvar] ?? null) {
            $tax_query[] = [
              'taxonomy' => $params[1],
              'field'    => 'term_id',
              'terms'    => $_GET[$filterqueryvar],
            ];
        }
    }
}

if (count($tax_query) > 1) {
  $args['tax_query'] = $tax_query;
}

if (count($meta_query) > 1) {
    $args['meta_query'] = $meta_query;
}



?>
<div class="container-<?= $attributes['container']; ?> lazyblock-teasers <?php if ($attributes['border-radius']) { ?>border-radius<?php } ?>">

    <?php if ($attributes["title"]) { ?>
      <div class="row">
        <div class="col-12">
          <h2 class="title"><?= $attributes["title"] ?></h2>
        </div>
      </div>
    <?php } ?>

    <?php if ($attributes["allowsorting"] || $attributes["allowfiltering"] || $attributes["allowcutoff"]) { ?>
      <form>
        <div class="row">
            <?php if ($attributes["allowsorting"]) { ?>
              <div class="col-5">
                <input id="<?= $orderqueryvar ?>_desc" name="<?= $orderqueryvar ?>" type="radio" value="DESC" <?= $order === 'DESC' ? 'checked' : '' ?> />
                <label for="<?= $orderqueryvar ?>_desc"><?= pll__('newest') ?></label>

                <input id="<?= $orderqueryvar ?>_asc" name="<?= $orderqueryvar ?>" type="radio" value="ASC" <?= $order === 'ASC' ? 'checked' : '' ?> />
                <label for="<?= $orderqueryvar ?>_asc"><?= pll__('oldest') ?></label>
              </div>
            <?php } ?>
            <?php if ($attributes["allowcutoff"]) { ?>
              <div class="col-5">
                <input id="<?= $cutoffqueryvar ?>_all" name="<?= $cutoffqueryvar ?>" type="radio" value="all" <?= $cutoff === 'all' ? 'checked' : '' ?> />
                <label for="<?= $cutoffqueryvar ?>_all"><?= pll__('all') ?></label>

                <input id="<?= $cutoffqueryvar ?>_upcoming" name="<?= $cutoffqueryvar ?>" type="radio" value="upcoming" <?= $cutoff === 'upcoming' ? 'checked' : '' ?> />
                <label for="<?= $cutoffqueryvar ?>_upcoming"><?= pll__('upcoming') ?></label>

                <input id="<?= $cutoffqueryvar ?>_past" name="<?= $cutoffqueryvar ?>" type="radio" value="past" <?= $cutoff === 'past' ? 'checked' : '' ?> />
                <label for="<?= $cutoffqueryvar ?>_past"><?= pll__('past') ?></label>
              </div>
            <?php } ?>
            <?php if ($attributes["allowfiltering"]) { ?>
              <div class="col-5">
                <?php foreach ($filters as $filter) { ?>
                  <?= $filter['html'] ?>
                <?php } ?>
              </div>
            <?php } ?>
          <div class="col-2">
            <input type="submit" value="<?= pll__('apply') ?>" class="btn btn-primary" />
            <a href="./" class="btn btn-secondary"><?= pll__('reset') ?></a>
          </div>
        </div>
      </form>
    <?php } ?>

    <?php
      $myquery = new WP_Query($args);
      if ($myquery->have_posts()) {
        $max = $myquery->max_num_pages - (floor($attributes['offset'] / $pagination)); // The max pages with the block offset considered.
    ?>
      <div class="row" id="content_<?= $attributes['blockId'] ?>">
          <?php while ($myquery->have_posts()) { $myquery->the_post(); ?>

              <?php if (!$template) { ?>
                <!-- NO TEMPLATE DEFINED: make a teasers/<?= $attributes['type'] ?>.php in your theme -->
                <div class="col-12 col-lg-<?= $attributes['col'] ?> mb-<?= $attributes['mb'] ?>">
                    <?= get_the_ID() ?>: <?= get_the_title(); ?>
                </div>
              <?php } else { ?>
                <div class="col-12 col-lg-<?= $attributes['col'] ?> mb-<?= $attributes['mb'] ?>">
                    <?php include $template; ?>
                </div>
              <?php } ?>

          <?php } ?>
      </div>

      <?php if ($attributes['paginationstyle'] !== 'none') { ?>
      <?php
        $max = $myquery->max_num_pages - (floor($attributes['offset'] / $pagination));
        // Only show pager if there is more than one page.
        if ($max > 1) {
      ?>
        <div
          class="row pagination <?= $attributes['paginationstyle'] === 'generic' ? ' pagination_generic' : '' ?><?= $attributes['paginationstyle'] === 'large' ? ' pagination_large' : '' ?><?= $attributes['paginationstyle'] === 'loadmore' ? ' d-none' : '' ?>"
          id="pagination_<?= $attributes['blockId'] ?>">
          <a class="pagination_link pagination_link_first"
             href="<?= $get ? '?' . http_build_query($get) : './' // query url to go to the first page with disturbing others. ?>"><?php pll_e('First') ?></a>
            <?php
            $i = 0;
            $query = $get; // url query without our queryvar in.
            while ($i++ < $max) {
                $query[$queryvar] = $i - 1;
                $queryparams = http_build_query($query); // query url params with our page.
                ?>
              <a
                class="pagination_link<?= $page === $i - 1 ? ' active' : '' ?> pagination_link_pagediff-<?= $page - ($i - 1) ?>"
                href="?<?= $queryparams ?>"><?= $i ?></a>
            <?php } ?>
          <a class="pagination_link pagination_link_last"
             href="?<?= http_build_query($get + [$queryvar => ($max - 1)]) // query url with the last page ?>"><?php pll_e('Last') ?></a>
        </div>


        <?php if ($attributes['paginationstyle'] === 'loadmore' && ($max - 1)) { ?>

          <div class="row pagination pagination_loadmore">
            <a href="#" data-page="0" data-max="<?= $max - 1 ?>" data-target="content_<?= $attributes['blockId'] ?>"
               data-base="<?= http_build_query($get) ?>" data-queryvar="<?= $queryvar ?>"
               class="pagination_loadmore"
               id="pagination_loadmore_<?= $attributes['blockId'] ?>"
            ><?php pll_e('load_more'); ?></a>
            <span class="d-none" id="hidden_content_<?= $attributes['blockId'] ?>"></span>
          </div>
          <script>
            jQuery(document).ready(function () {
              jQuery('#pagination_loadmore_<?= $attributes['blockId'] ?>').on('click', function (e) {
                e.preventDefault();
                let dis = jQuery(this); // save this for later
                let page = jQuery(this).data('page'); // get the page we are on

                if (typeof page !== "undefined" && !dis.hasClass('processing')) { // check the page and make sure we are not already processing.
                  let base = jQuery(this).data('base'); // base url
                  let max = jQuery(this).data('max'); // the max page we can go to
                  let queryvar = jQuery(this).data('queryvar'); // the url param we are working on
                  let target = jQuery(this).data('target'); // the target div
                  dis.addClass('processing'); // turn on the processing
                  let url = '?' + base + '&' + queryvar + '=' + (page + 1); // make the url we need to load up.
                  jQuery('#hidden_' + target).load(url + ' #' + target, function () { // load the div in to the hidden
                    jQuery('#' + target).append(jQuery('#hidden_' + target + ' > div').html()); // append the html of the div within the hidden
                    jQuery('#hidden_' + target).html(''); // blank out the hidden
                    page++; // increase the page
                    dis.data('page', page); // change the data on the link
                    dis.removeClass('processing'); // remove processing
                    if (page === max) {
                      dis.addClass('d-none'); // hide the link if we are at the end
                    }
                  });
                }
              });
            });
          </script>
        <?php } ?>
      <?php
        }
    }
      ?>
    <?php } else { ?>
      <div class="row nothingtoshow">
        <div class="col-12">
          <p><?php pll_e('nothing_to_show'); ?></p>
        </div>
      </div>
    <?php } ?>
    <?php wp_reset_query(); ?>
</div>
