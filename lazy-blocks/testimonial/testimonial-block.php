<?php

$blockslug = 'testimonial';
$blocktitle = 'Testimonials';

// START THE BLOCK
$block = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title'] = $blocktitle;
$block['icon'] = 'dashicons dashicons-testimonial';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';

// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = true;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [
    'spacings' => false,
    'display' => false,
    'scrollReveal' => false,
    'frame' => false,
    'customCSS' => false,
];

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// START THE CONTROLS
$controls = [];

// START REPEATER CONTROL
$control = [];
$control['label'] = 'Testimonial Content';
$control['type'] = 'repeater';
$control['name'] = 'testimonial-elements';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = '';
$control['placement'] = 'content';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['rows_min'] = '1';
$control['rows_max'] = '10';
$control['rows_label'] = 'Testimonial {{#}}';
$control['rows_add_button_label'] = '+ Add Content';
//$control['rows_collapsible']      = true;
//$control['rows_collapsed']        = true;

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END REPEATER CONTROL

// START CLASSICEDITOR CONTROL
$control = [];
$control['label'] = 'Content';
$control['type'] = 'classic_editor';
$control['name'] = 'content';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = 'control-' . $blockslug . '-testimonial-elements';
$control['placement'] = 'content';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END CLASSICEDITOR CONTROL

// START TOGGLE
$control = [];
$control['label'] = 'Show icon';
$control['name'] = 'show-icon';
$control['type'] = 'toggle';
$control['placement'] = 'inspector';
$control['required'] = false;
$control['default'] = true;
$control['checked'] = true;
$control['child_of'] = '';
$control['alongside_text'] = 'Show icon beside testimonial text';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

$control = [];
$control['label'] = 'Show Testimonials in carousel view';
$control['name'] = 'carousel-view';
$control['type'] = 'toggle';
$control['placement'] = 'inspector';
$control['required'] = false;
$control['default'] = false;
$control['checked'] = true;
$control['child_of'] = '';
$control['alongside_text'] = 'Show testimonial as a carousel';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;
// END TOGGLE

// Image
$control = [];
$control['label'] = 'Photo';
$control['name'] = 'photo';
$control['type'] = 'image';
$control['width'] = '30';
$control['child_of'] = 'control-' . $blockslug . '-testimonial-elements';
$control['default'] = '';
$control['help'] = '';
$control['placement'] = 'content';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = '';
$control['placeholder'] = '';
$control['translate'] = 'false';
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'false';
$control['translate'] = 'false';


$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END


// START TEXT CONTROL
$control = [];
$control['label'] = 'Name';
$control['type'] = 'text';
$control['name'] = 'name';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = 'control-' . $blockslug . '-testimonial-elements';
$control['placement'] = 'content';
$control['width'] = '30';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = '';
$control['placeholder'] = '';
$control['translate'] = 'false';
$control['required'] = false;

$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;

$control = [];
$control['label'] = 'Title';
$control['type'] = 'text';
$control['name'] = 'title';
$control['default'] = '';
$control['help'] = '';
$control['child_of'] = 'control-' . $blockslug . '-testimonial-elements';
$control['placement'] = 'content';
$control['width'] = '30';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = '';
$control['placeholder'] = '';
$control['translate'] = 'false';
$control['required'] = false;

$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;
// END TEXT CONTROL


// NUMBER CONTROL
$control = [];
$control['label'] = 'Star Rating';
$control['type'] = 'number';
$control['name'] = 'rating';
$control['default'] = '1';
$control['help'] = 'From one to five';
$control['child_of'] = 'control-' . $blockslug . '-testimonial-elements';
$control['placement'] = 'content';
$control['width'] = '10';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['save_in_meta_name'] = '';
$control['placeholder'] = '';
$control['translate'] = 'false';
$control['required'] = false;
$control['min'] = '1';
$control['max'] = '5';

$control_id = 'control-' . $blockslug . '-testimonial-elements-' . $control['name'];
$controls[$control_id] = $control;

// spacing
include __DIR__ . '/../extraspace.php';

// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['show_preview'] = 'never';
$code['single_output'] = false;
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['editor_css'] = '';
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}

