<?php
/** @var array $attributes */
// cleaning classes and block variables.
$blockID = $attributes['blockId'];
?>

<style>
    #lazyblock-testimonial-<?= $blockID; ?> {
        padding-top: <?= $attributes['custompaddingtop']; ?>rem;
        padding-bottom: <?= $attributes['custompaddingbottom'];?>rem;
    }

    .testimonial-rating {
        position: relative;
        display: inline-block;
        font-size: 0;
    }

    .testimonial-rating .stars {
        display: inline-block;
        width: 16px;
        height: 16px;
        background-image: url('<?= get_template_directory_uri() ?>/assets/img/icons/star.png');
        background-repeat: no-repeat;
        background-size: contain;
        vertical-align: middle;
    }

    .testimonial-rating .stars:after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 16px;
        height: 100%;
        background-image: url('<?= get_template_directory_uri() ?>/assets/img/icons/empty-star.png');
        background-repeat: no-repeat;
        background-size: contain;
    }
</style>

<div class="container-md lazyblock-testimonial-container" id="<?= 'lazyblock-testimonial-' . $blockID; ?>">
    <div class="row mx0 lazyblock-testimonial-wrapper">
        <div class="col-12 px-0 <?= $attributes['carousel-view'] ? 'testimonial-inner-wrapper' : '' ?>">
            <?php foreach ($attributes['testimonial-elements'] as $value) {
                $content = $value['content'] ?? '';
                $photoUrl = $value['photo']['url'];
                $photoAlt = $value['photo']['alt'];
                $name = $value['name'];
                $title = $value['title'];
                $testimonial_rating = $value['rating'];
                $max_rating = 5;
                ?>
                <div class="row mb-4">
                    <div class="lazyblock-testimonial-content d-flex align-items-start">
                        <?php if ($attributes['show-icon']) { ?>
                            <div class="lazyblock-testimonial-icon mt-1 me-3">
                                <i class="fa-solid fa-user"></i>
                            </div>
                        <?php } ?>
                        <div class="lazyblock-testimonial-text">
                            <?= $content; ?>
                        </div>
                    </div>
                    <div class="lazyblock-testimonial-author-related d-flex align-items-center px-0 mt-4">
                        <?php if (!empty($photoUrl)) { ?>
                            <div class="author-photo">
                                <img src="<?= $photoUrl; ?>" alt="<?= $photoAlt; ?>">
                            </div>
                        <?php } ?>
                        <div class="name-title ms-3 flex-grow-1">
                            <strong><?= $name; ?></strong>
                            <br>
                            <p><?= $title; ?></p>
                        </div>
                        <?php if (!empty($testimonial_rating)) { ?>
                        <div class="testimonial-rating">
                            <?php for ($index = 1; $index <= $max_rating; $index++): ?>
                                <?php $star_opacity = $index <= $testimonial_rating ? '1' : '0.3'; // Set opacity based on rating ?>
                                <span class="stars" style="opacity: <?= $star_opacity ?>;"></span>
                            <?php endfor; ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
