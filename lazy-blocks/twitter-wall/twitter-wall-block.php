<?php

$blockslug = 'twitter-wall';
$blocktitle = 'Twitter Wall';

// Start the block
$block             = [];
$block['slug'] = 'lazyblock/' . $blockslug;
$block['title']    = $blocktitle;
$block['icon']     = 'dashicons dashicons-twitter';
$block['description'] = "";
$block['keywords'] = [];
$block['category'] = 'common';
$block['category_label'] = 'Common';


// START THE SUPPORTS
$supports = [];
$supports['customClassName'] = false;
$supports['anchor'] = false;
$supports['align'] = [];
$supports['html'] = false;
$supports['multiple'] = true;
$supports['inserter'] = true;

// END SUPPORTS
$block['supports'] = $supports;

// START GHOSTKIT
$ghostkit = [];
$ghostkit['supports'] = [];
$ghostkit['supports']['spacings'] = false;
$ghostkit['supports']['display'] = false;
$ghostkit['supports']['scrollReveal'] = false;
$ghostkit['supports']['frame'] = false;
$ghostkit['supports']['customCSS'] = false;

// END GHOSTKIT
$block['ghostkit'] = $ghostkit;

// START STYLES
$block['styles'] = [];

// Holder for the controls
$controls = [];

// Label
$control             = [];
$control['label']    = 'Feed Name';
$control['name']     = 'feedName';
$control['type']     = 'text';
$control['help'] = 'Only fill in the Feed Name that you can find in the generated url "https://juicer.io/embed/Feed Name/embed-code.js" , register on https://juicer.io for a free account with one feed';
$control['child_of'] = '';
$control['placement'] = 'content';
//$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = false;
$control['placeholder'] = '';
$control['characters_limit'] = '';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;

// START NUMBER CONTROL
$control             = [];
$control['label']    = 'Show posts per';
$control['name']     = 'per';
$control['type']     = 'number';
$control['default'] = '';
$control['help'] = 'Show the amount of posts before load more button, leave empty or type 0 to show all';
$control['child_of'] = '';
//$control['placement'] = 'content';
$control['placement'] = 'inspector';
$control['width'] = '100';
$control['hide_if_not_selected'] = false;
$control['save_in_meta'] = false;
$control['required'] = true;
$control['placeholder'] = '';
$control['characters_limit'] = '';
$control['allow_null'] = 'true';
$control['min'] = '1';
$control['max'] = '';
$control['step'] = '1';

$control_id = 'control-' . $blockslug . '-' . $control['name'];
$controls[$control_id] = $control;


// END THE CONTROLS
$block['controls'] = $controls;


// START CODE
$code = [];
$code['output_method'] = 'php';
$code['editor_html'] = '';
$code['editor_callback'] = '';
$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['frontend_callback'] = '';
$code['frontend_css'] = '';
$code['show_preview'] = 'never';
$code['single_output'] = true;

// END CODE
$block['code'] = $code;

// CONDITIONAL
$block['condition'] = [];

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
