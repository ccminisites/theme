<?php
/** @var array $attributes */

?>

<div class="row px-0 mx-0 lazyblock-twitter-wall">
    <div class="col-12 px-0 mx-auto">
        <script src="https://juicer.io/embed/<?= $attributes['feedName'] ?>/embed-code.js<?php if ($attributes['per']) { ?>?per=<?= $attributes['per']?><?php } ?>" async defer></script>
    </div>
</div>
