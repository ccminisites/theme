<?php

$languages = pll_the_languages(['raw' => 1]);
ksort($languages);
?>

<div class="navbar-container fixed-top menu-top">
  <div class="container-xl">
    <nav class="navbar navbar-expand-lg">
      <div class="col-auto navbar-brand">
        <a class="brand" href="<?= bloginfo('url') ?>">
          <img class="d-inline-block align-top"
               src="<?= get_option('p_logo') ?: 'https://via.placeholder.com/320x240?text=logo' ?>"
               alt='<?= bloginfo('title') ?>'>
        </a>
      </div>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse justify-content-lg-end" id="navbarNav">

          <?php
          wp_nav_menu([
              'theme_location'  => 'main-menu',
              'menu_class'      => 'col nav navbar-nav ml-auto',
              'container_id'    => 'mainmenu',
              'container_class' => 'navbar-nav',
              'after'           => '<span class="menu-separator"></span>',
              'bootstrap'       => true,
              'walker' => new BootstrapNavMenuWalkerDropdown(),
          ]); ?>
          <?php if (get_option('p_socials_in_menu') === 'yes') { ?>
        <div id="social-icons">
            <?php
            if (get_option('p_instagram_link')) { ?>
              <div class="social-icon">
                  <a href="<?= get_option('p_instagram_link') ?>" target="_blank" aria-label="Instagram"
                     rel="noopener">
                      <i class="fab fa-instagram"></i>
                  </a>
              </div>
            <?php
            } ?>
            <?php
            if (get_option('p_facebook_link')) { ?>
              <div class="social-icon">
                  <a href="<?= get_option('p_facebook_link') ?>" target="_blank" aria-label="Facebook"
                     rel="noopener">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </div>
            <?php
            } ?>
            <?php
            if (get_option('p_linkedin_link')) { ?>
                <div class="social-icon">
                    <a href="<?= get_option('p_linkedin_link') ?>" target="_blank" aria-label="Linkedin"
                       rel="noopener">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                </div>
            <?php
            } ?>
            <?php
            if (get_option('p_x-twitter_link')) { ?>
                <div class="social-icon">
                    <a href="<?= get_option('p_x-twitter_link') ?>" target="_blank" aria-label="X twitter"
                       rel="noopener">
                        <i class="fa-brands fa-square-x-twitter"></i>
                    </a>
                </div>
                <?php
            } ?>
            <?php
            if (get_option('p_youtube_link')) { ?>
                <div class="social-icon">
                    <a href="<?= get_option('p_youtube_link') ?>" target="_blank" aria-label="Youtube"
                       rel="noopener">
                        <i class="fa-brands fa-youtube"></i>
                    </a>
                </div>
                <?php
            } ?>
        </div>
          <?php } ?>
          <?php
          if (count($languages) > 1) { ?>
            <div class="dropdown language-dropdown d-none d-lg-block">
              <div class="pointer dropdown-toggle">
                <a><?= $languages[pll_current_language()]['slug'] ?>
                </a>
              </div>
              <div class="other-language">
                  <?php
                  foreach ($languages as $iso => $language):
                      ?>
                      <?php
                      if ($iso != pll_current_language()):
                          ?>
                        <a href="<?= $language['url'] ?>"
                           class="language-item <?= implode(' ', $language['classes']) ?>"><?= $iso ?></a>
                      <?php
                      endif;
                      ?>
                  <?php
                  endforeach;
                  ?>
              </div>
            </div>

            <div class="languages d-lg-none">
              <div class="current-language">
                <a><?= $languages[pll_current_language()]['slug'] ?></a>
              </div>
              <div class="other-language">
                  <?php
                  foreach ($languages as $iso => $language) :
                      ?>
                      <?php
                      if ($iso != pll_current_language()) :
                          ?>
                        <a href="<?= $language['url'] ?>"
                           class="language-item <?= implode(' ', $language['classes']) ?>"><?= $iso ?></a>
                      <?php
                      endif;
                      ?>
                  <?php
                  endforeach;
                  ?>
              </div>
            </div>
          <?php
          } ?>
      </div>
    </nav>
  </div>
</div>
