<?php
$languages = pll_the_languages(['raw' => 1]);
ksort($languages);
?>

<div class="container-fluid px-xl-4 fixed-top menu-flyout">
  <nav class="row g-0 large">
    <div class="col-auto navbar-brand">
      <a class="brand" href="<?= bloginfo('url') ?>">
        <img class="d-inline-block align-top"
             src="<?= get_option('p_logo') ?: 'https://via.placeholder.com/320x240?text=logo' ?>"
             alt='<?= bloginfo('title') ?>'>
      </a>
    </div>
    <div id="nav-button" class="col-2 menu-button-wrapper pointer" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
         aria-label="Toggle navigation">
      <div class="menu-button">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    <div id="nav-menu-overlay">
      <div class="menu-content">
        <div class="overlay-brand">
          <a class="brand" href="<?= bloginfo('url'); ?>">
            <img class="d-inline-block align-top"
                 src="<?= get_option('p_logo_flyout') ?: get_option('p_logo') ?>"
                 alt='<?= bloginfo('title') ?>'>
          </a>
        </div>
          <?php
          wp_nav_menu([
              'theme_location'  => 'main-menu',
              'menu_class'      => 'main-menu',
              'container_id'    => 'navbarNav',
              'container_class' => '',
              'after'           => '<span class="menu-separator"></span>',
              'bootstrap'       => true,
              'walker' => new BootstrapNavMenuWalkerDropstart(),
          ]); ?>
          <?php if (get_option('p_socials_in_menu') === 'yes') { ?>
        <div id="social-icons">
            <?php
            if (get_option('p_instagram_link')) { ?>
                <div class="social-icon">
                    <a href="<?= get_option('p_instagram_link') ?>" target="_blank" aria-label="Instagram"
                       rel="noopener">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            <?php
            } ?>
            <?php
            if (get_option('p_facebook_link')) { ?>
              <div class="social-icon">
                  <a href="<?= get_option('p_facebook_link') ?>" target="_blank"
                     aria-label="Facebook"
                     rel="noopener">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </div>
            <?php
            } ?>
            <?php
            if (get_option('p_linkedin_link')) { ?>
              <div class="social-icon">
                <a href="<?= get_option('p_linkedin_link') ?>" target="_blank"
                   aria-label="Linkedin"
                   rel="noopener">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </div>
            <?php
            } ?>
            <?php
            if (get_option('p_x-twitter_link')) { ?>
                <div class="social-icon">
                    <a href="<?= get_option('p_x-twitter_link') ?>" target="_blank" aria-label="X twitter"
                       rel="noopener">
                        <i class="fa-brands fa-square-x-twitter"></i>
                    </a>
                </div>
                <?php
            } ?>
            <?php
            if (get_option('p_youtube_link')) { ?>
                <div class="social-icon">
                    <a href="<?= get_option('p_youtube_link') ?>" target="_blank" aria-label="Youtube"
                       rel="noopener">
                        <i class="fa-brands fa-youtube"></i>
                    </a>
                </div>
                <?php
            } ?>
        </div>
          <?php } ?>
          <?php
          if (count($languages) > 1) { ?>
            <div class="languages">
              <div class="current-language">
                <a><?= $languages[pll_current_language()]['slug']; ?></a>
              </div>
              <div class="other-language">
                  <?php
                  foreach ($languages as $iso => $language) :
                      ?>
                      <?php
                      if ($iso != pll_current_language()) :
                          ?>
                        <a href="<?= $language['url'] ?>"
                           class="language-item <?= implode(' ', $language['classes']) ?>"><?= $iso ?></a>
                      <?php
                      endif;
                      ?>
                  <?php
                  endforeach;
                  ?>
              </div>
            </div>
          <?php
          } ?>
      </div>
    </div>
  </nav>
</div>
