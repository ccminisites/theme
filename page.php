<?php

$seconds_to_cache = 300;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");


if (get_option('p_parkit_on')) {
    $parkit_card = get_option('p_parkit_card');
    if (isset($_GET['parkit_card']) && !empty($_GET['parkit_card'])) {
        $parkit_card = $_GET['parkit_card'];
    }
    $parkit_card = str_replace(".", "", $parkit_card);
    $parkit_card = get_template_directory() . '/parkit/' . $parkit_card . '.php';
    if (!file_exists($parkit_card)) {
      $parkit_card = get_template_directory() . '/parkit/artist.php';
    }
    include(get_template_directory() . '/parkit/includes/head.php');
    include $parkit_card;
    include(get_template_directory() . '/parkit/includes/foot.php');
    exit(0);
}

get_header();

if (have_posts()) {
  // Load posts loop.
    while (have_posts()) {
        the_post();
        ?>
    <div class="container-fluid px-0 main-content">
          <?php the_content(); ?>
    </div>
        <?php
    }
} else {
    get_404_template();
}

get_footer();
