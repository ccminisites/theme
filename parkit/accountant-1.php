<div class="container-fluid">
  <div class="row hellow" id="hellow-accountant-1">

    <div class="col-md-4 d-flex flex-column justify-content-center align-items-center brand-part">
      <img src="<?= get_option('p_logo') ?: get_template_directory_uri() . '/assets/img/icons/eu.png' ?>" alt="<?= get_option('p_title') ?> Logo">
      <h1><?= get_option('p_title') ?></h1>
    </div>
    <div class="col-md-8 d-flex flex-column justify-content-center card-infos p-5">
      <h2 class="card-accountant-heading">Over ons</h2>
      <p class="card-accountant-p mb-5">
          <?= get_option('p_description') ?>
      </p>

      <h2 class="card-accountant-heading">Contact</h2>
      <address>
        <?php if (get_option('p_gmap') && get_option('p_human_address')) { ?>
        <p class="card-accountant-p mb-2">
          <a href="<?= get_option('p_gmap') ?>" aria-label="Directions"><?= get_option('p_human_address') ?></a>
        </p>
        <?php } ?>
        <?php if(get_option('p_phone') && get_option('p_human_phone')) {?>
        <p class="card-accountant-p mb-2">
          <a href="tel:<?= get_option('p_phone') ?>" aria-label="Call"><?= get_option('p_human_phone') ?></a>
        </p>
        <?php } ?>
        <p class="card-accountant-p mb-2">
          <a href="mailto:<?= get_option('p_email') ?>" aria-label="Email"><?= get_option('p_email') ?></a>
        </p>
      </address>

      <h2 class="card-accountant-heading">Openingsuren</h2>
      <p class="card-accountant-p mb-5">
          <?= get_option('p_hours') ?>
      </p>

      <?php include(get_template_directory() . '/parkit/includes/social-links.php'); ?>
    </div>

    <?php include(get_template_directory() . '/parkit/includes/absolute-footer.php'); ?>
  </div>
</div>