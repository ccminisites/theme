<div class="container-fluid">
  <div class="row hellow" id="hellow-artist" style="background-image: url('<?= get_option('p_parkit_image') ?: 'https://via.placeholder.com/1024x768?text=%20' ?>');">

    <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5 align-self-center mx-auto text-center">
      <div class="card card-infos">
        <h1><?= get_option('p_title') ?></h1>
        <p class="px-4">
            <?= get_option('p_description') ?> <br>
        </p>
        <?php if(get_option('p_phone') && get_option('p_human_phone')) {?>
          <a href="tel:<?= get_option('p_phone') ?>" aria-label="Call"><?= get_option('p_human_phone') ?></a>
        <?php } ?>
        <a href="mailto:<?= get_option('p_email') ?>"><?= get_option('p_email') ?></a>
        <?php if (get_option('p_gmap') && get_option('p_human_address')) { ?>
          <a href="<?= get_option('p_gmap') ?>" aria-label="Directions"><?= get_option('p_human_address') ?></a>
        <?php } ?>
        <?php if (get_option('p_hours')) { ?>
          <p class="px-4 mt-4">
              <?= get_option('p_hours') ?> <br>
          </p>
        <?php } ?>
        <?php include(get_template_directory() . '/parkit/includes/social-links.php'); ?>
      </div>
    </div>

    <?php include(get_template_directory() . '/parkit/includes/absolute-footer.php'); ?>
  </div>
</div>