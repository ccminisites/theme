<div class="container-fluid">
  <div class="row justify-content-end hellow" id="hellow-florist-2" style="background-image: url('<?= get_option('p_parkit_image') ?: 'https://via.placeholder.com/1024x768?text=%20' ?>');">
    <div class="col-12 col-md-8 col-lg-10 col-xl-12 col-xxl-10 pe-0" id="card-container">
      <div class="card px-5 my-3">
        <div class="row card-content-wrapper">
          <div class="col-12 col-xl-3 text-center">
            <img src="<?= get_option('p_logo') ?: get_template_directory_uri() . '/assets/img/icons/eu.png' ?>" alt="<?= get_option('p_title') ?>">
          </div>
          <div class="col-12 col-md-8 col-xl-3 gx-lg-5 pt-4 pt-xl-0 mx-md-auto card-content">
            <h1><?= get_option('p_title') ?></h1>
            <p>
                <?= get_option('p_description') ?>
            </p>
          </div>
          <div class="col-md-8 col-xl-3 gx-lg-5 pt-4 pt-xl-0 mx-md-auto card-address">
            <address>
            <?php if (get_option('p_gmap') && get_option('p_human_address')) { ?>
              <p class="mb-3">
                <a href="<?= get_option('p_gmap') ?>" aria-label="Directions"><?= get_option('p_human_address') ?></a>
              </p>
            <?php } ?>
            <?php if(get_option('p_phone') && get_option('p_human_phone')) {?>
              <p>
                <a href="tel:<?= get_option('p_phone') ?>" class="card-link" aria-label="Call"><?= get_option('p_human_phone') ?></a>
              </p>
            <?php } ?>
              <p class="">
                <a href="mailto:<?= get_option('p_email') ?>" class="card-link" aria-label="Email"><?= get_option('p_email') ?></a>
              </p>
                <?php include(get_template_directory() . '/parkit/includes/social-links.php'); ?>
            </address>
          </div>

          <div class="col-md-8 col-xl-3 pt-xl-0 mx-md-auto card-hours-days">
            <div class="business-hours">
              <div class="opening-hours-days">
                <div class="mb-3"><?= get_option('p_hours') ?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include(get_template_directory() . '/parkit/includes/absolute-footer.php'); ?>
  </div>
</div>