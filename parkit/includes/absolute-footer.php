<footer class="blends-theme-footer position-absolute">
    <div class="row mx-4">
        <div class="col-md-12 text-start">
          <p class="text-start copyright">
            powered by <a href="https://blends.cloud">blends</a>
          </p>
        </div>
    </div>
</footer>