<!DOCTYPE html>
<html lang="<?= str_replace("_", "-", get_bloginfo('language')); ?>">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= get_option('p_title') ?></title>


    <meta name="robots" content="max-snippet:-1,max-image-preview:standard,max-video-preview:-1" />
    <meta name="description" content="<?= get_option('p_description') ?>" />

    <meta property="og:image" content="<?= get_option('p_logo') ?: get_template_directory_uri() . '/assets/img/icons/eu.png' ?>" />
    <meta property="og:image:width" content="512" />
    <meta property="og:image:height" content="512" />

    <meta property="og:locale" content="<?= get_locale() ?>" />
    <meta property="og:type" content="website" />

    <meta property="og:title" content="<?= get_option('p_title') ?>" />
    <meta property="og:description" content="<?= get_option('p_description') ?>" />
    <meta property="og:url" content="<?= get_bloginfo('url') ?>" />
    <meta property="og:site_name" content="<?= get_option('p_title') ?>" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="<?= get_option('p_title') ?>" />
    <meta name="twitter:description" content="<?= get_option('p_description') ?>" />

    <meta name="twitter:image" content="<?= get_option('p_logo') ?: get_template_directory_uri() . '/assets/img/icons/eu.png' ?>" />
    <meta name="twitter:image:width" content="512" />
    <meta name="twitter:image:height" content="512" />

    <link rel="canonical" href="<?= get_bloginfo('url') ?>" />
    <link rel="icon" type="image/x-icon" href="<?= get_option('p_logo') ?: get_template_directory_uri() . '/assets/img/icons/eu.png' ?>" />
    <script data-host="https://microanalytics.io" data-dnt="false" src="https://microanalytics.io/js/script.js" id="ZwSg9rf6GA" async defer></script>

    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/custom.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>