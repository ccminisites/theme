<?php if (get_option('p_instagram_link') ||
            get_option('p_facebook_link') ||
            get_option('p_linkedin_link') ||
            get_option('p_x-twitter_link')  ||
            get_option('p_youtube_link')) { ?>
<p>
    <ul class="d-flex list-unstyled social-link">

        <?php if (get_option('p_instagram_link')) { ?>
        <li class="me-3">
            <a href="<?= get_option('p_instagram_link') ?>" target="_blank">
                <i class="fab fa-instagram" style="font-size: 1.7rem;"></i>
            </a>
        </li>
        <?php } ?>

        <?php if (get_option('p_facebook_link')) { ?>
        <li class="me-3">
            <a href="<?= get_option('p_facebook_link') ?>" target="_blank">
                <i class="fab fa-facebook" style="font-size: 1.7rem;"></i>
            </a>
        </li>
        <?php } ?>

        <?php if (get_option('p_linkedin_link')) { ?>
        <li class="me-3">
            <a href="<?= get_option('p_linkedin_link') ?>" target="_blank">
                <i class="fab fa-linkedin" style="font-size: 1.7rem;"></i>
            </a>
        </li>
        <?php } ?>

        <?php if (get_option('p_x-twitter_link')) { ?>
        <li class="me-3">
            <a href="<?= get_option('p_x-twitter_link') ?>" target="_blank">
                <i class="fa-brands fa-x-twitter" style="font-size: 1.7rem;"></i>
            </a>
        </li>
        <?php } ?>

        <?php if (get_option('p_youtube_link')) { ?>
        <li class="me-3">
            <a href="<?= get_option('p_youtube_link') ?>" target="_blank">
                <i class="fab fa-youtube" style="font-size: 1.7rem;"></i>
            </a>
        </li>
        <?php } ?>
    </ul>
</p>
<?php } ?>
