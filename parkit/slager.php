<div class="container-fluid">
  <div class="row hellow" id="hellow-slager"
       style="background-image: url('<?= get_option('p_parkit_image') ?: 'https://via.placeholder.com/1024x768?text=%20' ?>')">

    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 align-self-center mx-auto text-center" id="slager-division">
      <div class="card mx-5 card-infos box-control">
        <h1><?= get_option('p_title') ?></h1>
        <h6><?= get_option('p_sub_title') ?></h6>

        <p class="px-4">
            <?= get_option('p_description') ?> <br>
        </p>
        <?php if(get_option('p_phone') && get_option('p_human_phone')) {?>
          <a href="tel:<?= get_option('p_phone') ?>" aria-label="Call"><?= get_option('p_human_phone') ?></a>
        <?php } ?>
          <a href="mailto:<?= get_option('p_email') ?>" aria-label="Email"><?= get_option('p_email') ?></a>
        <?php if (get_option('p_gmap') && get_option('p_human_address')) { ?>
          <a href="<?= get_option('p_gmap') ?>" aria-label="Directions"><?= get_option('p_human_address') ?></a>
        <?php } ?>

        <div class="row mt-2">
          <div class="col-md-12">
            <div class="business-hours">
              <div class="opening-hours-days">
                <div><?= get_option('p_hours') ?></div>
                <?php include(get_template_directory() . '/parkit/includes/social-links.php'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="blends-rectangles mx-5">
        <div class="one"></div>
        <div class="two"></div>
      </div>
    </div>

    <?php include(get_template_directory() . '/parkit/includes/absolute-footer.php'); ?>
  </div>
</div>