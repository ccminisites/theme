<div class="container-fluid">
  <!-- hellow-vet-1 card -->
  <div class="row hellow" id="hellow-vet-1">
    <div class="col-md-6 p-0">
      <img src="<?= get_option('p_parkit_image') ?: 'https://via.placeholder.com/1024x768?text=%20' ?>" alt="<?= get_option('p_title') ?>" class="img-fluid">
    </div>
    <div class="col-md-6">
      <div class="card-infos mx-4">
        <h1 class="card-heading mt-5 mb-5"><?= get_option('p_title') ?></h1>
        <p class="mb-5"><?= get_option('p_description') ?></p>
        <address>
          <p>

            <?php if (get_option('p_gmap') && get_option('p_human_address')) { ?>
              <a class="card-link" href="<?= get_option('p_gmap') ?>" aria-label="Directions"><?= get_option('p_human_address') ?></a> <br>
            <?php } ?>
            <?php if(get_option('p_phone') && get_option('p_human_phone')) {?>
              <a href="tel:<?= get_option('p_phone') ?>" class="card-link" aria-label="Call"><?= get_option('p_human_phone') ?></a> <br>
            <?php } ?>
            <a href="mailto:<?= get_option('p_email') ?>" class="card-link" aria-label="Email"><?= get_option('p_email') ?></a>
          </p>
        </address>

        <div class="row mt-5">
          <div class="col-md-12">
            <div class="flex-row business-hours">
              <div class="opening-hours-days">
                <div><?= get_option('p_hours') ?></div>
                <?php include( get_template_directory() . '/parkit/includes/social-links.php' ); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer Copyright -->
      <footer class="text-start blends-theme-footer position-fixed">
        <div class="col-md-12 text-start">
          <p class="text-start mx-4">
            powered by <a href="https://blends.cloud">blends</a>
          </p>
        </div>
      </footer>
      <!-- Footer Copyright ends -->
    </div>
  </div>
  <!-- hellow-vet-1 card ends -->
</div>
