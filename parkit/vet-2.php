<div class="container-fluid">

  <!-- hellow-vet-2 card -->
  <div class="row hellow" id="hellow-vet-2">
    <div class="col-md-12 col-lg-12 col-sm-12 p-0 parallax-cover" id="parallax"
         style="background-image: url('<?= get_option('p_parkit_image') ?: 'https://via.placeholder.com/1024x768?text=%20' ?>');">
      <img src="<?= get_option('p_parkit_image') ?: 'https://via.placeholder.com/1024x768?text=%20' ?>" alt="<?= get_option('p_title') ?>" class="img-fluid full-width" style="visibility: hidden;">
    </div>

    <div class="card-infos">
      <div class="row mx-4">
        <div class="col-md-4">
          <h1 class="card-heading mt-5"><?= get_option('p_title') ?></h1>
        </div>
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-6">
              <address>
                <p class="mb-5 mt-5">
                <?php if (get_option('p_gmap') && get_option('p_human_address')) { ?>
                  <a href="<?= get_option('p_gmap') ?>" class="card-link" aria-label="Directions"><?= get_option('p_human_address') ?></a> <br>
                <?php } ?>
                <?php if(get_option('p_phone') && get_option('p_human_phone')) {?>
                  <a href="tel:<?= get_option('p_phone') ?>" class="card-link" aria-label="Call"><?= get_option('p_human_phone') ?></a> <br>
                <?php } ?>
                  <a href="mailto:<?= get_option('p_email') ?>" class="card-link" aria-label="Email"><?= get_option('p_email') ?></a>
                </p>
              </address>
            </div>

            <div class="col-md-6">
              <div class="business-hours mb-5 mt-5">
                <div class="opening-hours-days">
                  <div><?= get_option('p_hours') ?></div>
                    <?php include(get_template_directory() . '/parkit/includes/social-links.php'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <footer class="blends-theme-footer">
      <div class="row mx-4">
        <div class="col-md-12 text-start">
          <p class="text-start copyright">
            powered by <a href="https://blends.cloud">blends</a>
          </p>
        </div>
      </div>
    </footer>
  </div>
  <!-- hellow-vet-2 card ends-->


</div>