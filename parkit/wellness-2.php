<div class="container-fluid">
  <div class="row hellow h100" id="hellow-wellness-2"
       style="background-image: url('<?= get_option('p_parkit_image') ?: 'https://via.placeholder.com/1024x768?text=%20' ?>');">

    <div class="col-xs-12 col-sm-4 col-md-6 col-lg-5 offset-md-7 offset-sm-0 wellness-box-controle">
      <div class="card mx-5 card-infos box-control text-center">
        <h1><?= get_option('p_title'); ?></h1>

        <div class="row mt-4">
          <div class="col-md-12">
            <p class="px-4">
                <?= get_option('p_description'); ?>
            </p>

            <p>
            <?php if(get_option('p_phone') && get_option('p_human_phone')) {?>
              <a href="tel:<?= get_option('p_phone') ?>" aria-label="Call"><?= get_option('p_human_phone') ?></a> <br>
            <?php } ?>
              <a href="mailto:<?= get_option('p_email') ?>" aria-label="Email"><?= get_option('p_email') ?></a> <br>
            <?php if (get_option('p_gmap') && get_option('p_human_address')) { ?>
              <a href="<?= get_option('p_gmap') ?>" aria-label="Directions"><?= get_option('p_human_address') ?></a>
            <?php } ?>
            </p>

            <div class="business-hours">
              <div class="opening-hours-days">
                <div> <?= get_option('p_hours') ?> </div>
              </div>
            </div>

              <?php
              include(get_template_directory() . '/parkit/includes/social-links.php'); ?>
          </div>
        </div>

      </div>
    </div>

      <?php
      include(get_template_directory() . '/parkit/includes/absolute-footer.php'); ?>
  </div>
</div>