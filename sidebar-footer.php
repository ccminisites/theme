<?php
$languages = pll_the_languages(['raw' => 1]);
$languageSlugs =  [];
foreach ($languages as $slug => $language) {
    $languageSlugs[$slug] = $slug;
}
?>
<div id="sidebar-footer" class="sidebar">
    <?php dynamic_sidebar( 'footer-' . $languageSlugs[pll_current_language()]); ?>
</div>
