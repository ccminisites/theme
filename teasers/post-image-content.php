<div class="row flex-centered single-post-teaser <?= $attributes['type']; ?> mb-5 p-2 p-lg-4">
    <div class="col-lg-6 col-12">
        <?php
        $featured_image = get_the_post_thumbnail_url(null, 'full');
        if ($featured_image) {
            ?>
            <div class="img-wrapper">
                <a href="<?php the_permalink(); ?>">
                    <img src="<?= $featured_image ?>" class="figure-img img-fluid" alt="<?= the_title(); ?>">
                </a>
            </div>
        <?php } ?>
    </div>
    <div class="col-lg-6 col-12">
        <h2 class="mb-5"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php if ($attributes['show_dates'] === 'yes') { ?>
        <div class="mb-5"><p><?php echo strtolower(get_the_date('d/m/Y')); ?></p></div>
        <?php } ?>
        <?php the_excerpt(); ?>
    </div>

</div>
